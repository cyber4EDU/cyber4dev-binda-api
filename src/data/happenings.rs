use super::{
    GroupId, Happening, HappeningId, HappeningPropsFrom, HappeningPropsMinimum, MemberId,
    MemberIdVariant, NewHappening,
};
use crate::error::Error;
use crate::model::{prelude::*, *};
use chrono::{DateTime, Utc};
use sea_orm::{
    ActiveModelTrait, ActiveValue, ColumnTrait, DatabaseConnection, EntityTrait, FromQueryResult,
    ModelTrait, QueryFilter, TransactionTrait,
};
use sea_orm_active_enums::HappeningRole;
use serde_json::json;
use std::collections::HashSet;
use tracing::info;

type JsonValue = serde_json::Value;

#[derive(FromQueryResult)]
#[allow(dead_code)]
struct HappeningFromQueryResult {
    id: uuid::Uuid,
    name: String,
    props: JsonValue,
}

fn happening_props_timestamps_to_utc(props: JsonValue) -> Result<JsonValue, Error> {
    let mut fixed_props = props.clone();

    // first check if props have the minimum set of keys
    // also here the timestamps are converted to UTC
    let min_props = serde_json::from_value::<HappeningPropsMinimum>(props)
        .map_err(|_| Error::BadRequest("Failed to parse props"))?;

    // overwrite with correct time stamps
    *fixed_props.get_mut("from").unwrap() = json!(min_props.from);
    *fixed_props.get_mut("to").unwrap() = json!(min_props.to);

    Ok(fixed_props)
}

impl NewHappening {
    pub async fn insert(
        self,
        conn: &DatabaseConnection,
        _creator: MemberIdVariant<'_>,
    ) -> Result<Happening, Error> {
        let props = self
            .props
            .map(happening_props_timestamps_to_utc)
            .transpose()?;

        let txn = conn.begin().await?;

        let new_happening = happenings::ActiveModel {
            name: ActiveValue::Set(self.name.clone().into()),
            props: ActiveValue::Set(props.clone()),
            ..ActiveModelTrait::default()
        };
        let new_happening = new_happening.insert(&txn).await?;
        let id = new_happening.id;

        // collect responsible members
        if let Some(resp) = &self.responsibles {
            for uuid in resp {
                member_is_to_happening::ActiveModel {
                    source_member_id: ActiveValue::Set(uuid.into()),
                    target_happening_id: ActiveValue::Set(id),
                    role: ActiveValue::Set(HappeningRole::Responsible),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        // collect participating members
        if let Some(resp) = &self.participants {
            for uuid in resp {
                member_is_to_happening::ActiveModel {
                    source_member_id: ActiveValue::Set(uuid.into()),
                    target_happening_id: ActiveValue::Set(id),
                    role: ActiveValue::Set(HappeningRole::Participant),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        // collect group connections
        if let Some(grps) = &self.groups {
            for uuid in grps {
                groups_happenings::ActiveModel {
                    group_id: ActiveValue::Set(uuid.into()),
                    happening_id: ActiveValue::Set(id),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        txn.commit().await?;

        info!("insert new happening");
        Ok(Happening {
            id: id.into(),
            name: self.name.clone(),
            props,
            responsibles: self.responsibles,
            participants: self.participants,
            groups: self.groups,
        })
    }
}

impl Happening {
    pub async fn load(conn: &DatabaseConnection, id: HappeningId) -> Result<Self, Error> {
        let happening: happenings::Model = Happenings::find_by_id(id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Happening not found"))?;

        // load responsibles, participants, happenings
        let responsibles = Some(
            MemberIsToHappening::find()
                .filter(member_is_to_happening::Column::TargetHappeningId.eq(id.0))
                .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Responsible))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.source_member_id.into())
                .collect::<Vec<MemberId>>(),
        )
        .filter(|vec| !vec.is_empty());

        let participants = Some(
            MemberIsToHappening::find()
                .filter(member_is_to_happening::Column::TargetHappeningId.eq(id.0))
                .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Participant))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.source_member_id.into())
                .collect::<Vec<MemberId>>(),
        )
        .filter(|vec| !vec.is_empty());
        let groups = Some(
            GroupsHappenings::find()
                .filter(groups_happenings::Column::HappeningId.eq(id.0))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.group_id.into())
                .collect::<Vec<GroupId>>(),
        )
        .filter(|vec| !vec.is_empty());

        Ok(Self {
            id: happening.id.into(),
            name: happening.name.into(),
            props: happening.props,
            responsibles,
            participants,
            groups,
        })
    }

    pub async fn update(mut self, conn: &DatabaseConnection) -> Result<Self, Error> {
        self.props = self
            .props
            .map(happening_props_timestamps_to_utc)
            .transpose()?;

        // load entry
        let id = Happenings::find_by_id(self.id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Happening not found"))?
            .id;

        let txn = conn.begin().await?;

        let happening = happenings::ActiveModel {
            id: ActiveValue::Set(id),
            name: ActiveValue::Set(self.name.clone().into()),
            props: ActiveValue::Set(self.props.clone()),
            ..ActiveModelTrait::default()
        };
        happening.update(&txn).await?;

        // update responsibles and participants (member-is-to-group)
        for (role, source) in &[
            (HappeningRole::Responsible, &self.responsibles),
            (HappeningRole::Participant, &self.participants),
        ] {
            let existing_members: HashSet<MemberId> = HashSet::from_iter(
                MemberIsToHappening::find()
                    .filter(member_is_to_happening::Column::TargetHappeningId.eq(id))
                    .filter(member_is_to_happening::Column::Role.eq(role.clone()))
                    .all(&txn)
                    .await?
                    .into_iter()
                    .map(|m| m.source_member_id.into()),
            );
            let update_members: HashSet<MemberId> = match source {
                Some(rspfr) => HashSet::from_iter(rspfr.iter().copied()),
                None => HashSet::new(),
            };
            for new_id in update_members.difference(&existing_members) {
                // create new entry
                member_is_to_happening::ActiveModel {
                    source_member_id: ActiveValue::Set(new_id.into()),
                    target_happening_id: ActiveValue::Set(id),
                    role: ActiveValue::Set(role.clone()),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
            for remove_id in existing_members.difference(&update_members) {
                // find endtry and remove it
                let rm_entry = MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::SourceMemberId.eq(remove_id.0))
                    .filter(member_is_to_group::Column::TargetGroupId.eq(id))
                    .filter(member_is_to_group::Column::Role.eq(role.clone()))
                    .one(&txn)
                    .await?;
                if let Some(entry) = rm_entry {
                    entry.delete(&txn).await?;
                }
            }
        }
        // update groups (groups-happenings)
        let existing_groups: HashSet<GroupId> = HashSet::from_iter(
            GroupsHappenings::find()
                .filter(groups_happenings::Column::HappeningId.eq(id))
                .all(&txn)
                .await?
                .into_iter()
                .map(|m| m.group_id.into()),
        );
        let update_groups: HashSet<GroupId> = match &self.groups {
            Some(rspfr) => HashSet::from_iter(rspfr.iter().copied()),
            None => HashSet::new(),
        };
        for new_id in update_groups.difference(&existing_groups) {
            // create new entry
            groups_happenings::ActiveModel {
                happening_id: ActiveValue::Set(id),
                group_id: ActiveValue::Set(new_id.into()),
                ..ActiveModelTrait::default()
            }
            .insert(&txn)
            .await?;
        }
        for remove_id in existing_groups.difference(&update_groups) {
            // find endtry and remove it
            let rm_entry = GroupsHappenings::find()
                .filter(groups_happenings::Column::GroupId.eq(id))
                .filter(groups_happenings::Column::HappeningId.eq(remove_id.0))
                .one(&txn)
                .await?;
            if let Some(entry) = rm_entry {
                entry.delete(&txn).await?;
            }
        }

        txn.commit().await?;

        Ok(self)
    }

    pub async fn delete(conn: &DatabaseConnection, id: HappeningId) -> Result<(), Error> {
        // don't delete if someone was checked-in
        let txn = conn.begin().await?;

        member_is_to_happening::Entity::delete_many()
            .filter(member_is_to_happening::Column::TargetHappeningId.eq(id.0))
            .exec(&txn)
            .await?;
        groups_happenings::Entity::delete_many()
            .filter(groups_happenings::Column::HappeningId.eq(id.0))
            .exec(&txn)
            .await?;
        happenings::Entity::delete_by_id(id).exec(&txn).await?;

        txn.commit().await?;

        Ok(())
    }

    pub async fn load_all(
        conn: &DatabaseConnection,
        not_before: &Option<DateTime<Utc>>,
    ) -> Result<Vec<Self>, Error> {
        let mut happenings = vec![];

        info!("get all happenings not before {:?}", not_before);
        for hp in Happenings::find()
            .into_model::<happenings::Model>()
            .all(conn)
            .await?
        {
            // if not_before, parse props and check
            if let Some(not_before) = not_before {
                if let Ok(props) =
                    serde_json::from_value::<HappeningPropsFrom>(hp.props.clone().into())
                {
                    if props.from < *not_before {
                        continue;
                    }
                } else {
                    continue;
                }
            }
            // load responsibles, participants, happenings
            let responsibles = Some(
                MemberIsToHappening::find()
                    .filter(member_is_to_happening::Column::TargetHappeningId.eq(hp.id))
                    .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Responsible))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.source_member_id.into())
                    .collect::<Vec<MemberId>>(),
            )
            .filter(|vec| !vec.is_empty());

            let participants = Some(
                MemberIsToHappening::find()
                    .filter(member_is_to_happening::Column::TargetHappeningId.eq(hp.id))
                    .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Participant))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.source_member_id.into())
                    .collect::<Vec<MemberId>>(),
            )
            .filter(|vec| !vec.is_empty());
            let groups = Some(
                GroupsHappenings::find()
                    .filter(groups_happenings::Column::HappeningId.eq(hp.id))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.group_id.into())
                    .collect::<Vec<GroupId>>(),
            )
            .filter(|vec| !vec.is_empty());

            happenings.push(Self {
                id: hp.id.into(),
                name: hp.name.into(),
                props: hp.props,
                responsibles,
                participants,
                groups,
            });
        }

        Ok(happenings)
    }
}
