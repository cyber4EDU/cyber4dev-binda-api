-- drop contacts column in members table
ALTER TABLE "members" DROP COLUMN "contacts";

-- recreate contact table
CREATE TABLE contact (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    member_id UUID NOT NULL,
    type TEXT NOT NULL,
    value TEXT NOT NULL,
    FOREIGN KEY (member_id) REFERENCES members (id)
);
