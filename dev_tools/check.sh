#!/bin/bash

set -e

sqlfluff format
cargo fmt --check
#cargo audit
cargo clippy
