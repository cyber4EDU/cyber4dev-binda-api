FROM rust:1-bullseye as build
WORKDIR /app
COPY . /app
# RUN rustup target add x86_64-unknown-linux-musl
RUN cargo build --release

FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/binda_api /usr/local/bin/binda_api
WORKDIR /usr/local/bin
CMD ["binda_api"]
