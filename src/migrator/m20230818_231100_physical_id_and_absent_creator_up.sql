ALTER TABLE "check_in_out" ADD COLUMN "physical_id" UUID;
ALTER TABLE "check_in_out" ADD CONSTRAINT "physical_id_fk_constraint"
FOREIGN KEY ("physical_id") REFERENCES "physical_ids" ("id");

ALTER TABLE "absents" ADD COLUMN "creator" UUID;
ALTER TABLE "absents" ADD CONSTRAINT "creator_fk_constraint"
FOREIGN KEY ("creator") REFERENCES "members" ("id");
