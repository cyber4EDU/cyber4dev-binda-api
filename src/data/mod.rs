mod absents;
mod absents_io;
mod actions;
pub mod dashboard;
mod groups;
mod groups_io;
mod happenings;
mod happenings_io;
pub mod helpers;
mod history;
mod members;
mod members_io;
mod physical_ids_io;
pub mod sorting;

use crate::model::sea_orm_active_enums::{AbsentReason as ModelAbsentReason, CheckType};
use chrono::{DateTime, Utc};
use sea_orm::{FromQueryResult, TryGetable};
use serde::{Deserialize, Serialize};
use tracing::debug;
use utoipa::{IntoParams, ToSchema};
use uuid::Uuid;

type JsonValue = serde_json::Value;

#[derive(Debug, Deserialize, IntoParams, ToSchema)]
pub struct TimeFrame {
    pub not_before: DateTime<Utc>,
    pub not_after: DateTime<Utc>,
}

#[derive(Debug, Deserialize, IntoParams, ToSchema)]
pub struct TimeFrameNotBefore {
    pub not_before: Option<DateTime<Utc>>,
}

#[derive(Debug, Deserialize, IntoParams, ToSchema)]
pub struct TimeData {
    pub now: Option<DateTime<Utc>>,
}

#[derive(Debug, Deserialize, IntoParams, ToSchema)]
pub struct Delay {
    minutes: Option<i16>,
}

#[derive(Debug, Deserialize, IntoParams, ToSchema)]
pub struct Search {
    pub search: Option<String>,
}

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Clone)]
#[schema(example = "Erika")]
pub struct MemberFirstName(String);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Clone)]
#[schema(example = "Elfriede Margarete")]
pub struct MemberMiddleNames(String);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Clone)]
#[schema(example = "Musterman")]
pub struct MemberLastName(String);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Clone)]
#[schema(example = "the funny one")]
pub struct MemberPersonQualifier(String);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Clone)]
#[schema(example = "Team A")]
pub struct GroupName(String);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Clone)]
#[schema(example = "Sport")]
pub struct HappeningName(String);

#[derive(Debug, Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct MemberId(Uuid);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct MemberIdentityId(Uuid);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct MemberPhysicalId(Uuid);

#[derive(Hash, Eq, PartialEq, Copy, Clone)]
pub enum MemberIdVariant<'a> {
    Id(&'a MemberId),
    Iid(&'a MemberIdentityId),
    Pid(&'a MemberPhysicalId),
}

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct GroupId(Uuid);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct HappeningId(Uuid);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct IdentityId(pub Uuid);

#[derive(Serialize, Deserialize, ToSchema, Hash, Eq, PartialEq, Copy, Clone)]
#[schema(example = "00000000-0000-0000-0000-000000000000")]
pub struct AbsentId(pub Uuid);

#[derive(Serialize, Deserialize, ToSchema)]
pub struct ResponsibleFor {
    pub members: Option<Vec<MemberId>>,
    pub groups: Option<Vec<GroupId>>,
    pub happenings: Option<Vec<HappeningId>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct ParticipantIn {
    pub groups: Option<Vec<GroupId>>,
    pub happenings: Option<Vec<HappeningId>>,
}

#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct NewPhysicalData {
    pub id: Option<MemberPhysicalId>,
    pub member_id: Option<MemberId>,
    pub deactivated: bool,
    pub note: Option<String>,
}

#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct PhysicalData {
    pub id: MemberPhysicalId,
    pub member_id: Option<MemberId>,
    pub deactivated: bool,
    pub note: Option<String>,
}

#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct PhysicalInfo {
    pub id: MemberPhysicalId,
    pub deactivated: bool,
    pub note: Option<String>,
}

#[derive(Clone, Serialize, Deserialize, ToSchema)]
// FIXME: apparently the example does not make it into the openapi docs :/
#[schema(example = json!({"name": "Gabi (Mutti von Erika)", "telefon": "0123456789"}))]
pub struct Contact(pub JsonValue);

#[derive(Serialize, Deserialize, ToSchema)]
pub struct NewMember {
    pub first_name: MemberFirstName,
    pub middle_names: Option<MemberMiddleNames>,
    pub last_name: MemberLastName,
    pub person_qualifier: Option<MemberPersonQualifier>,
    pub identity_id: Option<MemberIdentityId>,
    pub responsibles: Option<Vec<MemberId>>,
    pub responsible_for: Option<ResponsibleFor>,
    pub participant_in: Option<ParticipantIn>,
    pub physical_ids: Option<Vec<PhysicalInfo>>,
    pub contact: Option<Contact>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct Member {
    pub id: MemberId,
    pub first_name: MemberFirstName,
    pub middle_names: Option<MemberMiddleNames>,
    pub last_name: MemberLastName,
    pub person_qualifier: Option<MemberPersonQualifier>,
    pub identity_id: Option<MemberIdentityId>,
    pub responsibles: Option<Vec<MemberId>>,
    pub responsible_for: Option<ResponsibleFor>,
    pub participant_in: Option<ParticipantIn>,
    pub physical_ids: Option<Vec<PhysicalInfo>>,
    pub contact: Option<Contact>,
}

// TODO: remove
#[derive(Default, Serialize, Deserialize, ToSchema)]
pub enum MemberInfoCheckedInSortKeys {
    Id,
    FirstName,
    LastName,
    #[default]
    CheckedIn,
}

/// The central member data structure that is used to return information about members. Depending
/// on the endpoint that returns it optional values might be `None`.
#[derive(Serialize, Deserialize, ToSchema)]
pub struct MemberInfo {
    pub id: MemberId,
    pub first_name: MemberFirstName,
    pub middle_names: Option<MemberMiddleNames>,
    pub last_name: MemberLastName,
    pub person_qualifier: Option<MemberPersonQualifier>,
    pub contact: Option<Contact>,
    //
    pub groups: Option<Vec<GroupShortInfo>>,
    pub absent: Option<AbsentInfo>,
    pub checked_in: Option<HappeningShortInfo>,
    pub checked_in_before: Option<HappeningShortInfo>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct AbsentInfo {
    pub id: AbsentId,
    pub reason: AbsentReason,
    #[serde(rename = "from")]
    pub from_date: DateTime<Utc>,
    #[serde(rename = "until")]
    pub until_date: DateTime<Utc>,
}

// move to data::members module
#[derive(Serialize, Deserialize, FromQueryResult)]
pub struct MemberInfoAbsentCheckedIn {
    pub id: MemberId,
    pub first_name: MemberFirstName,
    pub middle_names: Option<MemberMiddleNames>,
    pub last_name: MemberLastName,
    pub person_qualifier: Option<MemberPersonQualifier>,
    // group-info
    // FIXME
    //pub groups: Vec<GroupShortInfo>,
    pub groups: Option<String>,
    // absent-info
    pub absent_id: Option<AbsentId>,
    pub absent_reason: Option<String>,
    pub absent_from_date: Option<DateTime<Utc>>,
    pub absent_until_date: Option<DateTime<Utc>>,
    // checked-in-info
    pub happening_id: Option<HappeningId>,
    pub happening_name: Option<HappeningName>,
    pub happening_from_date: Option<DateTime<Utc>>,
    pub happening_to_date: Option<DateTime<Utc>>,
    pub happening_auto_checkout: Option<i64>,
    pub contact: Option<Contact>,
}

#[derive(Default, Serialize, Deserialize, ToSchema)]
pub enum MemberInfoSortKeys {
    Id,
    #[default]
    FirstName,
    MiddleNames,
    LastName,
    PersonQualifier,
}

#[derive(Default, Serialize, Deserialize, ToSchema)]
pub enum MemberInfoAbsentSortKeys {
    Id,
    #[default]
    FirstName,
    MiddleNames,
    LastName,
    PersonQualifier,
    Reason,
    FromDate,
    UntilDate,
}

#[derive(Default, Serialize, Deserialize, ToSchema)]
pub enum MemberInfoGroupsSortKeys {
    Id,
    #[default]
    FirstName,
    MiddleNames,
    LastName,
    PersonQualifier,
    GroupName,
}

// TODO: remove
#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct MemberAbsentInfo {
    pub id: MemberId,
    pub first_name: MemberFirstName,
    pub last_name: MemberLastName,
    pub reason: AbsentReason,
    #[serde(rename = "from")]
    pub from_date: DateTime<Utc>,
    #[serde(rename = "until")]
    pub until_date: DateTime<Utc>,
}

// TODO: remove
#[derive(Default, Serialize, Deserialize, ToSchema)]
pub enum MemberAbsentInfoSortKeys {
    Id,
    #[default]
    FirstName,
    LastName,
    Reason,
    FromDate,
    UntilDate,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct MemberCheckedInInfo {
    pub id: MemberId,
    pub first_name: MemberFirstName,
    pub last_name: MemberLastName,
    pub checked_in: Option<HappeningShortInfo>,
    pub checked_in_before: Option<HappeningShortInfo>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct NewGroup {
    pub name: GroupName,
    pub responsibles: Option<Vec<MemberId>>,
    pub participants: Option<Vec<MemberId>>,
    pub happenings: Option<Vec<HappeningId>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct Group {
    pub id: GroupId,
    pub name: GroupName,
    pub responsibles: Option<Vec<MemberId>>,
    pub participants: Option<Vec<MemberId>>,
    pub happenings: Option<Vec<HappeningId>>,
}

#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct GroupShortInfo {
    pub id: GroupId,
    pub name: GroupName,
}

#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct HappeningPropsMinimum {
    pub from: DateTime<Utc>,
    pub to: DateTime<Utc>,
    pub auto_checkout: Option<i64>,
}

#[derive(Serialize, Deserialize, ToSchema, FromQueryResult)]
pub struct HappeningPropsFrom {
    pub from: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct NewHappening {
    pub name: HappeningName,
    #[schema(value_type = Object)]
    pub props: Option<JsonValue>,
    pub responsibles: Option<Vec<MemberId>>,
    pub participants: Option<Vec<MemberId>>,
    pub groups: Option<Vec<GroupId>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct Happening {
    pub id: HappeningId,
    pub name: HappeningName,
    #[schema(value_type = Object)]
    pub props: Option<JsonValue>,
    pub responsibles: Option<Vec<MemberId>>,
    pub participants: Option<Vec<MemberId>>,
    pub groups: Option<Vec<GroupId>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct HappeningShortInfo {
    pub id: HappeningId,
    pub name: HappeningName,
    pub props: Option<HappeningPropsMinimum>,
}

#[derive(Default, Serialize, Deserialize, ToSchema)]
pub enum HappeningShortInfoSortKeys {
    Id,
    #[default]
    Name,
    From,
    To,
}

pub struct Action;

pub struct Check {
    happening_id: HappeningId,
    check_type: CheckType,
    date: DateTime<Utc>,
}

#[derive(Serialize, Deserialize, ToSchema, Clone)]
pub enum AbsentReason {
    Excused,
    Sicknote,
    Lunchbreak,
    Educational,
    Classchange,
    Other,
}

// NOTE: for now we skip the notes
#[derive(Serialize, Deserialize, ToSchema)]
pub struct Absent {
    pub id: AbsentId,
    pub member_id: MemberId,
    pub reason: AbsentReason,
    pub from_date: DateTime<Utc>,
    pub until_date: DateTime<Utc>,
    pub canceled: bool,
    pub creator: Option<MemberId>,
}

impl From<String> for AbsentReason {
    fn from(val: String) -> Self {
        match val.to_lowercase().as_str() {
            "excused" => Self::Excused,
            "sicknote" => Self::Sicknote,
            "lunchbreak" => Self::Lunchbreak,
            "educational" => Self::Educational,
            "classchange" => Self::Classchange,
            _ => Self::Other,
        }
    }
}

impl From<&str> for AbsentReason {
    fn from(val: &str) -> Self {
        match val {
            "Excused" => Self::Excused,
            "Sicknote" => Self::Sicknote,
            "Lunchbreak" => Self::Lunchbreak,
            "Educational" => Self::Educational,
            "Classchange" => Self::Classchange,
            _ => Self::Other,
        }
    }
}

impl From<ModelAbsentReason> for AbsentReason {
    fn from(val: ModelAbsentReason) -> Self {
        match val {
            ModelAbsentReason::Excused => Self::Excused,
            ModelAbsentReason::Sicknote => Self::Sicknote,
            ModelAbsentReason::Lunchbreak => Self::Lunchbreak,
            ModelAbsentReason::Educational => Self::Educational,
            ModelAbsentReason::Classchange => Self::Classchange,
            ModelAbsentReason::Other => Self::Other,
        }
    }
}

impl From<AbsentReason> for ModelAbsentReason {
    fn from(val: AbsentReason) -> Self {
        match val {
            AbsentReason::Excused => Self::Excused,
            AbsentReason::Sicknote => Self::Sicknote,
            AbsentReason::Lunchbreak => Self::Lunchbreak,
            AbsentReason::Educational => Self::Educational,
            AbsentReason::Classchange => Self::Classchange,
            AbsentReason::Other => Self::Other,
        }
    }
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct NewAbsent {
    pub member_id: MemberId,
    pub reason: AbsentReason,
    pub from_date: DateTime<Utc>,
    pub until_date: DateTime<Utc>,
    pub canceled: bool,
}

#[derive(Serialize, ToSchema)]
pub struct HappeningPresenceInfo {
    pub id: HappeningId,
    pub name: HappeningName,
    pub props: HappeningPropsMinimum,
    pub absent_entries: Vec<AbsentInfo>,
    pub checked_ins: Vec<DateTime<Utc>>,
    pub checked_outs: Vec<DateTime<Utc>>,
}

#[derive(Serialize, ToSchema)]
pub struct History {
    pub member_id: MemberId,
    pub happening_presences: Vec<HappeningPresenceInfo>,
}

macro_rules! impl_from_to_inner_and_trygetable {
    ($name:ident, $inner:ident) => {
        impl From<$inner> for $name {
            fn from(val: $inner) -> Self {
                Self(val)
            }
        }

        impl From<&$inner> for $name {
            fn from(val: &$inner) -> Self {
                $name(val.to_owned())
            }
        }

        impl From<$name> for $inner {
            fn from(val: $name) -> Self {
                val.0
            }
        }

        impl From<&$name> for $inner {
            fn from(val: &$name) -> Self {
                val.0.to_owned()
            }
        }

        impl TryGetable for $name {
            fn try_get_by<I: sea_orm::ColIdx>(
                res: &sea_orm::QueryResult,
                index: I,
            ) -> Result<Self, sea_orm::TryGetError> {
                match $inner::try_get_by(res, index) {
                    Ok(v) => Ok($name(v)),
                    Err(e) => Err(e),
                }
            }
        }
    };
}
impl_from_to_inner_and_trygetable!(MemberId, Uuid);
impl_from_to_inner_and_trygetable!(GroupId, Uuid);
impl_from_to_inner_and_trygetable!(HappeningId, Uuid);
impl_from_to_inner_and_trygetable!(MemberIdentityId, Uuid);
impl_from_to_inner_and_trygetable!(MemberPhysicalId, Uuid);
impl_from_to_inner_and_trygetable!(AbsentId, Uuid);
impl_from_to_inner_and_trygetable!(MemberFirstName, String);
impl_from_to_inner_and_trygetable!(MemberMiddleNames, String);
impl_from_to_inner_and_trygetable!(MemberLastName, String);
impl_from_to_inner_and_trygetable!(MemberPersonQualifier, String);
impl_from_to_inner_and_trygetable!(GroupName, String);
impl_from_to_inner_and_trygetable!(HappeningName, String);
impl_from_to_inner_and_trygetable!(Contact, JsonValue);

// FIXME: actix-web-middleware-keycloak-auth uses an old version of uuid
impl TryFrom<String> for MemberIdentityId {
    type Error = crate::error::Error;
    fn try_from(uuid_string: String) -> Result<Self, Self::Error> {
        Ok(Self(Uuid::parse_str(&uuid_string).map_err(|_| {
            Self::Error::BadRequest("failed to parse uuid")
        })?))
    }
}

impl TryGetable for AbsentReason {
    fn try_get_by<I: sea_orm::ColIdx>(
        res: &sea_orm::QueryResult,
        index: I,
    ) -> Result<Self, sea_orm::TryGetError> {
        match String::try_get_by(res, index) {
            Ok(v) => Ok(v.as_str().into()),
            Err(e) => Err(e),
        }
    }
}

// FIXME: struct for temporary fix
#[derive(Serialize, Deserialize)]
pub struct GroupInfo {
    pub id: Option<GroupId>,
    pub name: Option<GroupName>,
}

impl From<MemberInfoAbsentCheckedIn> for MemberInfo {
    fn from(miac: MemberInfoAbsentCheckedIn) -> Self {
        let absent = if let (Some(id), Some(reason), Some(from_date), Some(until_date)) = (
            miac.absent_id,
            miac.absent_reason,
            miac.absent_from_date,
            miac.absent_until_date,
        ) {
            Some(AbsentInfo {
                id,
                reason: reason.into(),
                from_date,
                until_date,
            })
        } else {
            None
        };

        let checked_in = if let (Some(id), Some(name), Some(from), Some(to)) = (
            miac.happening_id,
            miac.happening_name,
            miac.happening_from_date,
            miac.happening_to_date,
        ) {
            Some(HappeningShortInfo {
                id,
                name,
                props: Some(HappeningPropsMinimum {
                    from,
                    to,
                    auto_checkout: miac.happening_auto_checkout,
                }),
            })
        } else {
            None
        };

        // FIXME: for now parse into GroupInfo
        debug!("groups: {:?}", miac.groups);
        let groups: Option<Vec<GroupShortInfo>> = if let Some(gps) = miac.groups {
            serde_json::from_str(&gps)
                .map(|v: Vec<GroupInfo>| {
                    v.into_iter()
                        .filter_map(|x| {
                            if let (Some(id), Some(name)) = (x.id, x.name) {
                                Some(GroupShortInfo { id, name })
                            } else {
                                None
                            }
                        })
                        .collect()
                })
                .ok()
        } else {
            None
        };

        Self {
            id: miac.id,
            first_name: miac.first_name,
            middle_names: miac.middle_names,
            last_name: miac.last_name,
            person_qualifier: miac.person_qualifier,
            groups,
            absent,
            checked_in,
            checked_in_before: None,
            contact: miac.contact,
        }
    }
}
