ALTER TABLE "members" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "members" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "groups" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "groups" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "happenings" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "happenings" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "contact" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "contact" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "member_notes_list" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "member_notes_list" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "notes" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "notes" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "member_is_to_member" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "member_is_to_member" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "member_is_to_group" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "member_is_to_group" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "member_is_to_happening" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "member_is_to_happening" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "groups_happenings" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "groups_happenings" ALTER COLUMN "updated_at" TYPE TIMESTAMP;

ALTER TABLE "absents" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "absents" ALTER COLUMN "updated_at" TYPE TIMESTAMP;
ALTER TABLE "absents" ALTER COLUMN "from_date" TYPE TIMESTAMP;
ALTER TABLE "absents" ALTER COLUMN "until_date" TYPE TIMESTAMP;

ALTER TABLE "check_in_out" ALTER COLUMN "created_at" TYPE TIMESTAMP;
ALTER TABLE "check_in_out" ALTER COLUMN "updated_at" TYPE TIMESTAMP;
ALTER TABLE "check_in_out" ALTER COLUMN "date" TYPE TIMESTAMP;
