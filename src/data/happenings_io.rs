use super::{
    sorting::{HappeningShortInfoSortColumns, Sorting},
    GroupId, HappeningId, HappeningPropsMinimum, HappeningShortInfo, MemberId, TimeFrame,
};
use crate::error::Error;
use crate::model::{prelude::*, *};
use chrono::{DateTime, Utc};
use sea_orm::{
    query::Order, DatabaseConnection, DbBackend, EntityTrait, FromQueryResult, Statement,
};
use tracing::{debug, info};

type JsonValue = serde_json::Value;

#[derive(FromQueryResult)]
#[allow(dead_code)]
struct HappeningFromQueryResult {
    id: uuid::Uuid,
    name: String,
    props: JsonValue,
}

impl HappeningShortInfo {
    pub async fn load(conn: &DatabaseConnection, id: &HappeningId) -> Result<Self, Error> {
        Happenings::find_by_id(id.0)
            .one(conn)
            .await?
            .map(|h| HappeningShortInfo {
                id: h.id.into(),
                name: h.name.into(),
                props: serde_json::from_value::<HappeningPropsMinimum>(h.props.into()).ok(),
            })
            .ok_or(Error::NotFound("Member not found"))
    }

    pub async fn load_by_responsible_and_timeframe(
        conn: &DatabaseConnection,
        responsible: MemberId,
        timeframe: TimeFrame,
        sorting: Sorting<HappeningShortInfoSortColumns>,
    ) -> Result<Vec<Self>, Error> {
        Ok(
            HappeningFromQueryResult::find_by_statement(Statement::from_sql_and_values(
                DbBackend::Postgres,
                format!(
                    r#"
                    SELECT
                        happenings.id,
                        happenings.name,
                        happenings.props,
                        happenings.props->>'from' as happening_from,
                        happenings.props->>'to' as happening_to
                    FROM
                        happenings
                    INNER JOIN
                        -- combined table of member_is_to_happening and member_is_to_group-groups_happenings
                        (
                            SELECT
                                member_is_to_happening.source_member_id,
                                member_is_to_happening.target_happening_id,
                                member_is_to_happening.role::TEXT
                            FROM
                                member_is_to_happening
                            UNION ALL
                            SELECT
                                member_is_to_group.source_member_id,
                                groups_happenings.happening_id,
                                member_is_to_group.role::TEXT
                            FROM
                                member_is_to_group
                            INNER JOIN
                                groups_happenings
                            ON
                                member_is_to_group.target_group_id = groups_happenings.group_id
                        ) AS member_is_to
                        ON
                            member_is_to.source_member_id = $1
                            AND member_is_to.target_happening_id = happenings.id
                            AND member_is_to.role = 'responsible'
                    WHERE
                        (
                            TO_TIMESTAMP(
                                happenings.props->>'from',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' >= TO_TIMESTAMP(
                                $2,
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc'
                        )
                        AND (
                            TO_TIMESTAMP(
                                happenings.props->>'to',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' <= TO_TIMESTAMP(
                                $3,
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc'
                        )
                    ORDER BY
                        {} {}
                    "#,
                    match sorting.sort_by {
                        HappeningShortInfoSortColumns::Happenings(happenings::Column::Id) => {
                            "happenings.id"
                        }
                        HappeningShortInfoSortColumns::Happenings(happenings::Column::Name) => {
                            "happenings.name"
                        }
                        HappeningShortInfoSortColumns::HappeningProps("from") => "happening_from",
                        HappeningShortInfoSortColumns::HappeningProps("to") => "happening_to",
                        _ => unreachable!(),
                    },
                    match sorting.order.into() {
                        Order::Asc => "ASC",
                        Order::Desc => "DESC",
                        _ => unreachable!(),
                    }
                )
                .as_str(),
                [
                    responsible.0.into(),
                    // FIXME: the debug formatter generates the format we want to have
                    format!("{:?}", timeframe.not_before).into(),
                    format!("{:?}", timeframe.not_after).into(),
                ],
            ))
            .all(conn)
            .await?
            .into_iter()
            .map(|h| Self {
                id: h.id.into(),
                name: h.name.into(),
                props: serde_json::from_value::<HappeningPropsMinimum>(h.props).ok(),
            })
            .collect(),
        )
    }

    pub async fn load_by_participant_and_timeframe(
        conn: &DatabaseConnection,
        participant: &MemberId,
        timeframe: &TimeFrame,
    ) -> Result<Vec<Self>, Error> {
        Ok(
            HappeningFromQueryResult::find_by_statement(Statement::from_sql_and_values(
                DbBackend::Postgres,
                r#"
                SELECT
                    happenings.id,
                    happenings.name,
                    happenings.props,
                    happenings.props->>'from' as happening_from,
                    happenings.props->>'to' as happening_to
                FROM
                    happenings
                INNER JOIN
                    -- combined table of member_is_to_happening and member_is_to_group-groups_happenings
                    (
                        SELECT
                            member_is_to_happening.source_member_id,
                            member_is_to_happening.target_happening_id,
                            member_is_to_happening.role::TEXT
                        FROM
                            member_is_to_happening
                        UNION ALL
                        SELECT
                            member_is_to_group.source_member_id,
                            groups_happenings.happening_id,
                            member_is_to_group.role::TEXT
                        FROM
                            member_is_to_group
                        INNER JOIN
                            groups_happenings
                        ON
                            member_is_to_group.target_group_id = groups_happenings.group_id
                    ) AS member_is_to
                    ON
                        member_is_to.source_member_id = $1
                        AND member_is_to.target_happening_id = happenings.id
                        AND member_is_to.role = 'participant'
                WHERE
                    (
                        TO_TIMESTAMP(
                            happenings.props->>'from',
                            'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                        )::timestamp at time zone 'utc' >= TO_TIMESTAMP(
                            $2,
                            'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                        )::timestamp at time zone 'utc'
                    )
                    AND (
                        TO_TIMESTAMP(
                            happenings.props->>'to',
                            'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                        )::timestamp at time zone 'utc' <= TO_TIMESTAMP(
                            $3,
                            'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                        )::timestamp at time zone 'utc'
                    )
                "#
                .to_string(),
                [
                    participant.0.into(),
                    // FIXME: the debug formatter generates the format we want to have
                    format!("{:?}", timeframe.not_before).into(),
                    format!("{:?}", timeframe.not_after).into(),
                ],
            ))
            .all(conn)
            .await?
            .into_iter()
            .map(|h| Self {
                id: h.id.into(),
                name: h.name.into(),
                props: serde_json::from_value::<HappeningPropsMinimum>(h.props).ok(),
            })
            .collect(),
        )
    }

    pub async fn load_by_group_and_active_time(
        conn: &DatabaseConnection,
        groups: &[GroupId],
        now: DateTime<Utc>,
    ) -> Result<Vec<Self>, Error> {
        info!("number of groups: {}", groups.len());
        //let gps = Into::<sea_orm::Value>::into(groups.iter().map(|gid| gid.0).collect::<Vec<_>>());
        //info!("groups: {}", gps.to_string());
        //(groups_happenings.group_id = ANY(CAST(ARRAY['d3152253-b7c1-48d4-ac4e-16687427a137'] AS UUID ARRAY)));

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                    SELECT
                        happenings.id,
                        happenings.name,
                        happenings.props
                    FROM
                        happenings
                        INNER JOIN groups_happenings
                            ON groups_happenings.happening_id = happenings.id
                            AND (groups_happenings.group_id = ANY(CAST($1 AS UUID ARRAY)))
                    WHERE
                        (
                            TO_TIMESTAMP(
                                happenings.props->>'from',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' <= TO_TIMESTAMP(
                                $2,
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc'
                        )
                        AND (
                            TO_TIMESTAMP(
                                happenings.props->>'to',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' >= TO_TIMESTAMP(
                                $2,
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc'
                        )
                "#,
            [
                Into::<sea_orm::Value>::into(groups.iter().map(|gid| gid.0).collect::<Vec<_>>()),
                // FIXME: the debug formatter generates the format we want to have
                format!("{:?}", now).into(),
            ],
        );
        debug!("load_by_group_and_active_time query: {}", query);

        Ok(HappeningFromQueryResult::find_by_statement(query)
            .all(conn)
            .await?
            .into_iter()
            .map(|h| Self {
                id: h.id.into(),
                name: h.name.into(),
                props: serde_json::from_value::<HappeningPropsMinimum>(h.props).ok(),
            })
            .collect())
    }
}
