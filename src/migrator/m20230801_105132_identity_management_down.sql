ALTER TABLE "members" RENAME COLUMN "identity_id" TO "keycloak_id";
ALTER TABLE "members" DROP COLUMN "middle_names";
ALTER TABLE "members" DROP COLUMN "person_qualifier";

DROP TABLE physical_ids;
