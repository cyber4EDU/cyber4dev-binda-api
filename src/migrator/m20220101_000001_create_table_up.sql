--
-- Enums
--

CREATE TYPE member_role AS ENUM ('responsible');

CREATE TYPE group_role AS ENUM ('responsible', 'participant');

CREATE TYPE happening_role AS ENUM ('responsible', 'participant');

CREATE TYPE check_type AS ENUM ('in', 'out', 'close');

CREATE TYPE member_notes_list_type AS ENUM ('member', 'absent', 'check_in_out');

CREATE TYPE absent_reason AS ENUM ('ill', 'holidays', 'released', 'late');

--
-- Objects
--

CREATE TABLE members (
    id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    keycloak_id UUID UNIQUE
);

CREATE TABLE groups (
    id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    name TEXT NOT NULL
);

CREATE TABLE happenings (
    id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    name TEXT NOT NULL,
    props JSONB,
    checked_in_filter BYTEA
);

CREATE TABLE contact (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    member_id UUID NOT NULL,
    type TEXT NOT NULL,
    value TEXT NOT NULL,
    FOREIGN KEY (member_id) REFERENCES members (id)
);

CREATE TABLE member_notes_list (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    member_id UUID NOT NULL,
    type MEMBER_NOTES_LIST_TYPE NOT NULL,
    FOREIGN KEY (member_id) REFERENCES members (id)
);

CREATE TABLE notes (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    note TEXT NOT NULL,
    author_member_id UUID NOT NULL,
    member_notes_list_id INTEGER NOT NULL,
    FOREIGN KEY (author_member_id) REFERENCES members (id),
    FOREIGN KEY (member_notes_list_id) REFERENCES member_notes_list (id)
);

--
-- Relations
--

-- member -> member
CREATE TABLE member_is_to_member (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    source_member_id UUID NOT NULL,
    target_member_id UUID NOT NULL,
    role MEMBER_ROLE NOT NULL,
    FOREIGN KEY (source_member_id) REFERENCES members (id),
    FOREIGN KEY (target_member_id) REFERENCES members (id)
);

-- member -> group
CREATE TABLE member_is_to_group (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    source_member_id UUID NOT NULL,
    target_group_id UUID NOT NULL,
    role GROUP_ROLE NOT NULL,
    FOREIGN KEY (source_member_id) REFERENCES members (id),
    FOREIGN KEY (target_group_id) REFERENCES groups (id)
);

-- member -> happening
CREATE TABLE member_is_to_happening (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    source_member_id UUID NOT NULL,
    target_happening_id UUID NOT NULL,
    role HAPPENING_ROLE NOT NULL,
    FOREIGN KEY (source_member_id) REFERENCES members (id),
    FOREIGN KEY (target_happening_id) REFERENCES happenings (id)
);

-- groups - happenings
CREATE TABLE groups_happenings (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    group_id UUID NOT NULL,
    happening_id UUID NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups (id),
    FOREIGN KEY (happening_id) REFERENCES happenings (id)
);


--
-- Actions
--

CREATE TABLE absents (
    id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    member_id UUID NOT NULL,
    reason ABSENT_REASON NOT NULL,
    from_date TIMESTAMP NOT NULL,
    until_date TIMESTAMP NOT NULL,
    canceled BOOLEAN NOT NULL,
    member_notes_list_id INTEGER,
    note TEXT,
    FOREIGN KEY (member_id) REFERENCES members (id),
    FOREIGN KEY (member_notes_list_id) REFERENCES member_notes_list (id)
);

CREATE TABLE check_in_out (
    id SERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMP DEFAULT current_timestamp NOT NULL,
    member_id UUID,
    happening_id UUID NOT NULL,
    date TIMESTAMP NOT NULL,
    check_type CHECK_TYPE NOT NULL,
    delay_minutes SMALLINT,
    FOREIGN KEY (member_id) REFERENCES members (id),
    FOREIGN KEY (happening_id) REFERENCES happenings (id)
);

--
-- updated_at trigger
--

CREATE FUNCTION update_updated_at()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

DO $$
DECLARE
    t TEXT;
BEGIN
    FOR t IN
        SELECT table_name FROM information_schema.columns WHERE column_name = 'updated_at'
    LOOP
        EXECUTE format('CREATE TRIGGER trigger_update_timestamp
                        BEFORE UPDATE ON %I
                        FOR EACH ROW EXECUTE PROCEDURE update_updated_at()', t);
    END LOOP;
END;
$$ LANGUAGE 'plpgsql';
