ALTER TABLE "check_in_out" DROP CONSTRAINT "physical_id_fk_constraint";
ALTER TABLE "check_in_out" DROP COLUMN "physical_id";

ALTER TABLE "absents" DROP CONSTRAINT "creator_fk_constraint";
ALTER TABLE "absents" DROP COLUMN "creator";
