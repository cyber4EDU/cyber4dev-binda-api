use super::responses::{JsonCreated, JsonOk};
use crate::auth::get_sub_from_token_unverified;
use crate::data::{Absent, AbsentId, MemberId, MemberIdVariant, NewAbsent};
use crate::error::Error;
use actix_web::{delete, get, post, put};
use actix_web::{web, HttpResponse};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use sea_orm::DatabaseConnection;

/// Create absent entry
#[utoipa::path(
    context_path = "/absent",
    tag = "absent",
    request_body = NewAbsent,
    responses(
        (status = 201, description = "Absent created", body = Absent),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("")]
async fn post_absent(
    auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    data: web::Json<NewAbsent>,
) -> Result<JsonCreated<Absent>, Error> {
    Ok(JsonCreated::from(
        data.into_inner()
            .insert(
                &conn,
                MemberIdVariant::Iid(&get_sub_from_token_unverified(auth)?),
            )
            .await?,
    ))
}

/// Get absent entry
#[utoipa::path(
    context_path = "/absent",
    tag = "absent",
    responses(
        (status = 200, description = "Absent returned", body = Absent),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = AbsentId, Path, description = "Absent id")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/{id}")]
async fn get_absent(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<AbsentId>,
) -> Result<JsonOk<Absent>, Error> {
    let abs = Absent::load(&conn, id.into_inner()).await?;
    Ok(JsonOk::from(abs))
}

/// Update absent entry
#[utoipa::path(
    context_path = "/absent",
    tag = "absent",
    request_body = Absent,
    responses(
        (status = 200, description = "Object updated", body = Absent),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[put("")]
async fn put_absent(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    data: web::Json<Absent>,
) -> Result<JsonOk<Absent>, Error> {
    Ok(JsonOk::from(data.into_inner().update(&conn).await?))
}

/// Delete absent entry
#[utoipa::path(
    context_path = "/absent",
    tag = "absent",
    responses(
        (status = 204, description = "Object deactivated"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = AbsentId, Path, description = "Absent id")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[delete("/{id}")]
async fn delete_absent(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<AbsentId>,
) -> Result<HttpResponse, Error> {
    Absent::delete(&conn, id.into_inner()).await?;
    Ok(HttpResponse::NoContent().finish())
}

/// Get all absent entries of a member
#[utoipa::path(
    context_path = "/absents",
    tag = "absent",
    responses(
        (status = 200, description = "Absents returned", body = Vec<Absent>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = MemberId, Path, description = "Member id")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/{id}")]
async fn get_absents_member(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<MemberId>,
) -> Result<JsonOk<Vec<Absent>>, Error> {
    let abs = Absent::load_by_member(&conn, id.into_inner()).await?;
    Ok(JsonOk::from(abs))
}
