use super::{Absent, AbsentId, MemberId, MemberIdentityId, TimeFrame};
use crate::error::Error;
use crate::model::{prelude::*, *};
use sea_orm::{
    ActiveModelTrait, ActiveValue, ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter,
};

impl Absent {
    pub async fn load_by_member(
        conn: &DatabaseConnection,
        member_id: MemberId,
    ) -> Result<Vec<Self>, Error> {
        // first check if member-id is present
        let _ = Members::find_by_id(member_id.0)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Member not found"))?;

        Ok(Absents::find()
            .filter(absents::Column::MemberId.eq(member_id.0))
            .all(conn)
            .await?
            .into_iter()
            .map(|abs| Self {
                id: abs.id.into(),
                member_id: abs.member_id.into(),
                reason: abs.reason.into(),
                from_date: abs.from_date.into(),
                until_date: abs.until_date.into(),
                canceled: abs.canceled,
                creator: abs.creator.map(Into::into),
            })
            .collect())
    }

    pub async fn load_by_member_and_timeframe(
        conn: &DatabaseConnection,
        member_id: &MemberId,
        timeframe: &TimeFrame,
    ) -> Result<Vec<Self>, Error> {
        Ok(Absents::find()
            .filter(absents::Column::MemberId.eq(member_id.0))
            .filter(absents::Column::UntilDate.gte(timeframe.not_before))
            .filter(absents::Column::FromDate.lte(timeframe.not_after))
            .all(conn)
            .await?
            .into_iter()
            .map(|abs| Self {
                id: abs.id.into(),
                member_id: abs.member_id.into(),
                reason: abs.reason.into(),
                from_date: abs.from_date.into(),
                until_date: abs.until_date.into(),
                canceled: abs.canceled,
                creator: abs.creator.map(Into::into),
            })
            .collect())
    }

    #[allow(dead_code)]
    pub async fn try_update(
        self,
        conn: &DatabaseConnection,
        _updater: MemberIdentityId,
    ) -> Result<Absent, Error> {
        let abs = Absents::find_by_id(self.id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Absent not found"))?;

        // FIXME: every teacher is able to update any entry for now
        // let updater = MemberId::load_from(conn, updater).await?;

        // if abs.creator != Some(updater.0) {
        //     return Err(Error::Forbidden(
        //         "Only creator or admin can update absent entry",
        //     ));
        // }

        let id = abs.id;
        absents::ActiveModel {
            id: ActiveValue::Set(id),
            member_id: ActiveValue::Set(self.member_id.into()),
            reason: ActiveValue::Set(self.reason.clone().into()),
            from_date: ActiveValue::Set(self.from_date.into()),
            until_date: ActiveValue::Set(self.until_date.into()),
            canceled: ActiveValue::Set(self.canceled),
            creator: ActiveValue::Set(self.creator.map(Into::into)),
            ..ActiveModelTrait::default()
        }
        .update(conn)
        .await?;
        Ok(self)
    }

    #[allow(dead_code)]
    pub async fn deactivate(conn: &DatabaseConnection, id: AbsentId) -> Result<Self, Error> {
        let abs = Absents::find_by_id(id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Absent not found"))?;

        absents::ActiveModel {
            id: ActiveValue::Set(abs.id),
            member_id: ActiveValue::Set(abs.member_id),
            reason: ActiveValue::Set(abs.reason.clone()),
            from_date: ActiveValue::Set(abs.from_date),
            until_date: ActiveValue::Set(abs.until_date),
            canceled: ActiveValue::Set(true),
            creator: ActiveValue::Set(abs.creator),
            ..ActiveModelTrait::default()
        }
        .update(conn)
        .await?;

        Ok(Self {
            id: abs.id.into(),
            member_id: abs.member_id.into(),
            reason: abs.reason.into(),
            from_date: abs.from_date.into(),
            until_date: abs.until_date.into(),
            canceled: true,
            creator: abs.creator.map(Into::into),
        })
    }
}
