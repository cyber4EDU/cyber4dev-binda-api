#![macro_use]

use crate::config::Config;
use crate::data::MemberIdentityId;
use actix_web::dev::ServiceRequest;
use actix_web_httpauth::extractors::bearer::BearerAuth;
use jsonwebtoken::{decode, decode_header, DecodingKey, Validation};
use serde::Deserialize;
use std::io::{
    Error,
    ErrorKind::{ConnectionRefused, InvalidData},
};
use tracing::debug;
use uuid::Uuid;

#[derive(Clone)]
pub struct Auth {
    pub_key: String,
}

#[derive(Debug, Deserialize)]
pub struct OIDCKeyResponse {
    keys: Vec<OIDCKey>,
}

#[expect(dead_code)]
#[derive(Debug, Deserialize)]
pub struct OIDCKey {
    kid: String,
    kty: String,
    alg: String,
    #[serde(rename = "use")]
    usage: String,
    n: String,
    e: String,
    x5c: Vec<String>,
    x5t: String,
    #[serde(rename = "x5t#S256")]
    x5t_s256: String,
}

#[expect(dead_code)]
#[derive(Debug, Deserialize)]
pub struct RealmAccess {
    pub roles: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct Claims {
    pub binda_roles: Vec<String>,
    #[expect(dead_code)]
    pub realm_access: Option<RealmAccess>,
    pub sub: Uuid,
}

impl Claims {
    pub fn has_role(&self, role: &str) -> bool {
        self.binda_roles.contains(&role.to_string())
    }
}

impl Auth {
    pub async fn new() -> Result<Self, Error> {
        // TODO:
        // - map: kid -> pub_key
        // - fetch public key at request
        let client = awc::Client::default();
        let req = client
            .get(Config::get_oidc_pub_key_url())
            .insert_header(("Content-Type", "application/json"));
        let mut res = req
            .send()
            .await
            .map_err(|_| Error::new(ConnectionRefused, "Failed to send request ot keycloak"))?;
        let keys: OIDCKeyResponse = res
            .json::<OIDCKeyResponse>()
            .await
            .map_err(|_| Error::new(InvalidData, "Failed to parse OIDCKeyResponse."))?;
        let key_string = &keys
            .keys
            .iter()
            .find(|key| key.usage == "sig")
            .ok_or(Error::new(
                InvalidData,
                "Could not find signing key in OIDCKeyResponse.",
            ))?
            .x5c[0];
        debug!("KC keys: {:?}", key_string);
        Ok(Self {
            pub_key: format!(
                "-----BEGIN PUBLIC KEY-----{}-----END PUBLIC KEY-----",
                &key_string
            ),
        })
    }
}

pub fn validator(
    req: ServiceRequest,
    credentials: BearerAuth,
    auth: &Auth,
) -> Result<(Claims, ServiceRequest), (actix_web::error::Error, ServiceRequest)> {
    match decode_header(credentials.token()) {
        Ok(jwt_header) => {
            println!("header: {:?}", jwt_header);

            match decode::<Claims>(
                credentials.token(),
                &DecodingKey::from_rsa_pem(auth.pub_key.as_bytes()).unwrap(),
                &Validation::new(jwt_header.alg),
            ) {
                Ok(token) => Ok((token.claims, req)),
                Err(err) => Err((actix_web::error::ErrorUnauthorized(err), req)),
            }
        }
        Err(err) => Err((actix_web::error::ErrorUnauthorized(err), req)),
    }
}

pub fn get_sub_from_token_unverified(
    auth: BearerAuth,
) -> Result<MemberIdentityId, crate::error::Error> {
    let head = jsonwebtoken::decode_header(auth.token())
        .map_err(|_| crate::error::Error::BadRequest("Failed to decode jwt head"))?;
    let mut validation = jsonwebtoken::Validation::new(head.alg);
    validation.insecure_disable_signature_validation();
    Ok(jsonwebtoken::decode::<crate::auth::Claims>(
        auth.token(),
        &jsonwebtoken::DecodingKey::from_secret(&[]),
        &validation,
    )
    .map_err(|_| crate::error::Error::BadRequest("Failed to decode jwt"))?
    .claims
    .sub
    .into())
}

macro_rules! http_auth {
    ($auth_key: expr, $condition: expr) => {{
        let auth_key = $auth_key.clone();
        actix_web_httpauth::middleware::HttpAuthentication::bearer(move |req, cred| {
            let auth_key = auth_key.clone();
            async move {
                match crate::auth::validator(req, cred, &auth_key) {
                    Ok((claim, req)) => {
                        if $condition(claim) {
                            Ok(req)
                        } else {
                            Err((
                                actix_web::error::ErrorForbidden("Required role not found"),
                                req,
                            ))
                        }
                    }
                    Err(err) => Err(err),
                }
            }
        })
    }};
}

pub(crate) use http_auth;
