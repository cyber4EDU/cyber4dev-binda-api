#!/usr/bin/env bash

set -e

trap clean_up INT
clean_up() {
    echo "stop binda_api"
    killall --quiet binda_api | true
}

export KEYCLOAK_URL="http://localhost:8282"
export KEYCLOAK_PUB_KEY_URL="${KEYCLOAK_URL}/realms/esbz/protocol/openid-connect/certs"

#export TMPDIR=$(mktemp -d /tmp/binda_api.XXXX)
export TMPDIR="/tmp/binda_api_test"
mkdir -p "${TMPDIR}"
export THISDIR=$(dirname $(readlink -f $0))
export MODE="dev"

RUN_ONLY_TEST=$1

if [ -z ${DATABASE_URL} ];
then
    if [ -z ${PGUSER} ] || [ -z ${PGPASSWORD} ] || [ -z ${PGDATABASE} ];
    then
        echo "start script from within pg_virtualenv"
        exit 1
    else
        # use postgres from e.g. pg_virtualenv
        export PORT=8001
        export DATABASE_URL="postgres://$PGUSER:$PGPASSWORD@localhost/$PGDATABASE"
    fi
else
    if [ -z ${PORT} ];
    then
        echo "specify PORT"
        exit 1
    fi
    echo "using DATABASE_URL: \"${DATABASE_URL}\""
fi

if [ -z ${BINDA_RUNNING} ];
then
    cargo b

    export RUST_LOG="trace,sqlx::query=trace,tracing_actix_web::root_span_builder=warn"
    export SQLX_LOGGING=

    # kill all other binda_apis
    #killall binda_api

    export BINDA_ROLE_TEACHER="role_esbz_teacher"
    export BINDA_ROLE_ADMIN="role_binda_admin"

    #
    # start binda_api
    #
    export LOG_FILE="${TMPDIR}/binda_api.log"
    echo "run binda_api, see logs: ${LOG_FILE}"
    ${THISDIR}/../target/debug/binda_api 2&> ${LOG_FILE} &
    sleep 0.5
fi

#
# carg test params
#
BINDA_CARGO_TEST_ARGS="--include-ignored"
if [ ${BINDA_API_TEST_VERBOSE+x} ]; then
    BINDA_CARGO_TEST_ARGS="${BINDA_CARGO_TEST_ARGS} --nocapture"
fi
if [ ${RUN_ONLY_TEST} ]; then
    BINDA_CARGO_TEST_ARGS="${BINDA_CARGO_TEST_ARGS} --test ${RUN_ONLY_TEST}"
fi

#
# exit or continue
#
if [ -z ${BINDA_API_TEST_CONTINUE+x} ]; then
    #
    # run tests
    #
    if ! cargo test -- ${BINDA_CARGO_TEST_ARGS}; then
        clean_up
        exit 1
    fi
    clean_up
    exit 0
else
    #
    # run tests, but ignore results
    #
    cargo test -- ${BINDA_CARGO_TEST_ARGS} || true

    get_token() {
        CLIENT_SECRET=$1
        USERNAME=$2
        PASSWORD=$3

        curl --location --request POST "http://localhost:8282/realms/esbz/protocol/openid-connect/token" \
            --header "Content-Type: application/x-www-form-urlencoded" \
            --data-urlencode "client_id=binda-ui_access" \
            --data-urlencode "grant_type=password" \
            --data-urlencode "username=${USERNAME}" \
            --data-urlencode "password=${PASSWORD}" \
            --data-urlencode "scope=openid" \
            --data-urlencode "client_secret=${CLIENT_SECRET}"
    }

    CLIENT_SECRET="zpLfQrWxWEknlpPEaxdF6scPDw3nEvOU"

    USERNAME="testseki01"
    PASSWORD="testseki01"
    ADMIN_TOKEN=$(get_token "$CLIENT_SECRET" "$USERNAME" "$PASSWORD" | jq -r ".access_token")
    echo "====== admin token ======"
    echo "${ADMIN_TOKEN}"
    echo "========================="

    USERNAME="testteacher01"
    PASSWORD="testteacher01"
    TEACHER_TOKEN=$(get_token "$CLIENT_SECRET" "$USERNAME" "$PASSWORD" | jq -r ".access_token")
    echo "===== teacher token ====="
    echo "${TEACHER_TOKEN}"
    echo "========================="

    USERNAME="testuser001"
    PASSWORD="testuser001"
    TEACHER_TOKEN=$(get_token "$CLIENT_SECRET" "$USERNAME" "$PASSWORD" | jq -r ".access_token")
    echo "====== user token ======="
    echo "${TEACHER_TOKEN}"
    echo "========================="

    wait
fi
