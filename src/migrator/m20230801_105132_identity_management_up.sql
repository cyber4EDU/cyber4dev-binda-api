-- modify members table
ALTER TABLE "members" RENAME COLUMN "keycloak_id" TO "identity_id";
ALTER TABLE "members" ADD COLUMN "middle_names" TEXT;
ALTER TABLE "members" ADD COLUMN "person_qualifier" TEXT;

-- add table for physical ids
CREATE TABLE physical_ids (
    id UUID PRIMARY KEY NOT NULL,
    created_at TIMESTAMPTZ DEFAULT current_timestamp NOT NULL,
    updated_at TIMESTAMPTZ DEFAULT current_timestamp NOT NULL,
    member_id UUID,
    deactivated BOOLEAN NOT NULL,
    note TEXT,
    FOREIGN KEY (member_id) REFERENCES members (id)
);
