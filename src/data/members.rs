use super::{
    sorting::*, GroupId, HappeningId, MemberId, MemberIdVariant, MemberIdentityId, MemberInfo,
    MemberInfoAbsentCheckedIn,
};
use crate::api::SortOrder;
use crate::error::Error;
use crate::model::{prelude::*, *};
use chrono::{DateTime, Utc};
use indoc::indoc;
use sea_orm::{
    sea_query::IntoCondition, ColumnTrait, DatabaseConnection, DbBackend, EntityTrait,
    FromQueryResult, JoinType, QueryFilter, QuerySelect, RelationTrait, Select, Statement,
};
use tracing::debug;

impl MemberId {
    pub async fn load_from(
        conn: &DatabaseConnection,
        iid: MemberIdentityId,
    ) -> Result<Self, Error> {
        Ok(Members::find()
            .filter(members::Column::IdentityId.eq(iid.0))
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Identity id not found"))?
            .id
            .into())
    }
}

impl MemberInfo {
    fn select_with_group_info() -> &'static str {
        indoc! {r#"
            SELECT DISTINCT
                "members"."id",
                "members"."first_name",
                "members"."middle_names",
                "members"."last_name",
                "members"."person_qualifier",
                "members"."contact",
                jsonb_agg(
                    DISTINCT jsonb_build_object('id', "groups"."id", 'name', "groups"."name")
                )::text AS "groups"
            FROM "members"
            "#}
    }

    fn select_with_all_info() -> &'static str {
        indoc! {r#"
            SELECT DISTINCT
                "members"."id",
                "members"."first_name",
                "members"."middle_names",
                "members"."last_name",
                "members"."person_qualifier",
                "members"."contact",
                jsonb_agg(
                    DISTINCT jsonb_build_object('id', "groups"."id", 'name', "groups"."name")
                )::text AS "groups",
                "absents"."id" AS "absent_id",
                "absents"."reason"::TEXT AS "absent_reason",
                "absents"."from_date" AS "absent_from_date",
                "absents"."until_date" AS "absent_until_date",
                "happening"."id" AS "happening_id",
                "happening"."name" AS "happening_name",
                TO_TIMESTAMP(
                    "happening"."props"->>'from',
                    'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                )::timestamp at time zone 'utc' AS "happening_from_date",
                TO_TIMESTAMP(
                    "happening"."props"->>'to',
                    'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                )::timestamp at time zone 'utc' AS "happening_to_date",
                ("happening"."props"->>'auto_checkout')::INT8 AS "happening_auto_checkout"
            FROM "members"
            "#}
    }

    fn select_with_all_info_and_checked_in_date() -> &'static str {
        indoc! {r#"
            SELECT DISTINCT
                "members"."id",
                "members"."first_name",
                "members"."middle_names",
                "members"."last_name",
                "members"."person_qualifier",
                "members"."contact",
                jsonb_agg(
                    DISTINCT jsonb_build_object('id', "groups"."id", 'name', "groups"."name")
                )::text AS "groups",
                "absents"."id" AS "absent_id",
                "absents"."reason"::TEXT AS "absent_reason",
                "absents"."from_date" AS "absent_from_date",
                "absents"."until_date" AS "absent_until_date",
                "happening"."id" AS "happening_id",
                "happening"."name" AS "happening_name",
                TO_TIMESTAMP(
                    "happening"."props"->>'from',
                    'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                )::timestamp at time zone 'utc' AS "happening_from_date",
                TO_TIMESTAMP(
                    "happening"."props"->>'to',
                    'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                )::timestamp at time zone 'utc' AS "happening_to_date",
                ("happening"."props"->>'auto_checkout')::INT8 AS "happening_auto_checkout",
                "check_in_out"."date"
            FROM "members"
            "#}
    }

    fn left_join_group() -> &'static str {
        indoc! {r#"
            LEFT JOIN "member_is_to_group"
                ON "members"."id" = "member_is_to_group"."source_member_id"
                    AND "member_is_to_group"."role" = 'participant'
            LEFT JOIN "groups"
                ON "member_is_to_group"."target_group_id" = "groups"."id"
        "#}
    }

    fn inner_join_group() -> &'static str {
        indoc! {r#"
            INNER JOIN "member_is_to_group"
                ON "members"."id" = "member_is_to_group"."source_member_id"
                    AND "member_is_to_group"."role" = 'participant'
            INNER JOIN "groups"
                ON "member_is_to_group"."target_group_id" = "groups"."id"
        "#}
    }

    fn left_join_absent() -> &'static str {
        indoc! {r#"
            LEFT JOIN LATERAL
                (
                    SELECT
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date"
                    FROM "absents"
                    WHERE
                        "members"."id" = "absents"."member_id"
                        AND (
                            "absents"."from_date" <= $1
                            AND "absents"."until_date" >= $1
                            AND "absents"."canceled" = FALSE
                        )
                    ORDER BY "absents"."updated_at" DESC
                    LIMIT 1
                ) AS "absents" ON TRUE
        "#}
    }

    fn inner_join_absent() -> &'static str {
        indoc! {r#"
            INNER JOIN LATERAL
                (
                    SELECT
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date"
                    FROM "absents"
                    WHERE
                        "members"."id" = "absents"."member_id"
                        AND (
                            "absents"."from_date" <= $1
                            AND "absents"."until_date" >= $1
                            AND "absents"."canceled" = FALSE
                        )
                    ORDER BY "absents"."updated_at" DESC
                    LIMIT 1
                ) AS "absents" ON TRUE
        "#}
    }

    fn join_absent() -> &'static str {
        indoc! {r#"
            JOIN LATERAL
                (
                    SELECT
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date"
                    FROM "absents"
                    WHERE
                        "members"."id" = "absents"."member_id"
                        AND (
                            "absents"."from_date" <= $1
                            AND "absents"."until_date" >= $1
                            AND "absents"."canceled" = FALSE
                        )
                    ORDER BY "absents"."updated_at" DESC
                    LIMIT 1
                ) AS "absents" ON TRUE
        "#}
    }

    fn left_join_check_in_out() -> &'static str {
        indoc! {r#"
            LEFT JOIN LATERAL
                (
                    SELECT
                        "check_in_out"."happening_id",
                        "check_in_out"."check_type"
                    FROM "check_in_out"
                    WHERE
                        "check_in_out"."member_id" = "members"."id"
                        AND "check_in_out"."date" <= $1
                    ORDER BY
                        "check_in_out"."date" DESC,
                        "check_in_out"."updated_at" DESC
                    LIMIT 1
                ) AS "check_in_out" ON TRUE
        "#}
    }

    fn member_search_condition(s: Option<String>) -> String {
        match s {
            Some(s) => format!(
                indoc! {r#"(LOWER("members"."first_name") LIKE LOWER('%{}%')
                            OR LOWER("members"."last_name") LIKE LOWER('%{}%')
                            OR LOWER("members"."middle_names") LIKE LOWER('%{}%')
                            OR LOWER("members"."person_qualifier") LIKE LOWER('%{}%'))
                            "#},
                s, s, s, s
            ),
            None => "TRUE".to_string(),
        }
    }

    /// Load complete information for one member
    pub async fn load_info(
        conn: &DatabaseConnection,
        member_id: MemberIdVariant<'_>,
        now: DateTime<Utc>,
    ) -> Result<Self, Error> {
        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    {}
                    {}
                    {}
                    {}
                    LEFT JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                    {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                "#},
                Self::select_with_all_info(),
                match member_id {
                    MemberIdVariant::Id(_) => "",
                    MemberIdVariant::Iid(_) => "",
                    MemberIdVariant::Pid(_) => indoc! {
                    r#"
                        INNER JOIN "physical_ids"
                            ON "physical_ids"."member_id" = "members"."id"
                                AND "physical_ids"."id" = $2
                    "#},
                },
                Self::left_join_group(),
                Self::left_join_absent(),
                Self::left_join_check_in_out(),
                match member_id {
                    MemberIdVariant::Id(_) => r#"WHERE "members"."id" = $2"#,
                    MemberIdVariant::Iid(_) => r#"WHERE "members"."identity_id" = $2"#,
                    MemberIdVariant::Pid(_) => "",
                }
            )
            .as_str(),
            [
                now.into(),
                match member_id {
                    MemberIdVariant::Id(id) => id.0,
                    MemberIdVariant::Iid(iid) => iid.0,
                    MemberIdVariant::Pid(pid) => pid.0,
                }
                .into(),
            ],
        );

        debug!("load_info query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Member not found"))?
            .into())
    }

    /// Load complete information for all members
    pub async fn load_info_all(
        conn: &DatabaseConnection,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    {}
                    {}
                    {}
                    LEFT JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                    WHERE {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info(),
                Self::left_join_group(),
                Self::left_join_absent(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                },
            )
            .as_str(),
            [now.into()],
        );

        debug!("load_all query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all absent members
    pub async fn load_info_all_absent(
        conn: &DatabaseConnection,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    {}
                    {}
                    {}
                    LEFT JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                    WHERE
                        "happening"."id" IS NULL
                        AND {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info(),
                Self::left_join_group(),
                Self::join_absent(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into()],
        );

        debug!("load_all_absent query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all missing members
    pub async fn load_info_all_missing(
        conn: &DatabaseConnection,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    INNER JOIN member_is_to_group AS mitg
                        ON
                            mitg.source_member_id = members.id
                            AND mitg.role = 'participant'
                    INNER JOIN groups_happenings AS gh
                        ON
                            gh.group_id = mitg.target_group_id
                    INNER JOIN groups
                        ON groups.id =  gh.group_id
                    INNER JOIN happenings AS h
                        ON
                            h.id = gh.happening_id
                            AND TO_TIMESTAMP(
                                h.props->>'from',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' <= $1
                            AND TO_TIMESTAMP(
                                h.props->>'to',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' > $1
                    {}
                    -- remove who is absent
                    LEFT JOIN absents AS a
                        ON
                            a.member_id = members.id
                            AND a.from_date <= $1
                            AND a.until_date >= $1
                            AND a.canceled = FALSE
                    WHERE
                        a.id IS NULL
                        AND (
                            check_in_out.check_type IS NULL
                            OR check_in_out.check_type = 'out'
                        )
                        AND {}
                    GROUP BY
                        members.id,
                        groups.id
                    ORDER BY {} {}
                "#},
                Self::select_with_group_info(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into()],
        );

        debug!("load_all_missing query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all members of a group
    pub async fn load_info_group(
        conn: &DatabaseConnection,
        id: GroupId,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    INNER JOIN "member_is_to_group"
                        ON "members"."id" = "member_is_to_group"."source_member_id"
                            AND "member_is_to_group"."role" = 'participant'
                            AND "member_is_to_group"."target_group_id" = $2
                    INNER JOIN "groups"
                        ON "member_is_to_group"."target_group_id" = "groups"."id"
                    {}
                    {}
                    LEFT JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                    WHERE {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info(),
                Self::left_join_absent(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into(), id.0.into()],
        );

        debug!("load_info_group query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all members of a group that are checked-in
    pub async fn load_info_group_checked_in(
        conn: &DatabaseConnection,
        id: GroupId,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    INNER JOIN "member_is_to_group"
                        ON "members"."id" = "member_is_to_group"."source_member_id"
                            AND "member_is_to_group"."role" = 'participant'
                            AND "member_is_to_group"."target_group_id" = $2
                    INNER JOIN "groups"
                        ON "member_is_to_group"."target_group_id" = "groups"."id"
                    {}
                    {}
                    INNER JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                    WHERE {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info(),
                Self::left_join_absent(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into(), id.0.into()],
        );

        debug!("load_info_group_checked_in query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all members of a group that are absent
    pub async fn load_info_group_absent(
        conn: &DatabaseConnection,
        id: GroupId,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    INNER JOIN "member_is_to_group"
                        ON "members"."id" = "member_is_to_group"."source_member_id"
                    INNER JOIN "groups"
                        ON "member_is_to_group"."target_group_id" = "groups"."id"
                            AND "groups"."id" = $2
                    {}
                    {}
                    LEFT JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                    WHERE
                        "happening"."id" is NULL
                        AND {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info(),
                Self::inner_join_absent(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into(), id.0.into()],
        );

        debug!("load_info_group_absent query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all members of a group that are missing
    pub async fn load_info_group_missing(
        conn: &DatabaseConnection,
        id: GroupId,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    INNER JOIN member_is_to_group AS mitg
                        ON
                            mitg.source_member_id = members.id
                            AND mitg.role = 'participant'
                    INNER JOIN groups_happenings AS gh
                        ON
                            gh.group_id = mitg.target_group_id
                    INNER JOIN groups
                        ON groups.id =  gh.group_id
                            AND groups.id = $2
                    INNER JOIN happenings AS h
                        ON
                            h.id = gh.happening_id
                            AND TO_TIMESTAMP(
                                h.props->>'from',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' <= $1
                            AND TO_TIMESTAMP(
                                h.props->>'to',
                                'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                            )::timestamp at time zone 'utc' > $1
                    {}
                    -- remove who is absent
                    LEFT JOIN absents AS a
                        ON
                            a.member_id = members.id
                            AND a.from_date <= $1
                            AND a.until_date >= $1
                            AND a.canceled = FALSE
                    WHERE
                        a.id IS NULL
                        AND (
                            check_in_out.check_type IS NULL
                            OR check_in_out.check_type = 'out'
                        )
                        AND {}
                    GROUP BY
                        members.id,
                        groups.id
                    ORDER BY {} {}
                "#},
                Self::select_with_group_info(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into(), id.0.into()],
        );

        debug!("load_info_group_missing query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all members of a happening
    pub async fn load_info_happening(
        conn: &DatabaseConnection,
        id: HappeningId,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoSortColumn>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let MemberInfoSortColumn::Members(sort_by) = sorting.sort_by;

        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    {}
                    INNER JOIN "groups_happenings"
                        ON "groups_happenings"."happening_id" = $2
                            AND "groups_happenings"."group_id" = "groups"."id"
                    {}
                    {}
                    LEFT JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "check_in_out"."check_type" = 'in'
                            AND "happening"."id" = $2
                    WHERE {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info(),
                Self::inner_join_group(),
                Self::left_join_absent(),
                Self::left_join_check_in_out(),
                Self::member_search_condition(search),
                match sort_by {
                    members::Column::Id => "members.id",
                    members::Column::FirstName => "members.first_name",
                    members::Column::LastName => "members.last_name",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into(), id.0.into()],
        );

        debug!("load_info_happening query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }

    /// Load complete information for all members of a happening that are checked-in
    pub async fn load_info_happening_checked_in(
        conn: &DatabaseConnection,
        id: HappeningId,
        now: DateTime<Utc>,
        sorting: Sorting<MemberInfoCheckedInSortColumns>,
        search: Option<String>,
    ) -> Result<Vec<Self>, Error> {
        let query = Statement::from_sql_and_values(
            DbBackend::Postgres,
            format!(
                indoc! {r#"
                    {}
                    {}
                    INNER JOIN LATERAL
                        (
                            SELECT
                                "check_in_out"."happening_id",
                                "check_in_out"."check_type",
                                "check_in_out"."date"
                            FROM "check_in_out"
                            WHERE
                                "check_in_out"."member_id" = "members"."id"
                                AND "check_in_out"."date" <= $1
                            ORDER BY
                                "check_in_out"."date" DESC,
                                "check_in_out"."updated_at" DESC
                            LIMIT 1
                        ) AS "check_in_out" ON TRUE
                    {}
                    INNER JOIN "happenings" as "happening"
                        ON "happening"."id" = "check_in_out"."happening_id"
                            AND "happening"."id" = $2
                            AND "check_in_out"."check_type" = 'in'
                    WHERE {}
                    GROUP BY
                        "members"."id",
                        "absents"."id",
                        "absents"."reason",
                        "absents"."from_date",
                        "absents"."until_date",
                        "happening"."id",
                        "check_in_out"."date"
                    ORDER BY {} {}
                "#},
                Self::select_with_all_info_and_checked_in_date(),
                Self::inner_join_group(),
                Self::left_join_absent(),
                Self::member_search_condition(search),
                match sorting.sort_by {
                    MemberInfoCheckedInSortColumns::Members(members::Column::Id) => "members.id",
                    MemberInfoCheckedInSortColumns::Members(members::Column::FirstName) =>
                        "members.first_name",
                    MemberInfoCheckedInSortColumns::Members(members::Column::LastName) =>
                        "members.last_name",
                    MemberInfoCheckedInSortColumns::CheckInOut(check_in_out::Column::Date) =>
                        "check_in_out.\"date\"",
                    _ => unreachable!(),
                },
                match sorting.order {
                    SortOrder::Ascending => "ASC",
                    SortOrder::Descending => "DESC",
                }
            )
            .as_str(),
            [now.into(), id.0.into()],
        );

        debug!("load_info_happening_checked_in query: {}", query);

        Ok(MemberInfoAbsentCheckedIn::find_by_statement(query)
            .into_model::<MemberInfoAbsentCheckedIn>()
            .all(conn)
            .await?
            .into_iter()
            .map(|m| m.into())
            .collect())
    }
    // FIXME ================================================================= FIXME

    fn load_member_query(member_id: MemberIdVariant<'_>) -> Select<members::Entity> {
        match member_id {
            MemberIdVariant::Id(id) => Members::find_by_id(id.0),
            MemberIdVariant::Iid(iid) => {
                Members::find().filter(members::Column::IdentityId.eq(iid.0))
            }
            MemberIdVariant::Pid(pid) => {
                let pid = pid.0;
                Members::find().join(
                    JoinType::InnerJoin,
                    members::Relation::PhysicalIds
                        .def()
                        .on_condition(move |_, _| {
                            physical_ids::Column::Id.eq(pid).into_condition()
                        }),
                )
            }
        }
    }

    pub async fn load(
        conn: &DatabaseConnection,
        member_id: MemberIdVariant<'_>,
        _date: Option<DateTime<Utc>>,
    ) -> Result<Self, Error> {
        Ok(Self::load_member_query(member_id)
            // TODO: load absent info
            // TODO: load checked_in info
            .into_model::<MemberInfoAbsentCheckedIn>()
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Member not found"))?
            .into())
    }
}

#[test]
fn query() {
    use sea_orm::{
        sea_query::{self, Func, Iden, IntoCondition, SimpleExpr},
        DbBackend, JoinType, QuerySelect, QueryTrait,
    };
    use std::fs::File;
    use std::io::Write;

    let uuid = uuid::Uuid::new_v4();
    let _date_a = Utc::now();
    let _date_b = Utc::now();

    let path = "test.sql";
    let mut output = File::create(path).unwrap();

    // --
    #[derive(Iden)]
    #[iden = "ARRAY"]
    struct ArrayFunc;

    let sub_query_statement = groups::Entity::find()
        .select_only()
        .columns([groups::Column::Id, groups::Column::Name])
        .join(
            JoinType::InnerJoin,
            groups::Relation::MemberIsToGroup
                .def()
                .on_condition(move |_, _| {
                    member_is_to_group::Column::SourceMemberId
                        .eq(uuid.clone())
                        .into_condition()
                }),
        )
        //.into_tuple()
        //.into()
        .into_query()
        .into_sub_query_statement()
        .into();

    // .......... -> Select<Entity>
    // into_query -> SelectStatement (ok)
    // into_tuple -> Selector<SelectGetableTuple<_>>

    // --
    write!(
        output,
        "{}",
        members::Entity::find()
            .select_only()
            .columns([
                members::Column::Id,
                members::Column::FirstName,
                members::Column::LastName,
            ])
            .expr(
                Func::cust(ArrayFunc)
                    .arg(SimpleExpr::SubQuery(None, Box::new(sub_query_statement))),
            )
            .build(DbBackend::Postgres)
            .to_string()
    )
    .unwrap();
}

#[test]
fn query2() {
    use sea_orm::{sea_query::expr::Expr, DbBackend, JoinType, QuerySelect, QueryTrait};
    use std::fs::File;
    use std::io::Write;

    let _uuid = uuid::Uuid::new_v4();
    let _date_a = Utc::now();
    let _date_b = Utc::now();

    let path = "test2.sql";
    let mut output = File::create(path).unwrap();

    write!(
        output,
        "{}",
        members::Entity::find()
            .select_only()
            .columns([
                members::Column::Id,
                members::Column::FirstName,
                members::Column::LastName,
            ])
            .expr(Expr::cust_with_exprs(
                "json_agg(json_build_object('id', $1, 'name', $2)) AS \"groups\"",
                [
                    Expr::col((groups::Entity, groups::Column::Id)).into(),
                    Expr::col((groups::Entity, groups::Column::Name)).into()
                ]
            ))
            .join(JoinType::LeftJoin, members::Relation::MemberIsToGroup.def())
            .join(
                JoinType::LeftJoin,
                member_is_to_group::Relation::Groups.def()
            )
            .group_by(members::Column::Id)
            .build(DbBackend::Postgres)
            .to_string()
    )
    .unwrap();
}
