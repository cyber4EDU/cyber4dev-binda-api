use super::{MemberIdVariant, MemberPhysicalId, NewPhysicalData, PhysicalData};
use crate::error::Error;
use crate::model::{prelude::*, *};
use sea_orm::{ActiveModelTrait, ActiveValue, DatabaseConnection, EntityTrait};
use tracing::info;
use uuid::Uuid;

impl NewPhysicalData {
    pub async fn insert(
        self,
        conn: &DatabaseConnection,
        _creator: MemberIdVariant<'_>,
    ) -> Result<PhysicalData, Error> {
        let new_pid = physical_ids::ActiveModel {
            id: ActiveValue::Set(self.id.unwrap_or(MemberPhysicalId(Uuid::new_v4())).into()),
            deactivated: ActiveValue::Set(self.deactivated),
            note: ActiveValue::Set(self.note),
            ..ActiveModelTrait::default()
        }
        .insert(conn)
        .await?;

        info!("insert new physical id");
        Ok(PhysicalData {
            id: new_pid.id.into(),
            member_id: new_pid.member_id.map(Into::into),
            deactivated: new_pid.deactivated,
            note: new_pid.note,
        })
    }
}

impl PhysicalData {
    pub async fn load(conn: &DatabaseConnection, id: MemberPhysicalId) -> Result<Self, Error> {
        let pid: physical_ids::Model = PhysicalIds::find_by_id(id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Physical id not found"))?;
        Ok(Self {
            id: pid.id.into(),
            member_id: pid.member_id.map(Into::into),
            deactivated: pid.deactivated,
            note: pid.note,
        })
    }

    pub async fn update(self, conn: &DatabaseConnection) -> Result<Self, Error> {
        physical_ids::ActiveModel {
            id: ActiveValue::Set(self.id.into()),
            deactivated: ActiveValue::Set(self.deactivated),
            note: ActiveValue::Set(self.note.clone()),
            ..ActiveModelTrait::default()
        }
        .update(conn)
        .await?;
        info!("update physical id");
        Ok(self)
    }

    pub async fn delete(conn: &DatabaseConnection, id: MemberPhysicalId) -> Result<(), Error> {
        physical_ids::Entity::delete_by_id(id).exec(conn).await?;
        info!("delete physical id");
        Ok(())
    }

    pub async fn load_all(conn: &DatabaseConnection) -> Result<Vec<Self>, Error> {
        Ok(PhysicalIds::find().into_model::<Self>().all(conn).await?)
    }
}
