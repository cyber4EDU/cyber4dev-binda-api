use super::{Group, GroupId, HappeningId, MemberId, MemberIdVariant, NewGroup};
use crate::error::Error;
use crate::model::{prelude::*, *};
use sea_orm::{
    ActiveModelTrait, ActiveValue, ColumnTrait, DatabaseConnection, EntityTrait, ModelTrait,
    QueryFilter, TransactionTrait,
};
use sea_orm_active_enums::GroupRole;
use std::collections::HashSet;
use tracing::info;

impl NewGroup {
    pub async fn insert(
        self,
        conn: &DatabaseConnection,
        _creator: MemberIdVariant<'_>,
    ) -> Result<Group, Error> {
        let txn = conn.begin().await?;

        let new_group = groups::ActiveModel {
            name: ActiveValue::Set(self.name.clone().into()),
            ..ActiveModelTrait::default()
        };
        let new_group = new_group.insert(&txn).await?;
        let id = new_group.id;

        // collect responsibles
        if let Some(resp) = &self.responsibles {
            for uuid in resp {
                member_is_to_group::ActiveModel {
                    source_member_id: ActiveValue::Set(uuid.into()),
                    target_group_id: ActiveValue::Set(id),
                    role: ActiveValue::Set(GroupRole::Responsible),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        // collect participating members
        if let Some(part) = &self.participants {
            for uuid in part {
                member_is_to_group::ActiveModel {
                    source_member_id: ActiveValue::Set(uuid.into()),
                    target_group_id: ActiveValue::Set(id),
                    role: ActiveValue::Set(GroupRole::Participant),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        // collect happeing connections
        if let Some(grps) = &self.happenings {
            for uuid in grps {
                groups_happenings::ActiveModel {
                    group_id: ActiveValue::Set(id),
                    happening_id: ActiveValue::Set(uuid.into()),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        txn.commit().await?;

        info!("insert new group");
        Ok(Group {
            id: id.into(),
            name: self.name.clone(),
            responsibles: self.responsibles,
            participants: self.participants,
            happenings: self.happenings,
        })
    }
}

impl Group {
    pub async fn load(conn: &DatabaseConnection, id: GroupId) -> Result<Self, Error> {
        let group: groups::Model = Groups::find_by_id(id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Group not found"))?;

        let responsibles = Some(
            MemberIsToGroup::find()
                .filter(member_is_to_group::Column::TargetGroupId.eq(id.0))
                .filter(member_is_to_group::Column::Role.eq(GroupRole::Responsible))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.source_member_id.into())
                .collect::<Vec<MemberId>>(),
        )
        .filter(|vec| !vec.is_empty());

        let participants = Some(
            MemberIsToGroup::find()
                .filter(member_is_to_group::Column::TargetGroupId.eq(id.0))
                .filter(member_is_to_group::Column::Role.eq(GroupRole::Participant))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.source_member_id.into())
                .collect::<Vec<MemberId>>(),
        )
        .filter(|vec| !vec.is_empty());
        let happenings = Some(
            GroupsHappenings::find()
                .filter(groups_happenings::Column::GroupId.eq(id.0))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.happening_id.into())
                .collect::<Vec<HappeningId>>(),
        )
        .filter(|vec| !vec.is_empty());

        Ok(Self {
            id: group.id.into(),
            name: group.name.into(),
            responsibles,
            participants,
            happenings,
        })
    }

    pub async fn update(self, conn: &DatabaseConnection) -> Result<Self, Error> {
        // load entry
        let id = Groups::find_by_id(self.id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Group not found"))?
            .id;

        let txn = conn.begin().await?;

        let group = groups::ActiveModel {
            id: ActiveValue::Set(id),
            name: ActiveValue::Set(self.name.clone().into()),
            ..ActiveModelTrait::default()
        };
        group.update(&txn).await?;

        // update responsibles and participants (member-is-to-group)
        for (role, source) in &[
            (GroupRole::Responsible, &self.responsibles),
            (GroupRole::Participant, &self.participants),
        ] {
            let existing_members: HashSet<MemberId> = HashSet::from_iter(
                MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::TargetGroupId.eq(id))
                    .filter(member_is_to_group::Column::Role.eq(role.clone()))
                    .all(&txn)
                    .await?
                    .into_iter()
                    .map(|m| m.source_member_id.into()),
            );
            let update_members: HashSet<MemberId> = match source {
                Some(rspfr) => HashSet::from_iter(rspfr.iter().copied()),
                None => HashSet::new(),
            };
            for new_id in update_members.difference(&existing_members) {
                // create new entry
                member_is_to_group::ActiveModel {
                    source_member_id: ActiveValue::Set(new_id.into()),
                    target_group_id: ActiveValue::Set(id),
                    role: ActiveValue::Set(role.clone()),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
            for remove_id in existing_members.difference(&update_members) {
                // find endtry and remove it
                let rm_entry = MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::SourceMemberId.eq(remove_id.0))
                    .filter(member_is_to_group::Column::TargetGroupId.eq(id))
                    .filter(member_is_to_group::Column::Role.eq(role.clone()))
                    .one(&txn)
                    .await?;
                if let Some(entry) = rm_entry {
                    entry.delete(&txn).await?;
                }
            }
        }
        // update happenings (groups-happenings)
        let existing_happenings: HashSet<HappeningId> = HashSet::from_iter(
            GroupsHappenings::find()
                .filter(groups_happenings::Column::GroupId.eq(id))
                .all(&txn)
                .await?
                .into_iter()
                .map(|m| m.happening_id.into()),
        );
        let update_happenings: HashSet<HappeningId> = match &self.happenings {
            Some(rspfr) => HashSet::from_iter(rspfr.iter().copied()),
            None => HashSet::new(),
        };
        for new_id in update_happenings.difference(&existing_happenings) {
            // create new entry
            groups_happenings::ActiveModel {
                group_id: ActiveValue::Set(id),
                happening_id: ActiveValue::Set(new_id.into()),
                ..ActiveModelTrait::default()
            }
            .insert(&txn)
            .await?;
        }
        for remove_id in existing_happenings.difference(&update_happenings) {
            // find endtry and remove it
            let rm_entry = GroupsHappenings::find()
                .filter(groups_happenings::Column::GroupId.eq(id))
                .filter(groups_happenings::Column::HappeningId.eq(remove_id.0))
                .one(&txn)
                .await?;
            if let Some(entry) = rm_entry {
                entry.delete(&txn).await?;
            }
        }

        txn.commit().await?;

        Ok(self)
    }

    pub async fn delete(conn: &DatabaseConnection, id: GroupId) -> Result<(), Error> {
        let txn = conn.begin().await?;

        member_is_to_group::Entity::delete_many()
            .filter(member_is_to_group::Column::TargetGroupId.eq(id.0))
            .exec(&txn)
            .await?;
        groups_happenings::Entity::delete_many()
            .filter(groups_happenings::Column::GroupId.eq(id.0))
            .exec(&txn)
            .await?;
        groups::Entity::delete_by_id(id).exec(&txn).await?;

        txn.commit().await?;

        Ok(())
    }

    pub async fn load_all(conn: &DatabaseConnection) -> Result<Vec<Self>, Error> {
        let mut groups = vec![];
        for gp in Groups::find()
            .into_model::<groups::Model>()
            .all(conn)
            .await?
        {
            let responsibles = Some(
                MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::TargetGroupId.eq(gp.id))
                    .filter(member_is_to_group::Column::Role.eq(GroupRole::Responsible))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.source_member_id.into())
                    .collect::<Vec<MemberId>>(),
            )
            .filter(|vec| !vec.is_empty());

            let participants = Some(
                MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::TargetGroupId.eq(gp.id))
                    .filter(member_is_to_group::Column::Role.eq(GroupRole::Participant))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.source_member_id.into())
                    .collect::<Vec<MemberId>>(),
            )
            .filter(|vec| !vec.is_empty());
            let happenings = Some(
                GroupsHappenings::find()
                    .filter(groups_happenings::Column::GroupId.eq(gp.id))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.happening_id.into())
                    .collect::<Vec<HappeningId>>(),
            )
            .filter(|vec| !vec.is_empty());

            groups.push(Self {
                id: gp.id.into(),
                name: gp.name.into(),
                responsibles,
                participants,
                happenings,
            })
        }
        Ok(groups)
    }
}
