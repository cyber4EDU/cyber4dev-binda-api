use super::{
    GroupId, HappeningId, Member, MemberId, MemberIdVariant, MemberIdentityId, MemberPhysicalId,
    NewMember, ParticipantIn, PhysicalInfo, ResponsibleFor,
};
use crate::error::Error;
use crate::model::{prelude::*, *};
use sea_orm::{
    query::Condition, sea_query::IntoCondition, ActiveModelTrait, ActiveValue, ColumnTrait,
    DatabaseConnection, EntityTrait, JoinType, ModelTrait, QueryFilter, QuerySelect, RelationTrait,
    TransactionTrait,
};
use sea_orm_active_enums::{GroupRole, HappeningRole, MemberRole};
use std::collections::HashSet;
use tracing::info;

impl NewMember {
    pub async fn insert(
        self,
        conn: &DatabaseConnection,
        _creator: MemberIdVariant<'_>,
    ) -> Result<Member, Error> {
        let txn = conn.begin().await?;

        let new_member = members::ActiveModel {
            first_name: ActiveValue::Set(self.first_name.clone().into()),
            middle_names: ActiveValue::Set(self.middle_names.clone().map(Into::into)),
            last_name: ActiveValue::Set(self.last_name.clone().into()),
            person_qualifier: ActiveValue::Set(self.person_qualifier.clone().map(Into::into)),
            identity_id: ActiveValue::Set(self.identity_id.map(|id| id.into())),
            contact: ActiveValue::Set(self.contact.clone().map(|ct| ct.0)),
            ..ActiveModelTrait::default()
        }
        .insert(&txn)
        .await?;

        // collect responsibles
        if let Some(resp) = &self.responsibles {
            for uuid in resp {
                member_is_to_member::ActiveModel {
                    source_member_id: ActiveValue::Set(uuid.0),
                    target_member_id: ActiveValue::Set(new_member.id),
                    role: ActiveValue::Set(MemberRole::Responsible),
                    ..ActiveModelTrait::default()
                }
                .insert(&txn)
                .await?;
            }
        }
        // collect responsibilities
        if let Some(resp) = &self.responsible_for {
            // collect member responsibilities
            if let Some(mresp) = &resp.members {
                for uuid in mresp {
                    member_is_to_member::ActiveModel {
                        source_member_id: ActiveValue::Set(new_member.id),
                        target_member_id: ActiveValue::Set(uuid.0),
                        role: ActiveValue::Set(MemberRole::Responsible),
                        ..ActiveModelTrait::default()
                    }
                    .insert(&txn)
                    .await?;
                }
            }
            // collect group responsibilities
            if let Some(gresp) = &resp.groups {
                for uuid in gresp {
                    let new_member_is_to_group_relation = member_is_to_group::ActiveModel {
                        source_member_id: ActiveValue::Set(new_member.id),
                        target_group_id: ActiveValue::Set(uuid.0),
                        role: ActiveValue::Set(GroupRole::Responsible),
                        ..ActiveModelTrait::default()
                    };
                    MemberIsToGroup::insert(new_member_is_to_group_relation)
                        .exec(&txn)
                        .await?;
                }
            }
            // collect happening responsibilities
            if let Some(gresp) = &resp.happenings {
                for uuid in gresp {
                    let new_member_is_to_happening_relation = member_is_to_happening::ActiveModel {
                        source_member_id: ActiveValue::Set(new_member.id),
                        target_happening_id: ActiveValue::Set(uuid.0),
                        role: ActiveValue::Set(HappeningRole::Responsible),
                        ..ActiveModelTrait::default()
                    };
                    MemberIsToHappening::insert(new_member_is_to_happening_relation)
                        .exec(&txn)
                        .await?;
                }
            }
        }
        // collect participation
        if let Some(part) = &self.participant_in {
            // collect group participation
            if let Some(gpart) = &part.groups {
                for uuid in gpart {
                    let new_member_is_to_group_relation = member_is_to_group::ActiveModel {
                        source_member_id: ActiveValue::Set(new_member.id),
                        target_group_id: ActiveValue::Set(uuid.0),
                        role: ActiveValue::Set(GroupRole::Participant),
                        ..ActiveModelTrait::default()
                    };
                    MemberIsToGroup::insert(new_member_is_to_group_relation)
                        .exec(&txn)
                        .await?;
                }
            }
            // collect happening participation
            if let Some(hpart) = &part.happenings {
                for uuid in hpart {
                    let new_member_is_to_happening_relation = member_is_to_happening::ActiveModel {
                        source_member_id: ActiveValue::Set(new_member.id),
                        target_happening_id: ActiveValue::Set(uuid.0),
                        role: ActiveValue::Set(HappeningRole::Responsible),
                        ..ActiveModelTrait::default()
                    };
                    MemberIsToHappening::insert(new_member_is_to_happening_relation)
                        .exec(&txn)
                        .await?;
                }
            }
        }
        // add physical ids
        if let Some(physical_ids) = &self.physical_ids {
            for pid in physical_ids {
                let new_physical_id = physical_ids::ActiveModel {
                    member_id: ActiveValue::Set(Some(new_member.id)),
                    id: ActiveValue::Set(pid.id.0),
                    deactivated: ActiveValue::Set(pid.deactivated),
                    note: ActiveValue::Set(pid.note.clone()),
                    ..ActiveModelTrait::default()
                };
                PhysicalIds::insert(new_physical_id).exec(&txn).await?;
            }
        }

        txn.commit().await?;

        info!("insert new member");
        Ok(Member {
            id: new_member.id.into(),
            first_name: self.first_name.clone(),
            middle_names: self.middle_names.clone(),
            last_name: self.last_name.clone(),
            person_qualifier: self.person_qualifier.clone(),
            identity_id: self.identity_id,
            responsibles: self.responsibles,
            responsible_for: self.responsible_for,
            participant_in: self.participant_in,
            physical_ids: self.physical_ids,
            contact: self.contact,
        })
    }
}

impl Member {
    pub async fn load(conn: &DatabaseConnection, id: MemberId) -> Result<Self, Error> {
        let member: members::Model = Members::find_by_id(id.0)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Member not found"))?;
        Self::load_rest(conn, member).await
    }

    pub async fn load_by_iid(
        conn: &DatabaseConnection,
        iid: MemberIdentityId,
    ) -> Result<Self, Error> {
        let member: members::Model = Members::find()
            .filter(members::Column::IdentityId.eq(iid.0))
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Identity id not found"))?;
        Self::load_rest(conn, member).await
    }

    pub async fn load_by_pid(
        conn: &DatabaseConnection,
        pid: MemberPhysicalId,
    ) -> Result<Self, Error> {
        let member: members::Model = Members::find()
            .join(
                JoinType::InnerJoin,
                members::Relation::PhysicalIds
                    .def()
                    .on_condition(move |_, _| physical_ids::Column::Id.eq(pid.0).into_condition()),
            )
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Member not found by physical id"))?;
        Self::load_rest(conn, member).await
    }

    async fn load_rest(conn: &DatabaseConnection, member: members::Model) -> Result<Self, Error> {
        let responsibles = Some(
            MemberIsToMember::find()
                .filter(member_is_to_member::Column::TargetMemberId.eq(member.id))
                .filter(member_is_to_member::Column::Role.eq(MemberRole::Responsible))
                .all(conn)
                .await?
                .into_iter()
                .map(|m| m.source_member_id.into())
                .collect::<Vec<MemberId>>(),
        )
        .filter(|vec| !vec.is_empty());

        let responsible_for = Some(ResponsibleFor {
            members: Some(
                MemberIsToMember::find()
                    .filter(member_is_to_member::Column::SourceMemberId.eq(member.id))
                    .filter(member_is_to_member::Column::Role.eq(MemberRole::Responsible))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.target_member_id.into())
                    .collect::<Vec<MemberId>>(),
            )
            .filter(|vec| !vec.is_empty()),
            groups: Some(
                MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::SourceMemberId.eq(member.id))
                    .filter(member_is_to_group::Column::Role.eq(GroupRole::Responsible))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.target_group_id.into())
                    .collect::<Vec<GroupId>>(),
            )
            .filter(|vec| !vec.is_empty()),
            happenings: Some(
                MemberIsToHappening::find()
                    .filter(member_is_to_happening::Column::SourceMemberId.eq(member.id))
                    .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Responsible))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.target_happening_id.into())
                    .collect::<Vec<HappeningId>>(),
            )
            .filter(|vec| !vec.is_empty()),
        })
        .filter(|rspf| {
            rspf.members.is_some() || rspf.groups.is_some() || rspf.happenings.is_some()
        });

        let participant_in = Some(ParticipantIn {
            groups: Some(
                MemberIsToGroup::find()
                    .filter(member_is_to_group::Column::SourceMemberId.eq(member.id))
                    .filter(member_is_to_group::Column::Role.eq(GroupRole::Participant))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.target_group_id.into())
                    .collect::<Vec<GroupId>>(),
            )
            .filter(|vec| !vec.is_empty()),
            happenings: Some(
                MemberIsToHappening::find()
                    .filter(member_is_to_happening::Column::SourceMemberId.eq(member.id))
                    .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Participant))
                    .all(conn)
                    .await?
                    .into_iter()
                    .map(|m| m.target_happening_id.into())
                    .collect::<Vec<HappeningId>>(),
            )
            .filter(|vec| !vec.is_empty()),
        })
        .filter(|rspf| rspf.groups.is_some() || rspf.happenings.is_some());

        let physical_ids = Some(
            PhysicalIds::find()
                .filter(physical_ids::Column::MemberId.eq(member.id))
                .all(conn)
                .await?
                .into_iter()
                .map(|pid| PhysicalInfo {
                    id: pid.id.into(),
                    deactivated: pid.deactivated,
                    note: pid.note,
                })
                .collect::<Vec<_>>(),
        )
        .filter(|vec| !vec.is_empty());

        Ok(Member {
            id: member.id.into(),
            first_name: member.first_name.into(),
            middle_names: member.middle_names.map(Into::into),
            last_name: member.last_name.into(),
            person_qualifier: member.person_qualifier.map(Into::into),
            identity_id: member.identity_id.map(|id| id.into()),
            responsibles,
            responsible_for,
            participant_in,
            physical_ids,
            contact: member.contact.map(Into::into),
        })
    }

    pub async fn update(self, conn: &DatabaseConnection) -> Result<Self, Error> {
        // load entry
        let id = Members::find_by_id(self.id.0)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Member not found"))?
            .id;

        let txn = conn.begin().await?;

        // set new values
        members::ActiveModel {
            id: ActiveValue::Set(id),
            first_name: ActiveValue::Set(self.first_name.clone().into()),
            middle_names: ActiveValue::Set(self.middle_names.clone().map(Into::into)),
            last_name: ActiveValue::Set(self.last_name.clone().into()),
            person_qualifier: ActiveValue::Set(self.person_qualifier.clone().map(Into::into)),
            identity_id: ActiveValue::Set(self.identity_id.map(|id| id.into())),
            contact: ActiveValue::Set(self.contact.clone().map(Into::into)),
            ..ActiveModelTrait::default()
        }
        .update(&txn)
        .await?;

        // update relation tables
        // FIXME: there should be an easier way to do it

        // update responsibles (member-is-to-member)
        let existing_resp_members: HashSet<MemberId> = HashSet::from_iter(
            MemberIsToMember::find()
                .filter(member_is_to_member::Column::TargetMemberId.eq(id))
                .filter(member_is_to_member::Column::Role.eq(MemberRole::Responsible))
                .all(&txn)
                .await?
                .into_iter()
                .map(|m| m.target_member_id.into()),
        );
        info!(
            "existing responsible members: {}",
            existing_resp_members.len()
        );
        let update_resp_members: HashSet<MemberId> = match &self.responsibles {
            Some(rspfr) => HashSet::from_iter(rspfr.iter().copied()),
            None => HashSet::new(),
        };
        info!("update responsible members: {}", update_resp_members.len());

        let mut count = 0;
        for new_id in update_resp_members.difference(&existing_resp_members) {
            // create new entry
            member_is_to_member::ActiveModel {
                source_member_id: ActiveValue::Set(new_id.into()),
                target_member_id: ActiveValue::Set(id),
                role: ActiveValue::Set(MemberRole::Responsible),
                ..ActiveModelTrait::default()
            }
            .insert(&txn)
            .await?;
            count += 1;
        }
        info!("add members: {}", count);

        count = 0;
        for remove_id in existing_resp_members.difference(&update_resp_members) {
            // find entry and remove it
            let rm_entry = MemberIsToMember::find()
                .filter(member_is_to_member::Column::SourceMemberId.eq(remove_id.0))
                .filter(member_is_to_member::Column::TargetMemberId.eq(id))
                .filter(member_is_to_member::Column::Role.eq(MemberRole::Responsible))
                .one(&txn)
                .await?;
            if let Some(entry) = rm_entry {
                entry.delete(&txn).await?;
                count -= 1;
            }
        }
        info!("remove members: {}", count);

        // update responsible-for (member-is-to-member)
        let existing_resp_for_members: HashSet<MemberId> = HashSet::from_iter(
            MemberIsToMember::find()
                .filter(member_is_to_member::Column::SourceMemberId.eq(id))
                .filter(member_is_to_member::Column::Role.eq(MemberRole::Responsible))
                .all(&txn)
                .await?
                .into_iter()
                .map(|m| m.target_member_id.into()),
        );
        let update_resp_for_members: HashSet<MemberId> = match &self.responsible_for {
            Some(rspfr) => HashSet::from_iter(rspfr.members.iter().flatten().copied()),
            None => HashSet::new(),
        };
        for new_id in update_resp_for_members.difference(&existing_resp_for_members) {
            // create new entry
            member_is_to_member::ActiveModel {
                source_member_id: ActiveValue::Set(self.id.into()),
                target_member_id: ActiveValue::Set(new_id.into()),
                role: ActiveValue::Set(MemberRole::Responsible),
                ..ActiveModelTrait::default()
            }
            .insert(&txn)
            .await?;
        }
        for remove_id in existing_resp_for_members.difference(&update_resp_for_members) {
            // find endtry and remove it
            let rm_entry = MemberIsToMember::find()
                .filter(member_is_to_member::Column::SourceMemberId.eq(id))
                .filter(member_is_to_member::Column::TargetMemberId.eq(remove_id.0))
                .filter(member_is_to_member::Column::Role.eq(MemberRole::Responsible))
                .one(&txn)
                .await?;
            if let Some(entry) = rm_entry {
                entry.delete(&txn).await?;
            }
        }

        // update responsible-for (member-is-to-group)
        let existing_resp_for_groups: HashSet<GroupId> = HashSet::from_iter(
            MemberIsToGroup::find()
                .filter(member_is_to_group::Column::SourceMemberId.eq(id))
                .filter(member_is_to_group::Column::Role.eq(MemberRole::Responsible))
                .all(&txn)
                .await?
                .into_iter()
                .map(|m| m.target_group_id.into()),
        );
        let update_resp_for_groups: HashSet<GroupId> = match &self.responsible_for {
            Some(rspfr) => HashSet::from_iter(rspfr.groups.iter().flatten().copied()),
            None => HashSet::new(),
        };
        for new_id in update_resp_for_groups.difference(&existing_resp_for_groups) {
            // create new entry
            member_is_to_group::ActiveModel {
                source_member_id: ActiveValue::Set(self.id.into()),
                target_group_id: ActiveValue::Set(new_id.into()),
                role: ActiveValue::Set(GroupRole::Responsible),
                ..ActiveModelTrait::default()
            }
            .insert(&txn)
            .await?;
        }
        for remove_id in existing_resp_for_groups.difference(&update_resp_for_groups) {
            // find endtry and remove it
            let rm_entry = MemberIsToGroup::find()
                .filter(member_is_to_group::Column::SourceMemberId.eq(id))
                .filter(member_is_to_group::Column::TargetGroupId.eq(remove_id.0))
                .filter(member_is_to_group::Column::Role.eq(GroupRole::Responsible))
                .one(&txn)
                .await?;
            if let Some(entry) = rm_entry {
                entry.delete(&txn).await?;
            }
        }

        // update responsible-for (member-is-to-happening)
        let existing_resp_for_happenings: HashSet<HappeningId> = HashSet::from_iter(
            MemberIsToHappening::find()
                .filter(member_is_to_happening::Column::SourceMemberId.eq(id))
                .filter(member_is_to_happening::Column::Role.eq(MemberRole::Responsible))
                .all(&txn)
                .await?
                .into_iter()
                .map(|m| m.target_happening_id.into()),
        );
        let update_resp_for_happenings: HashSet<HappeningId> = match &self.responsible_for {
            Some(rspfr) => HashSet::from_iter(rspfr.happenings.iter().flatten().copied()),
            None => HashSet::new(),
        };
        for new_id in update_resp_for_happenings.difference(&existing_resp_for_happenings) {
            // create new entry
            member_is_to_happening::ActiveModel {
                source_member_id: ActiveValue::Set(self.id.into()),
                target_happening_id: ActiveValue::Set(new_id.into()),
                role: ActiveValue::Set(HappeningRole::Responsible),
                ..ActiveModelTrait::default()
            }
            .insert(&txn)
            .await?;
        }
        for remove_id in existing_resp_for_happenings.difference(&update_resp_for_happenings) {
            // find endtry and remove it
            let rm_entry = MemberIsToHappening::find()
                .filter(member_is_to_happening::Column::SourceMemberId.eq(id))
                .filter(member_is_to_happening::Column::TargetHappeningId.eq(remove_id.0))
                .filter(member_is_to_happening::Column::Role.eq(HappeningRole::Responsible))
                .one(&txn)
                .await?;
            if let Some(entry) = rm_entry {
                entry.delete(&txn).await?;
            }
        }

        // update physical ids
        let existing_physical_ids: HashSet<MemberPhysicalId> = HashSet::from_iter(
            PhysicalIds::find()
                .filter(physical_ids::Column::MemberId.eq(id))
                .all(&txn)
                .await?
                .into_iter()
                .map(|pid| pid.id.into()),
        );

        let update_physical_ids: HashSet<MemberPhysicalId> = match &self.physical_ids {
            Some(pids) => HashSet::from_iter(pids.iter().map(|pid| pid.id)),
            None => HashSet::new(),
        };

        // remove physical ids
        for remove_pid in existing_physical_ids.difference(&update_physical_ids) {
            info!("remove pid: {}", remove_pid.0);
            if let Some(rm_pid) = PhysicalIds::find()
                .filter(physical_ids::Column::MemberId.eq(id))
                .filter(physical_ids::Column::Id.eq(remove_pid.0))
                .one(&txn)
                .await?
            {
                rm_pid.delete(&txn).await?;
            }
        }

        // add or update new physical ids
        if let Some(physical_ids) = &self.physical_ids {
            for pid in physical_ids {
                if existing_physical_ids.contains(&pid.id) {
                    if let Some(apid) = PhysicalIds::find()
                        .filter(physical_ids::Column::Id.eq(pid.id.0))
                        .one(&txn)
                        .await?
                    {
                        info!("update pid: {}", pid.id.0);
                        let mut apid: physical_ids::ActiveModel = apid.into();
                        apid.note = ActiveValue::Set(pid.note.clone());
                        apid.deactivated = ActiveValue::Set(pid.deactivated);
                        apid.update(&txn).await?;
                    }
                } else {
                    let new_physical_id = physical_ids::ActiveModel {
                        member_id: ActiveValue::Set(Some(self.id.0)),
                        id: ActiveValue::Set(pid.id.0),
                        deactivated: ActiveValue::Set(pid.deactivated),
                        note: ActiveValue::Set(pid.note.clone()),
                        ..ActiveModelTrait::default()
                    };
                    info!("insert pid: {}", pid.id.0);
                    PhysicalIds::insert(new_physical_id).exec(&txn).await?;
                }
            }
        }

        txn.commit().await?;
        info!("updated member");

        Ok(self)
    }

    pub async fn delete(conn: &DatabaseConnection, id: MemberId) -> Result<(), Error> {
        let txn = conn.begin().await?;

        member_is_to_member::Entity::delete_many()
            .filter(
                Condition::any()
                    .add(member_is_to_member::Column::SourceMemberId.eq(id.0))
                    .add(member_is_to_member::Column::TargetMemberId.eq(id.0)),
            )
            .exec(&txn)
            .await?;
        member_is_to_group::Entity::delete_many()
            .filter(member_is_to_group::Column::SourceMemberId.eq(id.0))
            .exec(&txn)
            .await?;
        member_is_to_happening::Entity::delete_many()
            .filter(member_is_to_happening::Column::SourceMemberId.eq(id.0))
            .exec(&txn)
            .await?;
        members::Entity::delete_by_id(id).exec(&txn).await?;
        physical_ids::Entity::delete_many()
            .filter(physical_ids::Column::MemberId.eq(id.0))
            .exec(&txn)
            .await?;

        txn.commit().await?;

        Ok(())
    }
}
