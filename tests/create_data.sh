#!/usr/bin/env bash

get_token() {
    CLIENT_SECRET=$1
    USERNAME=$2
    PASSWORD=$3

    curl --location --request POST "http://localhost:8282/realms/esbz/protocol/openid-connect/token" \
        --header "Content-Type: application/x-www-form-urlencoded" \
        --data-urlencode "client_id=binda-ui_access" \
        --data-urlencode "grant_type=password" \
        --data-urlencode "username=${USERNAME}" \
        --data-urlencode "password=${PASSWORD}" \
        --data-urlencode "scope=openid" \
        --data-urlencode "client_secret=${CLIENT_SECRET}"
}


create_member() {
    TOKEN=$1
    DATA=$2

    curl -s -X 'POST' \
        'http://localhost:8000/api/v1/admin/member' \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${TOKEN}" \
        -d "${DATA}"
}

update_member() {
    TOKEN=$1
    DATA=$2

    curl -s -X 'PUT' \
        'http://localhost:8000/api/v1/admin/member' \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -H "Authorization: Bearer ${TOKEN}" \
        -d "${DATA}"
}

create_group() {
    TOKEN=$1
    DATA=$2

    if false; then
        echo "$data"
    else
        curl -s -X 'POST' \
            'http://localhost:8000/api/v1/admin/group' \
            -H 'accept: application/json' \
            -H 'Content-Type: application/json' \
            -H "Authorization: Bearer ${TOKEN}" \
            -d "${DATA}"
    fi
}

create_happening() {
    TOKEN=$1
    NAME=$2
    RESPONSIBLES=$3
    PARTICIPANTS=$4
    GRPS=$5

    data="{\"name\":${NAME},\"participants\":${PARTICIPANTS},\"responsibles\":${RESPONSIBLES},\"groups\":${GRPS}}"
    if false; then
        echo "$data"
    else
        curl -s -X 'POST' \
            'http://localhost:8000/api/v1/admin/happening' \
            -H 'accept: application/json' \
            -H 'Content-Type: application/json' \
            -H "Authorization: Bearer ${TOKEN}" \
            -d "${data}"
    fi
}

send_api_requests() {
    TOKEN=$1
    #
    # create students
    #
    echo "===================== create students ==================="
    STUDENTS=$(cat ${THISDIR}/students.json)
    CREATED_STUDENTS=""
    while read -r student;
    do
        response=$(create_member "$TOKEN" "${student}")
        echo "create member: ${student} -> response: $(echo "${response}")"
        CREATED_STUDENTS="${CREATED_STUDENTS}${response},"
    done <<<$(echo "${STUDENTS}" | jq -c '.[]')
    CREATED_STUDENTS="[${CREATED_STUDENTS::-1}]"
    echo "${CREATED_STUDENTS}" > ${THISDIR}/created_students.json

    #
    # create teachers
    #
    # every teacher is responsible for all students
    echo "===================== create teachers ==================="
    TEACHERS=$(cat ${THISDIR}/teachers.json)
    CREATED_TEACHERS=""
    while read -r teacher;
    do
        data="$(echo "${teacher}" | \
            jq -c ". += {\"responsible_for\":{\"members\":$(echo "$CREATED_STUDENTS" | jq -c "map(.id)")}}")"

        response=$(create_member "$TOKEN" "${data}" \
            )
        echo "create member: ${teacher} -> response: $(echo "${response}")"
        CREATED_TEACHERS="${CREATED_TEACHERS}${response},"
    done <<<$(echo "${TEACHERS}" | jq -c '.[]')
    CREATED_TEACHERS="[${CREATED_TEACHERS::-1}]"
    echo "${CREATED_TEACHERS}" > ${THISDIR}/created_teachers.json

    echo "===================== create groups ==================="
    NEW_GROUPS="$(cat ${THISDIR}/groups.json)"
    CREATED_GROUPS=""
    while read -r group;
    do
        data="$(echo "${group}" | \
            jq -c ". += {\"participants\":$(echo "$CREATED_STUDENTS" | jq -c "map(.id)"),\"responsibles\":$(echo "$CREATED_TEACHERS" | jq -c "map(.id)")}")"

        response=$(create_group "$TOKEN" "${data}")
        echo "create group: ${group} -> response: $(echo "${response}")"
        CREATED_GROUPS="${CREATED_GROUPS}${response},"
    done <<<$(echo "${NEW_GROUPS}" | jq -c '.[]')
    CREATED_GROUPS="[${CREATED_GROUPS::-1}]"
    echo "${CREATED_GROUPS}" > ${THISDIR}/created_groups.json

    echo "===================== create happenings ==================="
    NEW_EVENTS="$(cat ${THISDIR}/happenings.json)"
    CREATED_EVENTS=""
    while read -r happening;
    do
        response=$(create_happening "$TOKEN" "${happening}" \
            "$(echo "$CREATED_TEACHERS" | jq -c "map(.id)")" \
            "$(echo "$CREATED_STUDENTS" | jq -c "map(.id)")" \
            "$(echo "$CREATED_GROUPS" | jq -c "map(.id)")")
        echo "create happening: ${happening} -> response: $(echo "${response}")"
        CREATED_EVENTS="${CREATED_EVENTS}${response},"
    done <<<$(echo "${NEW_EVENTS}" | jq -c '.[].name')
    CREATED_EVENTS="[${CREATED_EVENTS::-1}]"
    echo "${CREATED_EVENTS}" > ${THISDIR}/created_happenings.json

    echo "============ add keycloak_id to first teacher ============="
    # NOTE: this is the keycloak id from the test setup
    testuser001_keycloak_id="d26a3222-3c06-4d75-9f01-9b1c2a3d9e4f"
    data="$(cat ${THISDIR}/created_teachers.json | jq -c ".[0] += {\"keycloak_id\":\"${testuser001_keycloak_id}\"} | .[0]")"
    echo "$data"
    response=$(update_member "${TOKEN}" "${data}")
    echo "update member: ${data} -> response: $(echo "${response}")"

}
