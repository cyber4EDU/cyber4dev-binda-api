use super::{GroupId, GroupShortInfo};
use crate::error::Error;
use crate::model::prelude::*;
use chrono::{DateTime, Utc};
use sea_orm::{DatabaseConnection, DbBackend, EntityTrait, FromQueryResult, Statement};

impl GroupShortInfo {
    pub async fn load(conn: &DatabaseConnection, id: &GroupId) -> Result<Self, Error> {
        Groups::find_by_id(id.0)
            .one(conn)
            .await?
            .map(|m| Self {
                id: m.id.into(),
                name: m.name.into(),
            })
            .ok_or(Error::NotFound("Member not found"))
    }

    pub async fn load_all_active(
        conn: &DatabaseConnection,
        date: DateTime<Utc>,
    ) -> Result<Vec<Self>, Error> {
        Ok(Self::find_by_statement(Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                SELECT DISTINCT
                    groups.id,
                    groups.name
                FROM
                    groups
                INNER JOIN groups_happenings as gh
                    ON
                        gh.group_id = groups.id
                INNER JOIN happenings AS h
                    ON
                        h.id = gh.happening_id
                        AND TO_TIMESTAMP(
                            h.props->>'from',
                            'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                        )::timestamp at time zone 'utc' <= $1
                        AND TO_TIMESTAMP(
                            h.props->>'to',
                            'YYYY-MM-DD"T"HH24:MI:SS"Z"'
                        )::timestamp at time zone 'utc' >= $1
            "#,
            [date.into()],
        ))
        .all(conn)
        .await?)
    }
}
