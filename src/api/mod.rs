pub mod data_io;

pub mod absents;
pub mod actions;
pub mod admin;
pub mod history;
pub mod info;
pub mod me;
pub mod reports;
mod responses;

use actix_web::{get, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};

#[derive(Debug, Default, Serialize, Deserialize, ToSchema)]
pub enum SortOrder {
    #[default]
    Ascending,
    Descending,
}

#[derive(Default, Serialize, Deserialize, ToSchema)]
//#[into_params(parameter_in = Query)]
pub struct Sorting<T: utoipa::ToSchema> {
    pub sort_by: Option<T>,
    pub order: Option<SortOrder>,
}

static VERSION: &str = env!("CARGO_PKG_VERSION");

#[utoipa::path(
    tag = "misc",
    responses(
        (status = 200, description = "Info returned", body = String),
    )
)]
#[get("/version")]
async fn version() -> impl Responder {
    HttpResponse::Ok().body(VERSION)
}

#[utoipa::path(
    tag = "misc",
    responses(
        (status = 200, description = "Status ok"),
    )
)]
#[get("/status")]
async fn status() -> impl Responder {
    HttpResponse::Ok()
}

impl<T: ToSchema> IntoParams for Sorting<T> {
    fn into_params(
        parameter_in_provider: impl Fn() -> Option<utoipa::openapi::path::ParameterIn>,
    ) -> Vec<utoipa::openapi::path::Parameter> {
        // TODO
        vec![
            utoipa::openapi::path::ParameterBuilder::new()
                .name("sort_by")
                .required(utoipa::openapi::Required::False)
                .parameter_in(parameter_in_provider().unwrap_or_default())
                .description(Some("Sort by"))
                .schema(Some(
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::schema::Type::String), //.format(Some(utoipa::openapi::SchemaFormat::KnownFormat(
                                                                             //    utoipa::openapi::KnownFormat::Int64,
                                                                             //))),
                ))
                .build(),
            utoipa::openapi::path::ParameterBuilder::new()
                .name("order")
                .required(utoipa::openapi::Required::False)
                .parameter_in(parameter_in_provider().unwrap_or_default())
                .description(Some("Order of sorting"))
                .schema(Some(
                    utoipa::openapi::ObjectBuilder::new()
                        .schema_type(utoipa::openapi::schema::Type::String),
                ))
                .build(),
        ]
    }
}
