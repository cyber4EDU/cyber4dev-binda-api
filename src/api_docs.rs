#![allow(deprecated)]
use crate::api;
use crate::data::*;
use utoipa::{
    openapi::security::{Http, HttpAuthScheme, SecurityScheme},
    openapi::server::Server,
    Modify, OpenApi,
};

#[derive(OpenApi)]
#[openapi(
    paths(
        api::status,
        api::version,
        //
        api::admin::post_admin_member,
        api::admin::get_admin_member,
        api::admin::get_admin_member_iid,
        api::admin::get_admin_member_pid,
        api::admin::get_admin_info_member,
        api::admin::get_admin_info_member_iid,
        api::admin::get_admin_info_member_pid,
        api::admin::get_admin_info_members,
        api::admin::get_admin_info_members_absent,
        api::admin::get_admin_info_members_missing,
        api::admin::get_admin_info_absents_member,
        api::admin::get_admin_info_group_participants_checked_in,
        api::admin::put_admin_member,
        api::admin::delete_admin_member,
        //
        api::admin::post_admin_group,
        api::admin::get_admin_group,
        api::admin::put_admin_group,
        api::admin::delete_admin_group,
        api::admin::get_admin_groups,
        //
        api::admin::post_admin_happening,
        api::admin::get_admin_happening,
        api::admin::put_admin_happening,
        api::admin::delete_admin_happening,
        api::admin::get_admin_happenings,
        //
        api::admin::post_admin_absent,
        api::admin::get_admin_absent,
        api::admin::put_admin_absent,
        api::admin::delete_admin_absent,
        //
        api::admin::post_admin_physicaldata,
        api::admin::get_admin_physicaldata,
        api::admin::put_admin_physicaldata,
        api::admin::delete_admin_physicaldata,
        api::admin::get_admin_physicaldatas,
        //
        api::admin::get_admin_dashboard,
        api::admin::post_admin_dashboard_numbers,
        //
        api::admin::post_admin_check_out,
        api::admin::post_admin_check_out_iid,
        api::admin::post_admin_check_out_pid,
        //
        api::me::get_me,
        api::me::get_me_happenings,
        api::me::get_me_dashboard,
        api::me::post_me_dashboard_numbers,
        //
        api::info::get_info_member,
        api::info::get_info_member_kc,
        api::info::get_info_member_iid,
        api::info::get_info_member_pid,
        api::info::get_info_members,
        api::info::get_info_members_absent,
        api::info::get_info_members_missing,
        //
        api::info::get_info_group_participants,
        api::info::get_info_group_participants_checked_in,
        api::info::get_info_group_participants_absent,
        api::info::get_info_group_participants_missing,
        //
        api::info::get_info_happening_participants,
        api::info::get_info_happening_participants_checked_in,
        //
        api::history::get_info_history_member,
        //
        api::info::get_info_dashboard_happening,
        api::info::post_info_dashboard_numbers,
        //
        api::actions::post_actions_happening_check_in,
        api::actions::post_actions_happening_check_out,
        api::actions::post_actions_happening_check_in_kc,
        api::actions::post_actions_happening_check_in_iid,
        api::actions::post_actions_happening_check_in_pid,
        api::actions::post_actions_happening_check_out_kc,
        api::actions::post_actions_happening_check_out_iid,
        api::actions::post_actions_happening_check_out_pid,
        api::actions::post_actions_happening_close,
        //
        api::absents::post_absent,
        api::absents::get_absent,
        api::absents::get_absents_member,
        api::absents::put_absent,
        api::absents::delete_absent,
    ),
    components(schemas(
        MemberFirstName,
        MemberMiddleNames,
        MemberLastName,
        MemberPersonQualifier,
        Contact,
        GroupName,
        HappeningName,
        MemberId,
        GroupId,
        HappeningId,
        MemberIdentityId,
        MemberPhysicalId,
        Absent,
        AbsentId,
        AbsentReason,
        AbsentInfo,
        Member,
        MemberInfo,
        MemberInfoSortKeys,
        MemberInfoCheckedInSortKeys,
        MemberInfoAbsentSortKeys,
        MemberAbsentInfo,
        MemberAbsentInfoSortKeys,
        MemberCheckedInInfo,
        GroupShortInfo,
        HappeningShortInfo,
        HappeningShortInfoSortKeys,
        HappeningPropsMinimum,
        HappeningPresenceInfo,
        History,
        Group,
        Happening,
        NewMember,
        NewGroup,
        NewHappening,
        NewAbsent,
        ParticipantIn,
        ResponsibleFor,
        PhysicalInfo,
        PhysicalData,
        NewPhysicalData,
        TimeFrame,
        TimeData,
        Delay,
        api::SortOrder,
        dashboard::Dashboard,
        dashboard::AdminDashboard,
        dashboard::DashboardOld,
        dashboard::HappeningDashboard,
        dashboard::NumberRequest,
        dashboard::NumberResponse,
        dashboard::AdminNumberRequest,
        dashboard::AdminNumberResponse,
    )),
    tags(
        (
            name = "admin",
            description = "Administration endpoints for members, groups, happenings, physical-ids and absents. `role_binda_admin`-role is necessary here."
        ),
        (
            name = "admin-info",
            description = "deprecated: Administration endpoints for status information of members. `role_binda_admin`-role is necessary here."
        ),
        (
            name = "admin-dashboard",
            description = "Endpoints to load admin dashboards and related information. `role_binda_admin`-role is necessary here."
        ),
        (
            name = "admin-actions",
            description = "Endpoints to change current state of members, currently only check-out. `role_binda_admin`-role is necessary here."
        ),
        (
            name = "info",
            description = "Endpoints to load information about current state of members. `role_binda_teacher`-role or `role_binda_admin`-role is necessary here."
        ),
        (
            name = "absent",
            description = "Endpoints to load, modify and store absent information for members. `role_binda_teacher`-role is necessary here."
        ),
        (
            name = "info-dashboard",
            description = "Endpoints to load dashboards and related information. `role_binda_teacher`-role is necessary here."
        ),
        (
            name = "me",
            description = "Endpoints to get information specific for the user, which is derived from their token. `role_binda_user`-role is necessary here."
        ),
        (
            name = "actions",
            description = "Endpoints to change current state of members. `role_binda_teacher`-role is necessary here."
        ),
    ),
    modifiers(&ServerAddon, &SecurityAddon)
)]
pub struct ApiDoc;

struct SecurityAddon;

impl Modify for SecurityAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        // we can unwrap safely since there already is components registered.
        let components = openapi.components.as_mut().unwrap();
        components.add_security_scheme(
            "api_access_token",
            SecurityScheme::Http(Http::new(HttpAuthScheme::Bearer)),
        )
    }
}

struct ServerAddon;

impl Modify for ServerAddon {
    fn modify(&self, openapi: &mut utoipa::openapi::OpenApi) {
        openapi.servers = Some(vec![Server::new("/api/v1")])
    }
}
