#![allow(dead_code)]
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::Once;
use std::thread;
use std::time;
use ureq::Response;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TeacherConf {
    pub first_name: String,
    pub middle_names: Option<String>,
    pub last_name: String,
    pub username: String,
    pub password: String,
    pub identity_id: Uuid,
    pub uuid: Option<Uuid>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GroupConf {
    pub name: String,
    pub uuid: Option<Uuid>,
    pub responsibles: Vec<String>,
    pub responsible_uuids: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HappeningConf {
    pub name: String,
    pub uuid: Option<Uuid>,
    pub from: String,
    pub to: String,
    pub auto_checkout: Option<i64>,
    pub groups: Vec<String>,
    pub group_uuids: Option<Vec<String>>,
    pub responsibles: Vec<String>,
    pub responsible_uuids: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct StudentConf {
    pub first_name: String,
    pub middle_names: Option<String>,
    pub last_name: String,
    pub person_qualifier: Option<String>,
    pub uuid: Option<Uuid>,
    pub group: String,
    pub group_uuid: Option<Uuid>,
    pub physical_ids: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum ActionType {
    CheckIn,
    CheckOut,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ActionConf {
    pub action: ActionType,
    pub date: String,
    pub happening: String,
    pub physical_id: Uuid,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct AbsentConf {
    pub physical_id: Uuid,
    pub from: String,
    pub until: String,
    pub reason: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HappeningPresenceInfo {
    pub id: Uuid,
    pub name: String,
    pub props: Option<serde_json::Value>,
    pub absent_entries: Vec<serde_json::Value>,
    pub checked_ins: Vec<String>,
    pub checked_outs: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct History {
    pub member_id: Uuid,
    pub happening_presences: Vec<HappeningPresenceInfo>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum TestType {
    NumberCheckedIn,
    NumberMissing,
    NumberAbsent,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TestConf {
    pub date: String,
    pub num_checked_in: Option<HashMap<String, usize>>,
    pub num_missing: Option<HashMap<String, usize>>,
    pub num_absent: Option<HashMap<String, usize>>,
    pub num_absent_and_checked_in: Option<usize>,
}

lazy_static! {
    pub static ref TEACHERS: Vec<TeacherConf> =
        serde_json::from_str(include_str!("../data/teachers.json")).unwrap();
}

// data as it is configured in keycloak test instance
static TESTSEKI01_USERNAME: &str = "testseki01";
static TESTSEKI01_PASSWORD: &str = "testseki01";
static TESTSEKI01_UUID: &str = "1115cbc8-cbb7-4250-ab00-2e2003a58f5b";

static TESTUSER001_USERNAME: &str = "testuser001";
static TESTUSER001_PASSWORD: &str = "testuser001";
static TESTUSER001_UUID: &str = "d26a3222-3c06-4d75-9f01-9b1c2a3d9e4f";

static CLIENT_SECRET: &str = "zpLfQrWxWEknlpPEaxdF6scPDw3nEvOU";

static RUNNING_SERVICE: Once = Once::new();
static ADD_KC_MEMBERS: Once = Once::new();
lazy_static! {
    pub static ref PORT: u16 = std::env::var("PORT").unwrap().parse().unwrap();
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ResponsibleFor {
    pub members: Option<Vec<Uuid>>,
    pub groups: Option<Vec<Uuid>>,
    pub happenings: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ParticipantIn {
    pub groups: Option<Vec<Uuid>>,
    pub happenings: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct PhysicalId {
    pub id: Uuid,
    pub deactivated: bool,
    pub note: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct NewMember {
    pub first_name: String,
    pub middle_names: Option<String>,
    pub last_name: String,
    pub person_qualifier: Option<String>,
    pub identity_id: Option<Uuid>,
    pub responsibles: Option<Vec<Uuid>>,
    pub responsible_for: Option<ResponsibleFor>,
    pub participant_in: Option<ParticipantIn>,
    pub physical_ids: Option<Vec<PhysicalId>>,
    pub contact: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Member {
    pub id: Uuid,
    pub first_name: String,
    pub middle_names: Option<String>,
    pub last_name: String,
    pub person_qualifier: Option<String>,
    pub identity_id: Option<Uuid>,
    pub responsibles: Option<Vec<Uuid>>,
    pub responsible_for: Option<ResponsibleFor>,
    pub participant_in: Option<ParticipantIn>,
    pub physical_ids: Option<Vec<PhysicalId>>,
    pub contact: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct NewGroup {
    pub name: String,
    pub responsibles: Option<Vec<Uuid>>,
    pub participants: Option<Vec<Uuid>>,
    pub happenings: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Group {
    pub id: Uuid,
    pub name: String,
    pub responsibles: Option<Vec<Uuid>>,
    pub participants: Option<Vec<Uuid>>,
    pub happenings: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct NewHappening {
    pub name: String,
    pub props: Option<serde_json::Value>,
    pub responsibles: Option<Vec<Uuid>>,
    pub participants: Option<Vec<Uuid>>,
    pub groups: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct Happening {
    pub id: Uuid,
    pub name: String,
    pub props: Option<serde_json::Value>,
    pub responsibles: Option<Vec<Uuid>>,
    pub participants: Option<Vec<Uuid>>,
    pub groups: Option<Vec<Uuid>>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct HappeningShortInfo {
    pub id: Uuid,
    pub name: String,
    pub props: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ShortInfo {
    pub id: Uuid,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct MemberShortInfo {
    pub id: Uuid,
    pub first_name: String,
    pub last_name: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct MemberInfo {
    pub id: Uuid,
    pub first_name: String,
    pub middle_names: Option<String>,
    pub last_name: String,
    pub person_qualifier: Option<String>,
    pub absent: Option<serde_json::Value>,
    pub checked_in: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct HappeningDashboard {
    pub id: Uuid,
    pub name: String,
    pub props: Option<serde_json::Value>,
    pub responsibles: Option<Vec<MemberShortInfo>>,
    pub groups: Option<Vec<ShortInfo>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Dashboard {
    pub my_happening: HappeningDashboard,
    pub related_happenings: Option<Vec<HappeningDashboard>>,
    pub number_request: NumberRequest,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NumberRequest {
    pub happenings: Vec<Uuid>,
    pub groups: Vec<Uuid>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NumberResponse {
    pub happenings: HashMap<Uuid, u16>,
    pub expected: u16,
    pub missing: u16,
    pub absent: u16,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct NewAbsent {
    pub member_id: Uuid,
    pub reason: String,
    pub from_date: String,
    pub until_date: String,
    pub canceled: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Absent {
    pub id: Uuid,
    pub member_id: Uuid,
    pub reason: String,
    pub from_date: String,
    pub until_date: String,
    pub canceled: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Dummy {}

#[derive(Debug)]
pub enum RequestMethod {
    Get,
    Pos,
    Put,
    Del,
}

pub fn get(path: &str, token: Option<&str>) -> Response {
    let req = if let Some(tk) = token {
        ureq::get(&format!("http://localhost:{}{}", *PORT, path))
            .set("Authorization", &format!("Bearer {}", tk))
    } else {
        ureq::get(&format!("http://localhost:{}{}", *PORT, path))
    };
    match req.set("accept", "application/json").call() {
        Ok(response) => response,
        Err(ureq::Error::Status(_, response)) => response,
        Err(err) => panic!("Error sending get request: {}", err),
    }
}

pub fn post(path: &str, token: Option<&str>, data: impl Serialize) -> Response {
    let req = if let Some(tk) = token {
        ureq::post(&format!("http://localhost:{}{}", *PORT, path))
            .set("Authorization", &format!("Bearer {}", tk))
    } else {
        ureq::post(&format!("http://localhost:{}{}", *PORT, path))
    };
    match req
        .set("accept", "application/json")
        .set("Content-Type", "application/json")
        .send_json(data)
    {
        Ok(response) => response,
        Err(ureq::Error::Status(_, response)) => response,
        Err(err) => panic!("Error sending post request: {}", err),
    }
}

pub fn put(path: &str, token: Option<&str>, data: impl Serialize) -> Response {
    let req = if let Some(tk) = token {
        ureq::put(&format!("http://localhost:{}{}", *PORT, path))
            .set("Authorization", &format!("Bearer {}", tk))
    } else {
        ureq::put(&format!("http://localhost:{}{}", *PORT, path))
    };
    match req
        .set("accept", "application/json")
        .set("Content-Type", "application/json")
        .send_json(data)
    {
        Ok(response) => response,
        Err(ureq::Error::Status(_, response)) => response,
        Err(err) => panic!("Error sending put request: {}", err),
    }
}

pub fn delete(path: &str, token: Option<&str>) -> Response {
    let req = if let Some(tk) = token {
        ureq::delete(&format!("http://localhost:{}{}", *PORT, path))
            .set("Authorization", &format!("Bearer {}", tk))
    } else {
        ureq::delete(&format!("http://localhost:{}{}", *PORT, path))
    };
    match req.set("accept", "application/json").call() {
        Ok(response) => response,
        Err(ureq::Error::Status(_, response)) => response,
        Err(err) => panic!("Error sending put request: {}", err),
    }
}

pub fn expect_running_service() {
    RUNNING_SERVICE.call_once(|| {
        println!("Check if service is running");
        loop {
            if get("/status", None).status() == 200 {
                println!("... Ok");
                break;
            }
            println!("... not ready");
            thread::sleep(time::Duration::from_secs(1));
        }
    });
}

pub fn get_kc_teacher_token(username: &str) -> String {
    for teacher in TEACHERS.iter() {
        if teacher.username == username {
            return get_kc_token(&teacher.username, &teacher.password);
        }
    }
    panic!("username not found")
}

pub fn add_teacher_and_get_config(atk: &str, username: &str) -> TeacherConf {
    for teacher in TEACHERS.iter() {
        if teacher.username == username {
            let ttk = get_kc_token(&teacher.username, &teacher.password);
            let rsp = get("/api/v1/me", Some(&ttk));
            if rsp.status() != 200 {
                let rsp = post(
                    "/api/v1/admin/member",
                    Some(atk),
                    NewMember {
                        first_name: teacher.first_name.clone(),
                        middle_names: teacher.middle_names.clone(),
                        last_name: teacher.last_name.clone(),
                        person_qualifier: None,
                        identity_id: Some(teacher.identity_id),
                        responsibles: None,
                        responsible_for: None,
                        participant_in: None,
                        physical_ids: None,
                        contact: None,
                    },
                );
                assert_eq!(201, rsp.status());
            }
            let rsp = get("/api/v1/me", Some(&ttk));
            assert_eq!(200, rsp.status());
            let uuid = rsp.into_json::<MemberShortInfo>().unwrap().id;

            return TeacherConf {
                first_name: teacher.first_name.clone(),
                middle_names: teacher.middle_names.clone(),
                last_name: teacher.last_name.clone(),
                uuid: Some(uuid),
                username: teacher.username.clone(),
                password: teacher.password.clone(),
                identity_id: teacher.identity_id,
            };
        }
    }
    panic!("username not found")
}

pub fn get_kc_admin_token() -> String {
    get_kc_token(TESTSEKI01_USERNAME, TESTSEKI01_PASSWORD)
}

pub fn get_kc_user_token() -> String {
    get_kc_token(TESTUSER001_USERNAME, TESTUSER001_PASSWORD)
}

fn get_kc_token(username: &str, password: &str) -> String {
    #[derive(Deserialize)]
    struct Token {
        access_token: String,
    }
    let response = ureq::post("http://localhost:8282/realms/esbz/protocol/openid-connect/token")
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send_form(&[
            ("client_id", "binda-ui_access"),
            ("grant_type", "password"),
            ("username", username),
            ("password", password),
            ("scope", "openid"),
            ("client_secret", CLIENT_SECRET),
        ])
        .unwrap();
    assert_eq!(response.status(), 200);
    response.into_json::<Token>().unwrap().access_token
}

pub fn add_members(token: &str, number: u16, name_prefix: &str) -> Vec<Member> {
    let mut members = vec![];
    for ii in 0..number {
        let rsp = post(
            "/api/v1/admin/member",
            Some(token),
            NewMember {
                first_name: format!("{}", name_prefix),
                middle_names: None,
                last_name: format!("Nr {}", ii),
                person_qualifier: None,
                identity_id: None,
                responsibles: None,
                responsible_for: None,
                participant_in: None,
                physical_ids: None,
                contact: None,
            },
        );
        assert_eq!(201, rsp.status());
        members.push(rsp.into_json::<Member>().unwrap());
    }
    members
}

fn add_keycloak_member(atk: &str, first_name: &str, last_name: &str, kc_uuid: Uuid) {
    let rsp = post(
        "/api/v1/admin/member",
        Some(atk),
        NewMember {
            first_name: first_name.into(),
            middle_names: None,
            last_name: last_name.into(),
            person_qualifier: None,
            identity_id: Some(kc_uuid),
            responsibles: None,
            responsible_for: Some(ResponsibleFor {
                members: None,
                groups: None,
                happenings: None,
            }),
            participant_in: None,
            physical_ids: None,
            contact: None,
        },
    );
    if ![201, 400].contains(&rsp.status()) {
        panic!("Failed to add keycloak member");
    }
}

pub fn add_keycloak_members() {
    ADD_KC_MEMBERS.call_once(|| {
        println!("Add keycloak members");
        let atk = get_kc_admin_token();
        add_keycloak_member(
            &atk,
            "Test",
            "Seki01",
            Uuid::parse_str(TESTSEKI01_UUID).unwrap(),
        );
        add_keycloak_member(
            &atk,
            "Test",
            "User001",
            Uuid::parse_str(TESTUSER001_UUID).unwrap(),
        );
        for teacher in TEACHERS.iter() {
            add_keycloak_member(
                &atk,
                &teacher.first_name,
                &teacher.last_name,
                teacher.identity_id,
            );
        }
    });
}

pub fn add_group(atk: &str, group: &mut GroupConf, responsibles: &[TeacherConf]) {
    // we need to filter responsibles for those that are actually configured in group
    let rsp = post(
        "/api/v1/admin/group",
        Some(atk),
        NewGroup {
            name: group.name.clone(),
            responsibles: Some(
                responsibles
                    .iter()
                    .filter(|tc| group.responsibles.contains(&tc.username))
                    .map(|tc| tc.uuid.unwrap())
                    .collect(),
            ),
            participants: None,
            happenings: None,
        },
    );
    assert_eq!(201, rsp.status());
    group.uuid = Some(rsp.into_json::<Group>().unwrap().id);
}

pub fn add_happening(atk: &str, happening: &mut HappeningConf, groups: &[GroupConf]) {
    // we need to filter responsibles and groups for those that are actually configured in
    // happening
    let props = if let Some(auto_checkout) = happening.auto_checkout {
        serde_json::from_str(&format!(
            r#"{{
                "from": "{}",
                "to": "{}",
                "auto_checkout": {}
            }}"#,
            happening.from, happening.to, auto_checkout
        ))
        .unwrap()
    } else {
        serde_json::from_str(&format!(
            r#"{{
                "from": "{}",
                "to": "{}"
            }}"#,
            happening.from, happening.to,
        ))
        .unwrap()
    };
    let rsp = post(
        "/api/v1/admin/happening",
        Some(atk),
        NewHappening {
            name: happening.name.clone(),
            props,
            // rely on group responsibles only
            responsibles: None,
            participants: None,
            groups: Some(
                groups
                    .iter()
                    .filter(|gc| happening.groups.contains(&gc.name))
                    .map(|gc| gc.uuid.unwrap())
                    .collect(),
            ),
        },
    );
    assert_eq!(201, rsp.status());
    happening.uuid = Some(rsp.into_json::<Group>().unwrap().id);
}

pub fn add_student(atk: &str, student: &mut StudentConf, groups: &[GroupConf]) {
    // we need to filter the one group that is actually configured in student
    let rsp = post(
        "/api/v1/admin/member",
        Some(atk),
        NewMember {
            first_name: student.first_name.clone(),
            middle_names: student.middle_names.clone(),
            last_name: student.last_name.clone(),
            person_qualifier: student.person_qualifier.clone(),
            identity_id: None,
            responsibles: None,
            responsible_for: None,
            participant_in: Some(ParticipantIn {
                groups: Some(
                    groups
                        .iter()
                        .filter(|gc| student.group == gc.name)
                        .map(|gc| gc.uuid.unwrap())
                        .collect(),
                ),
                happenings: None,
            }),
            physical_ids: student.physical_ids.clone().map(|vec| {
                vec.iter()
                    .map(|pid| PhysicalId {
                        id: *pid,
                        deactivated: false,
                        note: None,
                    })
                    .collect::<Vec<_>>()
            }),
            contact: None,
        },
    );
    assert_eq!(201, rsp.status());
    student.uuid = Some(rsp.into_json::<Member>().unwrap().id);
}
