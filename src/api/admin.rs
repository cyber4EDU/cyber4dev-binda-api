#![allow(deprecated)]
use super::{responses::JsonOk, Sorting};
use crate::api::SortOrder;
use crate::data::{
    dashboard::{AdminDashboard, AdminNumberRequest, AdminNumberResponse},
    Absent, AbsentId, Action, Delay, Group, GroupId, Happening, HappeningId, Member, MemberId,
    MemberIdVariant, MemberIdentityId, MemberInfo, MemberInfoSortKeys, MemberPhysicalId, NewAbsent,
    NewGroup, NewHappening, NewMember, NewPhysicalData, PhysicalData, Search, TimeData,
    TimeFrameNotBefore,
};
use crate::error::Error;
use actix_web::web;
use actix_web::{delete, get, post, put, HttpResponse};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use chrono::Utc;
use sea_orm::DatabaseConnection;

data_io!(
    "member",
    Member,
    NewMember,
    MemberId,
    "/member",
    "/member/{id}"
);
data_io!("group", Group, NewGroup, GroupId, "/group", "/group/{id}");
data_io!(
    "happening",
    Happening,
    NewHappening,
    HappeningId,
    "/happening",
    "/happening/{id}"
);
data_io!(
    "absent",
    Absent,
    NewAbsent,
    AbsentId,
    "/absent",
    "/absent/{id}"
);
data_io!(
    "physical-id",
    PhysicalData,
    NewPhysicalData,
    MemberPhysicalId,
    "/physical_id",
    "/physical_id/{id}"
);

/// Get entry by identity id
#[utoipa::path(
    context_path = "/admin",
    tag = "admin",
    responses(
        (status = 200, description = "Info returned", body = Member),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("iid" = MemberIdentityId, Path, description = "Member identity id")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/iid/{iid}")]
async fn get_admin_member_iid(
    _auth: BearerAuth,
    conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
    iid: actix_web::web::Path<MemberIdentityId>,
) -> Result<super::responses::JsonOk<Member>, crate::error::Error> {
    Ok(super::responses::JsonOk::from(
        Member::load_by_iid(&conn, iid.into_inner()).await?,
    ))
}

/// Get entry by physical id
#[utoipa::path(
    context_path = "/admin",
    tag = "admin",
    responses(
        (status = 200, description = "Info returned", body = Member),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("pid" = MemberPhysicalId, Path, description = "Member physical id")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/pid/{pid}")]
async fn get_admin_member_pid(
    _auth: BearerAuth,
    conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
    pid: actix_web::web::Path<MemberPhysicalId>,
) -> Result<super::responses::JsonOk<Member>, crate::error::Error> {
    Ok(super::responses::JsonOk::from(
        Member::load_by_pid(&conn, pid.into_inner()).await?,
    ))
}

/// deprecated: Get status information of one member by id
///
/// Use `/info/member/{id}` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = MemberId, Path, description = "Id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/member/{id}")]
#[deprecated(note = "use get_info_member instead")]
async fn get_admin_info_member(
    _auth: BearerAuth,
    conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
    timedata: web::Query<TimeData>,
    id: actix_web::web::Path<MemberId>,
) -> Result<super::responses::JsonOk<MemberInfo>, crate::error::Error> {
    Ok(super::responses::JsonOk::from(
        MemberInfo::load_info(
            &conn,
            MemberIdVariant::Id(&id.into_inner()),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

/// deprecated: Get status information of one member by identity id
///
/// Use `/info/member/iid/{iid}` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("iid" = MemberIdentityId, Path, description = "Identity id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/member/iid/{iid}")]
#[deprecated(note = "use get_info_member_iid instead")]
async fn get_admin_info_member_iid(
    _auth: BearerAuth,
    conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
    timedata: web::Query<TimeData>,
    iid: actix_web::web::Path<MemberIdentityId>,
) -> Result<super::responses::JsonOk<MemberInfo>, crate::error::Error> {
    Ok(super::responses::JsonOk::from(
        MemberInfo::load_info(
            &conn,
            MemberIdVariant::Iid(&iid.into_inner()),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

/// deprecated: Get status information of one member by physical id
///
/// Use `/info/member/pid/{pid}` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("pid" = MemberPhysicalId, Path, description = "Physical id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/member/pid/{pid}")]
#[deprecated(note = "use get_info_member_pid instead")]
async fn get_admin_info_member_pid(
    _auth: BearerAuth,
    conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
    timedata: web::Query<TimeData>,
    pid: actix_web::web::Path<MemberPhysicalId>,
) -> Result<super::responses::JsonOk<MemberInfo>, crate::error::Error> {
    Ok(super::responses::JsonOk::from(
        MemberInfo::load_info(
            &conn,
            MemberIdVariant::Pid(&pid.into_inner()),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

/// deprecated: Get status information of all members
///
/// Use `/info/members` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/members")]
#[deprecated(note = "use get_info_members instead")]
async fn get_admin_info_members(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_all(
            &conn,
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// deprecated: Get status information of all members that are absent
///
/// Use `/info/members/absent` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/members/absent")]
#[deprecated(note = "use get_info_members_absent instead")]
async fn get_admin_info_members_absent(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_all_absent(
            &conn,
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// deprecated: Get status information of all members that are missing
///
/// Use `/info/members/missing` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/members/missing")]
#[deprecated(note = "use get_info_members_missing instead")]
async fn get_admin_info_members_missing(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_all_missing(
            &conn,
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// deprecated: Get status information about participants of a specific group, that are also checked-in
///
/// Use `/info/group/{id}/participants/checked_in` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = GroupId, Path, description = "Id of group"),
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/group/{id}/participants/checked_in")]
#[deprecated(note = "use get_info_group_participant_checked_in instead")]
async fn get_admin_info_group_participants_checked_in(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<GroupId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_group_checked_in(
            &conn,
            id.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get list of all physical ids
#[utoipa::path(
    context_path = "/admin",
    tag = "admin",
    responses(
        (status = 200, description = "Info returned", body = Vec<PhysicalData>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/physical_ids")]
async fn get_admin_physicaldatas(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
) -> Result<JsonOk<Vec<PhysicalData>>, Error> {
    Ok(JsonOk::from(PhysicalData::load_all(&conn).await?))
}

/// Get list of all groups
#[utoipa::path(
    context_path = "/admin",
    tag = "admin",
    responses(
        (status = 200, description = "Info returned", body = Vec<Group>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/groups")]
async fn get_admin_groups(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
) -> Result<JsonOk<Vec<Group>>, Error> {
    Ok(JsonOk::from(Group::load_all(&conn).await?))
}

/// Get list of all happenings
#[utoipa::path(
    context_path = "/admin",
    tag = "admin",
    responses(
        (status = 200, description = "Info returned", body = Vec<Happening>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("not_before" = Option<DateTime<Utc>>, Query, description = "Filter not before")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/happenings")]
async fn get_admin_happenings(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timeframe: web::Query<TimeFrameNotBefore>,
) -> Result<JsonOk<Vec<Happening>>, Error> {
    Ok(JsonOk::from(
        Happening::load_all(&conn, &timeframe.into_inner().not_before).await?,
    ))
}

/// deprecated: Get all absent entries of a member
///
/// Use `/info/absents/member/{id}` instead
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-info",
    responses(
        (status = 200, description = "Absents returned", body = Vec<Absent>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = MemberId, Path, description = "Member id")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/absents/member/{id}")]
#[deprecated(note = "use get_info_absents_member instead")]
async fn get_admin_info_absents_member(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<MemberId>,
) -> Result<JsonOk<Vec<Absent>>, Error> {
    let abs = Absent::load_by_member(&conn, id.into_inner()).await?;
    Ok(JsonOk::from(abs))
}

/// Get dashboard for admin
#[utoipa::path(
    context_path = "/admin",
    tag = "admin-dashboard",
    responses(
        (status = 200, description = "Info returned", body = AdminDashboard),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/info/dashboard")]
async fn get_admin_dashboard(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<AdminDashboard>, Error> {
    let now = timedata.into_inner().now.unwrap_or(Utc::now());
    Ok(JsonOk::from(AdminDashboard::load(&conn, now).await?))
}

#[utoipa::path(
    context_path = "/admin",
    tag = "admin-dashboard",
    responses(
        (status = 200, description = "Info returned", body = AdminNumberResponse),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    request_body = AdminNumberRequest,
    security(
        ("api_access_token" = [])
    )
)]
#[post("/info/dashboard/numbers")]
async fn post_admin_dashboard_numbers(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    body: web::Json<AdminNumberRequest>,
) -> Result<JsonOk<AdminNumberResponse>, Error> {
    let now = timedata.into_inner().now.unwrap_or(Utc::now());
    Ok(JsonOk::from(
        AdminNumberResponse::load_from(&conn, now, &body.into_inner()).await?,
    ))
}

async fn admin_checkout(
    conn: web::Data<DatabaseConnection>,
    id: MemberIdVariant<'_>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    let now = timedata.into_inner().now.unwrap_or(Utc::now());

    let member_info = MemberInfo::load_info(&conn, id, now).await?;
    if let Some(happening_info) = member_info.checked_in {
        Action::check_out_with_id_variant(&conn, &happening_info.id, id, &delay.into_inner(), &now)
            .await?;
        Ok(HttpResponse::Ok().finish())
    } else {
        Ok(HttpResponse::NoContent().finish())
    }
}

#[utoipa::path(
    context_path = "/admin",
    tag = "admin-actions",
    responses(
        (status = 200, description = "Member checked out"),
        (status = 204, description = "Not checked-in"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = MemberId, Path, description = "Id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/check_out/{id}")]
async fn post_admin_check_out(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<MemberId>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    admin_checkout(conn, MemberIdVariant::Id(&id), delay, timedata).await
}

#[utoipa::path(
    context_path = "/admin",
    tag = "admin-actions",
    responses(
        (status = 200, description = "Member checked out"),
        (status = 204, description = "Not checked-in"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("iid" = MemberId, Path, description = "Identity id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/check_out/iid/{iid}")]
async fn post_admin_check_out_iid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    iid: web::Path<MemberIdentityId>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    admin_checkout(conn, MemberIdVariant::Iid(&iid), delay, timedata).await
}

#[utoipa::path(
    context_path = "/admin",
    tag = "admin-actions",
    responses(
        (status = 200, description = "Member checked out"),
        (status = 204, description = "Not checked-in"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("pid" = MemberId, Path, description = "Physical id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/check_out/pid/{pid}")]
async fn post_admin_check_out_pid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    pid: web::Path<MemberPhysicalId>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    admin_checkout(conn, MemberIdVariant::Pid(&pid), delay, timedata).await
}
