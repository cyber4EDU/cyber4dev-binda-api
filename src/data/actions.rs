use super::{
    Action, Check, Delay, HappeningId, HappeningShortInfo, MemberId, MemberIdVariant, MemberInfo,
    MemberPhysicalId,
};
use crate::api::SortOrder;
use crate::error::Error;
use crate::model::{prelude::*, *};
use chrono::{DateTime, Duration, Utc};
use sea_orm::{
    ActiveModelTrait, ActiveValue, ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter,
    QuerySelect,
};
use sea_orm_active_enums::CheckType;

impl Action {
    async fn check_in(
        conn: &DatabaseConnection,
        happening_id: &HappeningId,
        member_id: &MemberId,
        delay: &Delay,
        now: &DateTime<Utc>,
        physical_id: Option<MemberPhysicalId>,
    ) -> Result<HappeningShortInfo, Error> {
        let checked_in_now = HappeningShortInfo::load(conn, happening_id).await?;

        let mut events = vec![check_in_out::ActiveModel {
            member_id: ActiveValue::Set(Some(member_id.into())),
            happening_id: ActiveValue::Set(happening_id.into()),
            date: ActiveValue::Set((*now).into()),
            check_type: ActiveValue::Set(CheckType::In),
            delay_minutes: ActiveValue::Set(delay.minutes),
            physical_id: ActiveValue::Set(physical_id.map(|pid| pid.into())),
            ..ActiveModelTrait::default()
        }];

        // if auto_checkout option exists, also write checkout-entry
        if let Some(props) = &checked_in_now.props {
            if let Some(auto_checkout) = props.auto_checkout {
                events.push(check_in_out::ActiveModel {
                    member_id: ActiveValue::Set(Some(member_id.into())),
                    happening_id: ActiveValue::Set(happening_id.into()),
                    date: ActiveValue::Set((props.to + Duration::minutes(auto_checkout)).into()),
                    check_type: ActiveValue::Set(CheckType::Out),
                    delay_minutes: ActiveValue::Set(None),
                    physical_id: ActiveValue::Set(None),
                    ..ActiveModelTrait::default()
                });
            };
        };

        CheckInOut::insert_many(events).exec(conn).await?;

        Ok(checked_in_now)
    }

    pub async fn try_check_in_with_id_variant(
        conn: &DatabaseConnection,
        happening_id: HappeningId,
        member_id: MemberIdVariant<'_>,
        delay: Delay,
        now: DateTime<Utc>,
    ) -> Result<MemberInfo, Error> {
        let mut member_info = MemberInfo::load_info(conn, member_id, now).await?;
        let physical_id = if let MemberIdVariant::Pid(pid) = member_id {
            Some(*pid)
        } else {
            None
        };

        if let Some(ci) = member_info.checked_in {
            if ci.id == happening_id {
                // already checked in
                // return with checked_in_before
                member_info.checked_in_before = Some(ci);
                member_info.checked_in = None;
            } else {
                // checked in somewhere else
                // -> checkout from ci.id
                Self::check_out(conn, &ci.id, &member_info.id, &delay, &now, physical_id).await?;

                // -> checkin to happening_id
                let checked_in_now = Self::check_in(
                    conn,
                    &happening_id,
                    &member_info.id,
                    &delay,
                    &now,
                    physical_id,
                )
                .await?;

                member_info.checked_in_before = Some(ci);
                member_info.checked_in = Some(checked_in_now);
            }
        } else {
            // checked in no where
            // -> checkin to happening_id
            let checked_in_now = Self::check_in(
                conn,
                &happening_id,
                &member_info.id,
                &delay,
                &now,
                physical_id,
            )
            .await?;

            member_info.checked_in = Some(checked_in_now);
        }

        Ok(member_info)
    }

    async fn check_out(
        conn: &DatabaseConnection,
        happening_id: &HappeningId,
        member_id: &MemberId,
        delay: &Delay,
        now: &DateTime<Utc>,
        physical_id: Option<MemberPhysicalId>,
    ) -> Result<(), Error> {
        check_in_out::ActiveModel {
            member_id: ActiveValue::Set(Some(member_id.into())),
            happening_id: ActiveValue::Set(happening_id.into()),
            date: ActiveValue::Set((*now).into()),
            check_type: ActiveValue::Set(CheckType::Out),
            delay_minutes: ActiveValue::Set(delay.minutes),
            physical_id: ActiveValue::Set(physical_id.map(|pid| pid.into())),
            ..ActiveModelTrait::default()
        }
        .insert(conn)
        .await?;
        Ok(())
    }

    pub async fn check_out_with_id_variant(
        conn: &DatabaseConnection,
        happening_id: &HappeningId,
        member_id: MemberIdVariant<'_>,
        delay: &Delay,
        now: &DateTime<Utc>,
    ) -> Result<(), Error> {
        let (member_id, physical_id): (MemberId, Option<MemberPhysicalId>) = match member_id {
            MemberIdVariant::Id(id) => (*id, None),
            MemberIdVariant::Iid(iid) => (
                MemberId(
                    Members::find()
                        .filter(members::Column::IdentityId.eq(iid.0))
                        .select_only()
                        .column(members::Column::IdentityId)
                        .one(conn)
                        .await?
                        .ok_or(Error::NotFound("Identity id not found"))?
                        .id,
                ),
                None,
            ),
            MemberIdVariant::Pid(pid) => (
                MemberId(
                    PhysicalIds::find()
                        .filter(physical_ids::Column::Id.eq(pid.0))
                        .select_only()
                        .column(physical_ids::Column::MemberId)
                        .one(conn)
                        .await?
                        .ok_or(Error::NotFound("Physical id not found"))?
                        .member_id
                        .ok_or(Error::Forbidden(
                            "Physical id found but no member id attached",
                        ))?,
                ),
                Some(*pid),
            ),
        };
        Self::check_out(conn, happening_id, &member_id, delay, now, physical_id).await?;
        Ok(())
    }

    pub async fn close(
        conn: &DatabaseConnection,
        happening_id: HappeningId,
        delay: Delay,
        now: DateTime<Utc>,
    ) -> Result<(), Error> {
        CheckInOut::insert_many(
            MemberInfo::load_info_happening_checked_in(
                conn,
                happening_id,
                now,
                crate::data::sorting::Sorting {
                    sort_by: crate::data::sorting::MemberInfoCheckedInSortColumns::Members(
                        crate::model::members::Column::Id,
                    ),
                    order: SortOrder::default(),
                },
                None,
            )
            .await?
            .into_iter()
            .map(|m| check_in_out::ActiveModel {
                member_id: ActiveValue::Set(Some(m.id.into())),
                happening_id: ActiveValue::Set(happening_id.into()),
                date: ActiveValue::Set(now.into()),
                check_type: ActiveValue::Set(CheckType::Out),
                delay_minutes: ActiveValue::Set(delay.minutes),
                physical_id: ActiveValue::Set(None),
                ..ActiveModelTrait::default()
            }),
        )
        .exec(conn)
        .await?;
        Ok(())
    }
}

impl Check {
    pub async fn load(
        conn: &DatabaseConnection,
        member_id: &MemberId,
        happening_ids: &[HappeningId],
    ) -> Result<Vec<Self>, Error> {
        // load check_in_outs
        Ok(CheckInOut::find()
            .filter(check_in_out::Column::MemberId.eq(member_id.0))
            .filter(check_in_out::Column::HappeningId.is_in(happening_ids.iter().map(|hid| hid.0)))
            .all(conn)
            .await?
            .into_iter()
            .map(|c| Check {
                happening_id: c.happening_id.into(),
                check_type: c.check_type,
                date: c.date.into(),
            })
            .collect())
    }
}
