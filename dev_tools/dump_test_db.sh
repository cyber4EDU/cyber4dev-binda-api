#!/bin/bash

if [ -z ${PGUSER} ] || [ -z ${PGPASSWORD} ] || [ -z ${PGDATABASE} ];
then
    echo "start script from within pg_virtualenv"
    exit 1
else
    # use postgres from e.g. pg_virtualenv
    export PORT=8001
    export DATABASE_URL="postgres://$PGUSER:$PGPASSWORD@localhost/$PGDATABASE"
fi

./tests/run_test.sh

pg_dump --data-only > ../db/binda_api/dump-small.sql
