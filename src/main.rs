mod api;
mod api_docs;
mod auth;
mod config;
mod data;
mod error;
mod migrator;
mod model;

use crate::auth::{http_auth, Auth, Claims};
use crate::config::Config;
use crate::migrator::Migrator;
use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use actix_web_prom::PrometheusMetricsBuilder;
use env_logger::Env;
use sea_orm::Database;
use sea_orm_migration::MigratorTrait;
use std::io::{
    Error,
    ErrorKind::{ConnectionRefused, Other},
};
use std::sync::Arc;
use tracing_actix_web::TracingLogger;
use utoipa::OpenApi;
use utoipa_swagger_ui::{SwaggerUi, Url};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(Env::default().default_filter_or("info"));

    let conn = Database::connect(Config::get_db_connection_options()?)
        .await
        .map_err(|err| {
            Error::new(
                ConnectionRefused,
                format!("Failed to connect to database: {err}"),
            )
        })?;

    let auth = Arc::new(Auth::new().await.map_err(|err| {
        Error::new(
            ConnectionRefused,
            format!("Failed to connect to keycloak: {err}"),
        )
    })?);

    Migrator::up(&conn, None)
        .await
        .map_err(|err| Error::new(Other, format!("Failed to run migration: {err}")))?;

    let prometheus = PrometheusMetricsBuilder::new("api")
        .endpoint("/metrics")
        .build()
        .unwrap();

    let admin_auth = http_auth!(auth, |c: Claims| c
        .has_role(config::Config::get_admin_role()));
    let teacher_auth = http_auth!(auth, |c: Claims| c
        .has_role(config::Config::get_teacher_role()));
    let admin_or_teacher_auth = http_auth!(auth, |c: Claims| c
        .has_role(config::Config::get_admin_role())
        || c.has_role(config::Config::get_teacher_role()));

    HttpServer::new(move || {
        let cors = if Config::cors_permissive() {
            Cors::permissive()
        } else {
            Cors::default()
        };
        App::new()
            .wrap(cors)
            .wrap(TracingLogger::default())
            .wrap(prometheus.clone())
            .app_data(web::Data::new(conn.clone()))
            .service(api::version)
            .service(api::status)
            .service(SwaggerUi::new("/swagger/{_:.*}").url(
                Url::new("/api/v1", "/docs/openapi_v1.json"),
                api_docs::ApiDoc::openapi(),
            ))
            .service(
                web::scope("/api/v1")
                    .service(api::version)
                    .service(api::status)
                    .service(
                        web::scope("/admin")
                            .wrap(admin_auth.clone())
                            // member
                            .service(api::admin::post_admin_member)
                            .service(api::admin::get_admin_member)
                            .service(api::admin::get_admin_member_iid)
                            .service(api::admin::get_admin_member_pid)
                            // --- deprecated ---
                            .service(api::admin::get_admin_info_member)
                            .service(api::admin::get_admin_info_member_iid)
                            .service(api::admin::get_admin_info_member_pid)
                            .service(api::admin::get_admin_info_members)
                            .service(api::admin::get_admin_info_members_absent)
                            .service(api::admin::get_admin_info_members_missing)
                            .service(api::admin::get_admin_info_absents_member)
                            .service(api::admin::get_admin_info_group_participants_checked_in)
                            // ------------------
                            .service(api::admin::put_admin_member)
                            .service(api::admin::delete_admin_member)
                            // group
                            .service(api::admin::post_admin_group)
                            .service(api::admin::get_admin_group)
                            .service(api::admin::put_admin_group)
                            .service(api::admin::delete_admin_group)
                            .service(api::admin::get_admin_groups)
                            // happening
                            .service(api::admin::post_admin_happening)
                            .service(api::admin::get_admin_happening)
                            .service(api::admin::put_admin_happening)
                            .service(api::admin::delete_admin_happening)
                            .service(api::admin::get_admin_happenings)
                            // absent
                            .service(api::admin::post_admin_absent)
                            .service(api::admin::get_admin_absent)
                            .service(api::admin::put_admin_absent)
                            .service(api::admin::delete_admin_absent)
                            // physical ids
                            .service(api::admin::post_admin_physicaldata)
                            .service(api::admin::get_admin_physicaldata)
                            .service(api::admin::put_admin_physicaldata)
                            .service(api::admin::delete_admin_physicaldata)
                            .service(api::admin::get_admin_physicaldatas)
                            // dashboard
                            .service(api::admin::get_admin_dashboard)
                            .service(api::admin::post_admin_dashboard_numbers)
                            // actions
                            .service(api::admin::post_admin_check_out)
                            .service(api::admin::post_admin_check_out_iid)
                            .service(api::admin::post_admin_check_out_pid),
                    )
                    .service(
                        web::scope("/me")
                            .service(api::me::get_me)
                            .service(api::me::get_me_happenings)
                            .service(api::me::get_me_dashboard)
                            .service(api::me::post_me_dashboard_numbers)
                            .wrap(admin_or_teacher_auth.clone()),
                    )
                    .service(
                        web::scope("/info")
                            // member
                            .service(api::info::get_info_member)
                            .service(api::info::get_info_member_kc)
                            .service(api::info::get_info_member_iid)
                            .service(api::info::get_info_member_pid)
                            .service(api::info::get_info_members)
                            .service(api::info::get_info_members_absent)
                            .service(api::info::get_info_members_missing)
                            // group
                            .service(api::info::get_info_group_participants)
                            .service(api::info::get_info_group_participants_checked_in)
                            .service(api::info::get_info_group_participants_absent)
                            .service(api::info::get_info_group_participants_missing)
                            // happening
                            .service(api::info::get_info_happening_participants)
                            .service(api::info::get_info_happening_participants_checked_in)
                            // history
                            .service(api::history::get_info_history_member)
                            .wrap(admin_or_teacher_auth.clone()),
                    )
                    .service(
                        web::scope("/info")
                            // dashboard
                            .service(api::info::get_info_dashboard_happening)
                            .service(api::info::post_info_dashboard_numbers)
                            .wrap(teacher_auth.clone()),
                    )
                    .service(
                        web::scope("/actions")
                            .service(api::actions::post_actions_happening_check_in)
                            .service(api::actions::post_actions_happening_check_out)
                            .service(api::actions::post_actions_happening_check_in_kc)
                            .service(api::actions::post_actions_happening_check_in_iid)
                            .service(api::actions::post_actions_happening_check_in_pid)
                            .service(api::actions::post_actions_happening_check_out_kc)
                            .service(api::actions::post_actions_happening_check_out_iid)
                            .service(api::actions::post_actions_happening_check_out_pid)
                            .service(api::actions::post_actions_happening_close)
                            .wrap(teacher_auth.clone()),
                    )
                    .service(
                        web::scope("/absent")
                            .service(api::absents::post_absent)
                            .service(api::absents::get_absent)
                            .service(api::absents::put_absent)
                            .service(api::absents::delete_absent)
                            .wrap(teacher_auth.clone()),
                    )
                    .service(
                        web::scope("/absents")
                            .service(api::absents::get_absents_member)
                            .wrap(teacher_auth.clone()),
                    ),
            )
    })
    .bind((std::net::Ipv4Addr::UNSPECIFIED, Config::get_port()))?
    .run()
    .await
}
