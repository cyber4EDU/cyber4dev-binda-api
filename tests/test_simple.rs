mod common;
use common::*;

#[test]
#[ignore]
fn basic() {
    expect_running_service();

    let version = get("/version", None);
    assert_eq!(200, version.status());
    assert_eq!(env!("CARGO_PKG_VERSION"), version.into_string().unwrap());

    get_kc_admin_token();
    get_kc_teacher_token("testteacher01");
    get_kc_teacher_token("testteacher02");
    get_kc_teacher_token("testteacher03");
    get_kc_teacher_token("testteacher04");
    get_kc_teacher_token("testteacher05");
}

#[test]
#[ignore]
fn basic_fail() {
    expect_running_service();

    assert_eq!(404, get("/foobar", None).status());
    assert_eq!(401, get("/api/v1/me", None).status());
}
