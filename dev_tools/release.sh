#!/bin/bash
set -e

failure() {
    echo "...failed"
}
trap failure ERR

LEVEL=$1

sqlfluff format
cargo fmt --check
#cargo audit
cargo clippy

echo "check for fixup commits"
if git log --oneline | grep "fixup!"; then
    failure
    exit 1
fi

echo "gererate api docs"
cargo run --bin generate_docs

echo "check for unstaged changes"
if [ -z "$(git status --porcelain)" ]; then 
    echo "ok"
else 
    failure
    exit 1
fi

echo "set new version"
cargo set-version --bump ${LEVEL}

VERSION=$(head Cargo.toml -n 5 | grep version | sed 's/version = //g' | sed 's/"//g')
echo "new version is: ${VERSION}"

echo "update version tag in docs"
cargo run --bin generate_docs


echo "commit changes and generate tag"
git add Cargo.lock Cargo.toml docs/openapi_v1.yaml
git commit -m "release version ${VERSION} 🎉"
git tag -a "v${VERSION}" -m "version ${VERSION}"
