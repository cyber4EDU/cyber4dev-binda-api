use std::env;
use std::io::{Error, ErrorKind::NotFound};
use tracing::warn;

pub struct Config;

impl Config {
    pub fn cors_permissive() -> bool {
        if let Ok(val) = env::var("MODE") {
            val == "dev" || val == "local"
        } else {
            false
        }
    }

    pub fn get_oidc_pub_key_url() -> String {
        match env::var("OIDC_PUB_KEY_URL") {
            Ok(url) => url.to_string(),
            Err(_) => {
                warn!("OIDC_PUB_KEY_URL not set");
                match env::var("KEYCLOAK_PUB_KEY_URL") {
                    Ok(url) => {
                        warn!("use fallback KEYCLOAK_PUB_KEY_URL={}", url);
                        url.to_string()
                    }
                    Err(_) => {
                        let url = "http://kc:8080/realms/esbz/protocol/openid-connect/certs";
                        warn!("use default path instead {}", url);
                        url.to_string()
                    }
                }
            }
        }
    }

    pub fn get_teacher_role() -> &'static str {
        "role_binda_teacher"
    }

    pub fn get_admin_role() -> &'static str {
        "role_binda_admin"
    }

    pub fn get_port() -> u16 {
        match env::var("PORT") {
            Ok(val) => val.parse().unwrap_or(8000),
            Err(_) => 8000,
        }
    }

    pub fn get_db_connection_options() -> Result<sea_orm::ConnectOptions, Error> {
        let mut db_options = sea_orm::ConnectOptions::new(
            env::var("DATABASE_URL").map_err(|_| Error::new(NotFound, "DATABASE_URL not found"))?,
        );
        db_options.sqlx_logging(env::var("SQLX_LOGGING").is_ok());
        Ok(db_options)
    }
}
