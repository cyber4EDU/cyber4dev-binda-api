use super::responses::JsonOk;
use crate::data::{History, MemberId, TimeFrame};
use crate::error::Error;
use actix_web::{get, web};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use sea_orm::DatabaseConnection;

/// Get history information for member
///
/// The history of a member for a given timeframe consists of all happenings
/// that are _within_ the timeframe, attached to the happenings are all
/// absent-notes _that overlap_ with the happening and _all_ check-ins/outs
/// that were applied to the happening (independent of the timeframe).
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = History),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = MemberId, Path, description = "Id of member"),
        ("not_before" = DateTime<Utc>, Query, description = "Filter not before"),
        ("not_after" = DateTime<Utc>, Query, description = "Filter not after")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/{id}/history")]
pub async fn get_info_history_member(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<MemberId>,
    timeframe: web::Query<TimeFrame>,
) -> Result<JsonOk<History>, Error> {
    Ok(JsonOk::from(
        History::load(&conn, &id.into_inner(), &timeframe.into_inner()).await?,
    ))
}
