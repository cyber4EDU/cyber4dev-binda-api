use std::collections::HashMap;

use super::{
    Absent, AbsentInfo, Check, HappeningId, HappeningPresenceInfo, HappeningShortInfo, History,
    MemberId, TimeFrame,
};
use crate::{error::Error, model::sea_orm_active_enums::CheckType};
use sea_orm::DatabaseConnection;

impl History {
    pub async fn load(
        conn: &DatabaseConnection,
        member_id: &MemberId,
        timeframe: &TimeFrame,
    ) -> Result<History, Error> {
        // load all happenings for member within given timeframe
        let mut happening_presence_infos =
            HappeningShortInfo::load_by_participant_and_timeframe(conn, member_id, timeframe)
                .await?
                .into_iter()
                .filter_map(|hsi| {
                    if let Some(props) = hsi.props {
                        Some((
                            hsi.id,
                            HappeningPresenceInfo {
                                id: hsi.id,
                                name: hsi.name,
                                props,
                                absent_entries: vec![],
                                checked_ins: vec![],
                                checked_outs: vec![],
                            },
                        ))
                    } else {
                        None
                    }
                })
                .collect::<HashMap<HappeningId, HappeningPresenceInfo>>();

        let happening_ids: Vec<HappeningId> = happening_presence_infos.keys().copied().collect();

        // load check-in/out events
        Check::load(conn, member_id, &happening_ids)
            .await?
            .iter()
            .for_each(|check| {
                if let Some(hpi) = happening_presence_infos.get_mut(&check.happening_id) {
                    match check.check_type {
                        CheckType::In => hpi.checked_ins.push(check.date),
                        CheckType::Out | CheckType::Close => hpi.checked_outs.push(check.date),
                    }
                }
            });

        // load relevant absent entries
        Absent::load_by_member_and_timeframe(conn, member_id, timeframe)
            .await?
            .into_iter()
            .for_each(|absent| {
                happening_presence_infos.iter_mut().for_each(|(_, hpi)| {
                    if hpi.props.from <= absent.until_date && absent.from_date <= hpi.props.to {
                        hpi.absent_entries.push(AbsentInfo {
                            id: absent.id,
                            reason: absent.reason.clone(),
                            from_date: absent.from_date,
                            until_date: absent.until_date,
                        });
                    }
                });
            });

        Ok(History {
            member_id: *member_id,
            happening_presences: happening_presence_infos.into_values().collect(),
        })
    }
}
