#![allow(deprecated)]
use super::{responses::JsonOk, Sorting};
use crate::api::SortOrder;
use crate::auth::get_sub_from_token_unverified;
use crate::data::{
    dashboard::{Dashboard, DashboardOld, NumberRequest, NumberResponse},
    helpers::load_all_happening_responsibles,
    Happening, HappeningId, HappeningShortInfo, HappeningShortInfoSortKeys, MemberIdVariant,
    MemberInfo, TimeData, TimeFrame,
};
use crate::error::Error;
use actix_web::web;
use actix_web::{get, post};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use chrono::Utc;
use futures::future::try_join_all;
use sea_orm::DatabaseConnection;
use tracing::info;

#[utoipa::path(
    context_path = "/me",
    tag = "me",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("")]
async fn get_me(
    auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
) -> Result<JsonOk<MemberInfo>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load(
            &conn,
            MemberIdVariant::Iid(&get_sub_from_token_unverified(auth)?),
            None,
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/me",
    tag = "me",
    responses(
        (status = 200, description = "Info returned", body = Vec<HappeningShortInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<HappeningShortInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("not_before" = DateTime<Utc>, Query, description = "Filter not before"),
        ("not_after" = DateTime<Utc>, Query, description = "Filter not after")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/happenings")]
async fn get_me_happenings(
    auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timeframe: web::Query<TimeFrame>,
    sorting: web::Query<Sorting<HappeningShortInfoSortKeys>>,
) -> Result<JsonOk<Vec<HappeningShortInfo>>, Error> {
    info!("get-me-happenings timeframe: {timeframe:?}");
    let me = MemberInfo::load(
        &conn,
        MemberIdVariant::Iid(&get_sub_from_token_unverified(auth)?),
        None,
    )
    .await?;

    Ok(JsonOk::from(
        HappeningShortInfo::load_by_responsible_and_timeframe(
            &conn,
            me.id,
            timeframe.into_inner(),
            sorting.into_inner().into(),
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/me",
    tag = "me",
    responses(
        (status = 200, description = "Info returned", body = Dashboard),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = HappeningId, Path, description = "Id of happening"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/dashboard/{id}")]
#[deprecated]
async fn get_me_dashboard(
    auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    id: web::Path<HappeningId>,
) -> Result<JsonOk<DashboardOld>, Error> {
    info!("get-me-dashboard timedata: {timedata:?}");
    let me = MemberInfo::load(
        &conn,
        MemberIdVariant::Iid(&get_sub_from_token_unverified(auth)?),
        None,
    )
    .await?;

    // load happening for associated groups
    let happening_id = id.into_inner();
    let happening = Happening::load(&conn, happening_id).await?;
    let happening_responsibles = load_all_happening_responsibles(&conn, &happening_id).await?;
    info!("happening_responsibles: {:?}", happening_responsibles);
    info!("my id: {:?}", &me.id);

    if !happening_responsibles.contains(&me.id) {
        return Err(Error::Forbidden("Not your happening"));
    }

    let related_happenings = if let Some(groups) = &happening.groups {
        // load via groups related happenings
        let now = timedata.into_inner().now.unwrap_or(Utc::now());
        let related_happenings_info =
            HappeningShortInfo::load_by_group_and_active_time(&conn, groups, now).await?;
        // load data for related happenings
        // FIXME: combine with above request
        try_join_all(
            related_happenings_info
                .iter()
                .map(|h| Happening::load(&conn, h.id)),
        )
        .await?
        .into_iter()
        // remove own happening
        .filter(|h| h.id != happening.id)
        .collect()
    } else {
        vec![]
    };
    info!("related_happenings: {}", related_happenings.len());

    Ok(JsonOk::from(
        DashboardOld::load(&conn, happening, related_happenings).await?,
    ))
}

#[utoipa::path(
    context_path = "/me",
    tag = "me",
    responses(
        (status = 200, description = "Info returned", body = NumberResponse),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    request_body = NumberRequest,
    security(
        ("api_access_token" = [])
    )
)]
#[post("/dashboard/numbers")]
#[deprecated]
async fn post_me_dashboard_numbers(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    body: web::Json<NumberRequest>,
) -> Result<JsonOk<NumberResponse>, Error> {
    let now = timedata.into_inner().now.unwrap_or(Utc::now());
    Ok(JsonOk::from(
        NumberResponse::load_from(&conn, now, &body.into_inner()).await?,
    ))
}
