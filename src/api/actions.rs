#![allow(deprecated)]
use super::responses::JsonOk;
use crate::data::{
    Action, Delay, HappeningId, MemberId, MemberIdVariant, MemberIdentityId, MemberInfo,
    MemberPhysicalId, TimeData,
};
use crate::error::Error;
use actix_web::web;
use actix_web::{post, HttpResponse, Result};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use chrono::Utc;
use sea_orm::DatabaseConnection;

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 200, description = "Checked in", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("member_id" = MemberId, Path, description = "Id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-in"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/check_in/{member_id}")]
async fn post_actions_happening_check_in(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    let (happening_id, member_id) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    Ok(JsonOk::from(
        Action::try_check_in_with_id_variant(
            &conn,
            happening_id,
            MemberIdVariant::Id(&member_id),
            delay.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 200, description = "Checked in", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("member_iid" = MemberIdentityId, Path, description = "Identity id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-in"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/check_in/iid/{member_iid}")]
async fn post_actions_happening_check_in_iid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberIdentityId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    let (happening_id, iid) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-in
    Ok(JsonOk::from(
        Action::try_check_in_with_id_variant(
            &conn,
            happening_id,
            MemberIdVariant::Iid(&iid),
            delay.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 200, description = "Checked in", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("member_pid" = MemberPhysicalId, Path, description = "Physical id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-in"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/check_in/pid/{member_pid}")]
async fn post_actions_happening_check_in_pid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberPhysicalId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    let (happening_id, pid) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-in
    Ok(JsonOk::from(
        Action::try_check_in_with_id_variant(
            &conn,
            happening_id,
            MemberIdVariant::Pid(&pid),
            delay.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 200, description = "Checked in", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("keycloak_id" = MemberIdentityId, Path, description = "Keycloak id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-in"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[deprecated(note = "use check_in_iid instead")]
#[post("/happening/{happening_id}/check_in/kc/{keycloak_id}")]
async fn post_actions_happening_check_in_kc(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberIdentityId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    let (happening_id, iid) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-in
    Ok(JsonOk::from(
        Action::try_check_in_with_id_variant(
            &conn,
            happening_id,
            MemberIdVariant::Iid(&iid),
            delay.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 201, description = "Checked out"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("member_id" = MemberId, Path, description = "Id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/check_out/{member_id}")]
async fn post_actions_happening_check_out(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    let (happening_id, member_id) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-out
    Action::check_out_with_id_variant(
        &conn,
        &happening_id,
        MemberIdVariant::Id(&member_id),
        &delay.into_inner(),
        &timedata.into_inner().now.unwrap_or(Utc::now()),
    )
    .await?;
    Ok(HttpResponse::Ok().finish())
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 201, description = "Checked out"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("iid" = MemberIdentityId, Path, description = "Identity id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/check_out/iid/{iid}")]
async fn post_actions_happening_check_out_iid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberIdentityId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    let (happening_id, iid) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-out
    Action::check_out_with_id_variant(
        &conn,
        &happening_id,
        MemberIdVariant::Iid(&iid),
        &delay.into_inner(),
        &timedata.into_inner().now.unwrap_or(Utc::now()),
    )
    .await?;
    Ok(HttpResponse::Ok().finish())
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 201, description = "Checked out"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("pid" = MemberPhysicalId, Path, description = "Physical id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/check_out/pid/{pid}")]
async fn post_actions_happening_check_out_pid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberPhysicalId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    let (happening_id, pid) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-out
    Action::check_out_with_id_variant(
        &conn,
        &happening_id,
        MemberIdVariant::Pid(&pid),
        &delay.into_inner(),
        &timedata.into_inner().now.unwrap_or(Utc::now()),
    )
    .await?;
    Ok(HttpResponse::Ok().finish())
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 201, description = "Checked out"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("keycloak_id" = MemberIdentityId, Path, description = "Keycloak id of member"),
        ("minutes" = Option<i16>, Query, description = "Delay of check-out"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[deprecated(note = "use check_out_iid instead")]
#[post("/happening/{happening_id}/check_out/kc/{keycloak_id}")]
async fn post_actions_happening_check_out_kc(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    ids: web::Path<(HappeningId, MemberIdentityId)>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    let (happening_id, iid) = ids.into_inner();
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-out
    Action::check_out_with_id_variant(
        &conn,
        &happening_id,
        MemberIdVariant::Iid(&iid),
        &delay.into_inner(),
        &timedata.into_inner().now.unwrap_or(Utc::now()),
    )
    .await?;
    Ok(HttpResponse::Ok().finish())
}

#[utoipa::path(
    context_path = "/actions",
    tag = "actions",
    responses(
        (status = 201, description = "Closed"),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("happening_id" = HappeningId, Path, description = "Id of happening"),
        ("minutes" = Option<i16>, Query, description = "Delay of closing"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[post("/happening/{happening_id}/close")]
async fn post_actions_happening_close(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    happening_id: web::Path<HappeningId>,
    delay: web::Query<Delay>,
    timedata: web::Query<TimeData>,
) -> Result<HttpResponse, Error> {
    // TODO: check if me is responsible for this happening
    // TODO: check if already checked-out
    Action::close(
        &conn,
        happening_id.into_inner(),
        delay.into_inner(),
        timedata.into_inner().now.unwrap_or(Utc::now()),
    )
    .await?;
    Ok(HttpResponse::Ok().finish())
}
