use actix_web::{
    body::EitherBody, error::JsonPayloadError, web, HttpRequest, HttpResponse, Responder,
};
use serde::Serialize;

pub struct JsonCreated<T>(pub web::Json<T>);
pub struct JsonOk<T>(pub web::Json<T>);

impl<T> JsonOk<T> {
    pub fn from(data: T) -> Self {
        Self(web::Json(data))
    }
}

impl<T> JsonCreated<T> {
    pub fn from(data: T) -> Self {
        Self(web::Json(data))
    }
}

impl<T: Serialize> Responder for JsonOk<T> {
    type Body = EitherBody<String>;

    fn respond_to(self, req: &HttpRequest) -> HttpResponse<Self::Body> {
        self.0.respond_to(req)
    }
}

impl<T: Serialize> Responder for JsonCreated<T> {
    type Body = EitherBody<String>;

    fn respond_to(self, _: &HttpRequest) -> HttpResponse<Self::Body> {
        match serde_json::to_string(&self.0) {
            Ok(body) => match HttpResponse::Created()
                .content_type(mime::APPLICATION_JSON)
                .message_body(body)
            {
                Ok(res) => res.map_into_left_body(),
                Err(err) => HttpResponse::from_error(err).map_into_right_body(),
            },

            Err(err) => {
                HttpResponse::from_error(JsonPayloadError::Serialize(err)).map_into_right_body()
            }
        }
    }
}
