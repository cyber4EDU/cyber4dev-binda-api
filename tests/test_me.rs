mod common;
use common::*;

#[test]
#[ignore]
fn me() {
    expect_running_service();
    add_keycloak_members();

    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    assert_eq!(200, get("/api/v1/me", Some(&atk)).status());
    assert_eq!(200, get("/api/v1/me", Some(&ttk)).status());
}
