#![allow(deprecated)]
use super::{responses::JsonOk, Sorting};
use crate::api::SortOrder;
use crate::data::{
    dashboard::{Dashboard, NumberRequest, NumberResponse},
    GroupId, Happening, HappeningId, HappeningShortInfo, MemberId, MemberIdVariant,
    MemberIdentityId, MemberInfo, MemberInfoCheckedInSortKeys, MemberInfoSortKeys,
    MemberPhysicalId, Search, TimeData,
};
use crate::error::Error;
use actix_web::web;
use actix_web::{get, post};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use chrono::Utc;
use futures::future::try_join_all;
use sea_orm::DatabaseConnection;
use tracing::info;

/// Get status information of one member by id
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = MemberId, Path, description = "Id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/{id}")]
async fn get_info_member(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<MemberId>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info(
            &conn,
            MemberIdVariant::Id(&id.into_inner()),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

/// deprecated: Get information a specific member by keycloak_id
///
/// Use `/member/iid/{iid}` instead!
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("keycloak_id" = MemberIdentityId, Path, description = "Keycloak id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[deprecated(note = "use get_member_info_identity_id instead")]
#[get("/member/kc/{keycloak_id}")]
async fn get_info_member_kc(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    iid: web::Path<MemberIdentityId>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load(
            &conn,
            MemberIdVariant::Iid(&iid.into_inner()),
            Some(timedata.into_inner().now.unwrap_or(Utc::now())),
        )
        .await?,
    ))
}

/// Get status information of one member by identity id
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("iid" = MemberIdentityId, Path, description = "Identity id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/iid/{iid}")]
async fn get_info_member_iid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    iid: web::Path<MemberIdentityId>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info(
            &conn,
            MemberIdVariant::Iid(&iid.into_inner()),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

/// Get status information of one member by physical id
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = MemberInfo),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("pid" = MemberPhysicalId, Path, description = "Physical id of member"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/member/pid/{pid}")]
async fn get_info_member_pid(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    pid: web::Path<MemberPhysicalId>,
    timedata: web::Query<TimeData>,
) -> Result<JsonOk<MemberInfo>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info(
            &conn,
            MemberIdVariant::Pid(&pid.into_inner()),
            timedata.into_inner().now.unwrap_or(Utc::now()),
        )
        .await?,
    ))
}

/// Get status information of all members
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/members")]
async fn get_info_members(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_all(
            &conn,
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information of all members that are absent
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/members/absent")]
async fn get_info_members_absent(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_all_absent(
            &conn,
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information of all members that are missing
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/members/missing")]
async fn get_info_members_missing(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_all_missing(
            &conn,
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information about participants of a specific happening
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = HappeningId, Path, description = "Id of happening"),
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/happening/{id}/participants")]
async fn get_info_happening_participants(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<HappeningId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_happening(
            &conn,
            id.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information about participants of a specific happening that are checked_in
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = HappeningId, Path, description = "Id of happening"),
        ("sort_by" = Option<MemberInfoCheckedInSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/happening/{id}/participants/checked_in")]
async fn get_info_happening_participants_checked_in(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<HappeningId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoCheckedInSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_happening_checked_in(
            &conn,
            id.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information about participants of a specific group
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = GroupId, Path, description = "Id of group"),
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/group/{id}/participants")]
async fn get_info_group_participants(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<GroupId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_group(
            &conn,
            id.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information about participants of a specific group, that are also checked-in
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = GroupId, Path, description = "Id of group"),
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/group/{id}/participants/checked_in")]
async fn get_info_group_participants_checked_in(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<GroupId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_group_checked_in(
            &conn,
            id.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information about participants of a specific group, that are also absent
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = GroupId, Path, description = "Id of group"),
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/group/{id}/participants/absent")]
async fn get_info_group_participants_absent(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<GroupId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    let now = timedata.into_inner().now.unwrap_or(Utc::now());
    Ok(JsonOk::from(
        MemberInfo::load_info_group_absent(
            &conn,
            id.into_inner(),
            now,
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

/// Get status information about participants of a specific group, that are also missing
#[utoipa::path(
    context_path = "/info",
    tag = "info",
    responses(
        (status = 200, description = "Info returned", body = Vec<MemberInfo>),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 404, description = "Not found"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = GroupId, Path, description = "Id of group"),
        ("sort_by" = Option<MemberInfoSortKeys>, Query, description = "Sort key"),
        ("order" = Option<SortOrder>, Query, description = "Sort order"),
        ("search" = Option<String>, Query, description = "Search string"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/group/{id}/participants/missing")]
async fn get_info_group_participants_missing(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    id: web::Path<GroupId>,
    timedata: web::Query<TimeData>,
    sorting: web::Query<Sorting<MemberInfoSortKeys>>,
    search: web::Query<Search>,
) -> Result<JsonOk<Vec<MemberInfo>>, Error> {
    Ok(JsonOk::from(
        MemberInfo::load_info_group_missing(
            &conn,
            id.into_inner(),
            timedata.into_inner().now.unwrap_or(Utc::now()),
            sorting.into_inner().into(),
            search.into_inner().search,
        )
        .await?,
    ))
}

#[utoipa::path(
    context_path = "/info",
    tag = "info-dashboard",
    responses(
        (status = 200, description = "Info returned", body = Dashboard),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("id" = HappeningId, Path, description = "Id of happening"),
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    security(
        ("api_access_token" = [])
    )
)]
#[get("/dashboard/happening/{id}")]
async fn get_info_dashboard_happening(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    id: web::Path<HappeningId>,
) -> Result<JsonOk<Dashboard>, Error> {
    info!("get-me-dashboard timedata: {timedata:?}");

    // load happening for associated groups
    let happening = Happening::load(&conn, id.into_inner()).await?;
    info!("got happening");
    if let Some(gps) = &happening.groups {
        info!("... has groups: {}", gps.len());
    } else {
        info!("... has no groups");
    }

    let happenings = if let Some(groups) = &happening.groups {
        // load via groups related happenings
        let now = timedata.into_inner().now.unwrap_or(Utc::now());
        let related_happenings_info =
            HappeningShortInfo::load_by_group_and_active_time(&conn, groups, now).await?;
        // load data for related happenings
        // FIXME: combine with above request
        try_join_all(
            related_happenings_info
                .iter()
                .map(|h| Happening::load(&conn, h.id)),
        )
        .await?
        .into_iter()
        .collect()
    } else {
        vec![happening]
    };
    info!("number of related happenings: {}", happenings.len());

    Ok(JsonOk::from(Dashboard::load(&conn, happenings).await?))
}

#[utoipa::path(
    context_path = "/info",
    tag = "info-dashboard",
    responses(
        (status = 200, description = "Info returned", body = NumberResponse),
        (status = 400, description = "Bad request"),
        (status = 401, description = "Unauthorized"),
        (status = 403, description = "Forbidden"),
        (status = 500, description = "Internal server error"),
    ),
    params(
        ("now" = Option<DateTime<Utc>>, Query, description = "Current time")
    ),
    request_body = NumberRequest,
    security(
        ("api_access_token" = [])
    )
)]
#[post("/dashboard/numbers")]
async fn post_info_dashboard_numbers(
    _auth: BearerAuth,
    conn: web::Data<DatabaseConnection>,
    timedata: web::Query<TimeData>,
    body: web::Json<NumberRequest>,
) -> Result<JsonOk<NumberResponse>, Error> {
    let now = timedata.into_inner().now.unwrap_or(Utc::now());
    Ok(JsonOk::from(
        NumberResponse::load_from(&conn, now, &body.into_inner()).await?,
    ))
}
