DROP TABLE check_in_out;
DROP TABLE absents;
DROP TABLE groups_happenings;
DROP TABLE member_is_to_happening;
DROP TABLE member_is_to_group;
DROP TABLE member_is_to_member;
DROP TABLE notes;
DROP TABLE member_notes_list;
DROP TABLE contact;
DROP TABLE happenings;
DROP TABLE groups;
DROP TABLE members;
DROP TYPE absent_reason;
DROP TYPE member_notes_list_type;
DROP TYPE check_type;
DROP TYPE happening_role;
DROP TYPE group_role;
DROP TYPE member_role;
