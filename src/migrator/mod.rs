use sea_orm_migration::prelude::*;

mod m20220101_000001_create_table;
mod m20230709_203019_create_table;
mod m20230801_105132_identity_management;
mod m20230818_231100_physical_id_and_absent_creator;
mod m20230822_163500_absent_reasons;
mod m20250103_210609_contacts_json;
mod m20250110_203225_contact_json;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000001_create_table::Migration),
            Box::new(m20230709_203019_create_table::Migration),
            Box::new(m20230801_105132_identity_management::Migration),
            Box::new(m20230818_231100_physical_id_and_absent_creator::Migration),
            Box::new(m20230822_163500_absent_reasons::Migration),
            Box::new(m20250103_210609_contacts_json::Migration),
            Box::new(m20250110_203225_contact_json::Migration),
        ]
    }
}
