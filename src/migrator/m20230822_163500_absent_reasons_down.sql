ALTER TYPE absent_reason RENAME TO absent_reason_old;
CREATE TYPE absent_reason AS ENUM ('ill', 'holidays', 'released', 'late');
ALTER TABLE absents ALTER COLUMN reason TYPE absent_reason USING 'released';
DROP TYPE absent_reason_old;
