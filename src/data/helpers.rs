use super::{HappeningId, MemberId};
use crate::error::Error;
use sea_orm::{DatabaseConnection, DbBackend, FromQueryResult, Statement};

#[derive(FromQueryResult)]
struct HappeningResponsible {
    source_member_id: MemberId,
}

pub async fn load_all_happening_responsibles(
    conn: &DatabaseConnection,
    happening_id: &HappeningId,
) -> Result<Vec<MemberId>, Error> {
    Ok(
        HappeningResponsible::find_by_statement(Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                SELECT
                    member_is_to.source_member_id
                FROM
                    -- combined table of member_is_to_happening and member_is_to_group-groups_happenings
                    (
                        SELECT
                            member_is_to_happening.source_member_id,
                            member_is_to_happening.target_happening_id,
                            member_is_to_happening.role::TEXT
                        FROM
                            member_is_to_happening
                        UNION ALL
                        SELECT
                            member_is_to_group.source_member_id,
                            groups_happenings.happening_id,
                            member_is_to_group.role::TEXT
                        FROM
                            member_is_to_group
                        INNER JOIN
                            groups_happenings
                        ON
                            member_is_to_group.target_group_id = groups_happenings.group_id
                    ) AS member_is_to
                WHERE
                    member_is_to.target_happening_id = $1
                    AND
                    member_is_to.role = 'responsible'

            "#,
            [
                happening_id.0.into(),
            ],
            ))
            .all(conn)
            .await?
            .into_iter()
            .map(|hr| hr.source_member_id)
            .collect()
        )
}
