use crate::api::SortOrder;
use crate::data::{HappeningShortInfoSortKeys, MemberInfoCheckedInSortKeys, MemberInfoSortKeys};
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Sorting<T> {
    pub sort_by: T,
    pub order: SortOrder,
}

impl From<SortOrder> for sea_orm::query::Order {
    fn from(val: SortOrder) -> sea_orm::query::Order {
        match val {
            SortOrder::Ascending => sea_orm::query::Order::Asc,
            SortOrder::Descending => sea_orm::query::Order::Desc,
        }
    }
}

pub enum MemberInfoSortColumn {
    Members(crate::model::members::Column),
}

pub enum MemberInfoCheckedInSortColumns {
    Members(crate::model::members::Column),
    CheckInOut(crate::model::check_in_out::Column),
}

#[derive(Debug)]
pub enum HappeningShortInfoSortColumns {
    Happenings(crate::model::happenings::Column),
    HappeningProps(&'static str),
}

impl<T, S> From<crate::api::Sorting<T>> for Sorting<S>
where
    T: Default + utoipa::ToSchema,
    S: From<T>,
{
    fn from(sorting: crate::api::Sorting<T>) -> Sorting<S> {
        Self {
            sort_by: sorting.sort_by.unwrap_or_default().into(),
            order: sorting.order.unwrap_or_default(),
        }
    }
}

impl From<MemberInfoSortKeys> for MemberInfoSortColumn {
    fn from(val: MemberInfoSortKeys) -> MemberInfoSortColumn {
        match val {
            MemberInfoSortKeys::Id => Self::Members(crate::model::members::Column::Id),
            MemberInfoSortKeys::FirstName => {
                Self::Members(crate::model::members::Column::FirstName)
            }
            MemberInfoSortKeys::LastName => Self::Members(crate::model::members::Column::LastName),
            MemberInfoSortKeys::MiddleNames => {
                Self::Members(crate::model::members::Column::MiddleNames)
            }
            MemberInfoSortKeys::PersonQualifier => {
                Self::Members(crate::model::members::Column::PersonQualifier)
            }
        }
    }
}

impl From<MemberInfoCheckedInSortKeys> for MemberInfoCheckedInSortColumns {
    fn from(val: MemberInfoCheckedInSortKeys) -> MemberInfoCheckedInSortColumns {
        match val {
            MemberInfoCheckedInSortKeys::Id => Self::Members(crate::model::members::Column::Id),
            MemberInfoCheckedInSortKeys::FirstName => {
                Self::Members(crate::model::members::Column::FirstName)
            }
            MemberInfoCheckedInSortKeys::LastName => {
                Self::Members(crate::model::members::Column::LastName)
            }
            MemberInfoCheckedInSortKeys::CheckedIn => {
                Self::CheckInOut(crate::model::check_in_out::Column::Date)
            }
        }
    }
}

impl From<HappeningShortInfoSortKeys> for HappeningShortInfoSortColumns {
    fn from(val: HappeningShortInfoSortKeys) -> HappeningShortInfoSortColumns {
        match val {
            HappeningShortInfoSortKeys::Id => {
                Self::Happenings(crate::model::happenings::Column::Id)
            }
            HappeningShortInfoSortKeys::Name => {
                Self::Happenings(crate::model::happenings::Column::Name)
            }
            HappeningShortInfoSortKeys::From => Self::HappeningProps("from"),
            HappeningShortInfoSortKeys::To => Self::HappeningProps("to"),
        }
    }
}
