-- drop "contact" table and add simple jsonb column to member
DROP TABLE "contact";
ALTER TABLE "members" ADD COLUMN "contacts" JSONB;
