mod common;
use common::*;

#[test]
#[ignore]
fn create_get_update_group() {
    expect_running_service();
    let atk = get_kc_admin_token();

    // create some members
    let responsibles = add_members(&atk, 2, "create_get_update_group responsibles");
    let participants = add_members(&atk, 3, "create_get_update_group participants");

    // create group
    let rsp = post(
        "/api/v1/admin/group",
        Some(&atk),
        NewGroup {
            name: "Group A".into(),
            responsibles: Some(responsibles.into_iter().map(|rsp| rsp.id).collect()),
            participants: Some(participants.into_iter().map(|rsp| rsp.id).collect()),
            happenings: None,
        },
    );
    assert_eq!(201, rsp.status());
    let mut group = rsp.into_json::<Group>().unwrap();

    // get group
    let rsp = get(&format!("/api/v1/admin/group/{}", group.id), Some(&atk));
    assert_eq!(200, rsp.status());
    // NOTE: can we be sure that the order of e.g. responsibles is always the same here?
    assert_eq!(group, rsp.into_json::<Group>().unwrap());

    // update group name
    group.name = "Group B".to_string();
    let rsp = put("/api/v1/admin/group", Some(&atk), group.clone());
    assert_eq!(200, rsp.status());
    assert_eq!(group, rsp.into_json::<Group>().unwrap());

    // get member
    let rsp = get(&format!("/api/v1/admin/group/{}", group.id), Some(&atk));
    assert_eq!(200, rsp.status());
    assert_eq!(group, rsp.into_json::<Group>().unwrap());

    // TODO: delete
}
