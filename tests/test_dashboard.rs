mod common;
use common::*;

#[test]
#[ignore]
fn dashboard() {
    expect_running_service();
    add_keycloak_members();

    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");
    let _utk = get_kc_user_token();

    let rsp = post(
        "/api/v1/admin/group",
        Some(&atk),
        NewGroup {
            name: "Group A".into(),
            responsibles: None,
            participants: None,
            happenings: None,
        },
    );
    assert_eq!(201, rsp.status());
    let group = rsp.into_json::<Group>().unwrap();

    let rsp = post(
        "/api/v1/admin/happening",
        Some(&atk),
        NewHappening {
            name: "Sport".into(),
            props: serde_json::from_str(
                r#"{
                    "from": "2024-01-01T08:00:00Z",
                    "to": "2024-01-01T10:00:00Z",
                    "foo": "bar",
                    "bar": "foo"
                }"#,
            )
            .unwrap(),
            responsibles: None,
            participants: None,
            groups: Some(vec![group.id]),
        },
    );
    assert_eq!(201, rsp.status());
    let happening1 = rsp.into_json::<Happening>().unwrap();
    let rsp = post(
        "/api/v1/admin/happening",
        Some(&atk),
        NewHappening {
            name: "Erdkunde".into(),
            props: serde_json::from_str(
                r#"{
                    "from": "2024-01-01T09:00:00Z",
                    "to": "2024-01-01T10:00:00Z"
                }"#,
            )
            .unwrap(),
            responsibles: None,
            participants: None,
            groups: Some(vec![group.id]),
        },
    );
    assert_eq!(201, rsp.status());
    let happening2 = rsp.into_json::<Happening>().unwrap();

    // add happenings to teacher
    let rsp = get("/api/v1/me", Some(&ttk));
    assert_eq!(200, rsp.status());
    let me_id = rsp.into_json::<MemberShortInfo>().unwrap().id;
    let rsp = get(&format!("/api/v1/admin/member/{}", me_id), Some(&atk));
    assert_eq!(200, rsp.status());
    let mut me_data = rsp.into_json::<Member>().unwrap();
    match me_data.responsible_for {
        Some(ref mut rsp_for) => {
            if let Some(ref mut hps) = rsp_for.happenings {
                hps.append(&mut vec![happening1.id, happening2.id])
            } else {
                rsp_for.happenings = Some(vec![happening1.id, happening2.id]);
            }
        }
        None => {
            me_data.responsible_for = Some(ResponsibleFor {
                members: None,
                groups: None,
                happenings: Some(vec![happening1.id, happening2.id]),
            });
        }
    }
    let rsp = put("/api/v1/admin/member", Some(&atk), me_data.clone());
    assert_eq!(200, rsp.status());
    let rsp = get(&format!("/api/v1/admin/member/{}", me_id), Some(&atk));
    assert_eq!(200, rsp.status());
    //assert_eq!(
    //    me_data,
    //    rsp.into_json::<Member>().unwrap(),
    //    "TODO: compare vectors as HashSet! So this might fail currently :/"
    //);

    let rsp = get("/api/v1/me", Some(&ttk));
    println!("{:?}", rsp);
    assert_eq!(200, rsp.status());

    // with time-frame not containing first
    let rsp = get(
        &format!(
            "/api/v1/me/happenings?not_before={}&not_after={}",
            urlencoding::encode("2024-01-01T08:00:01Z"),
            urlencoding::encode("2024-01-01T10:00:00Z"),
        ),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    let data = rsp.into_json::<Vec<HappeningShortInfo>>().unwrap();
    println!("{:?}", data);
    assert_eq!(1, data.len());

    // with time-frame containing nothing
    let rsp = get(
        &format!(
            "/api/v1/me/happenings?not_before={}&not_after={}",
            urlencoding::encode("2024-01-01T10:00:00Z"),
            urlencoding::encode("2024-01-01T11:00:00Z"),
        ),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    let data = rsp.into_json::<Vec<HappeningShortInfo>>().unwrap();
    assert_eq!(0, data.len());

    // with time-frame containing both
    let rsp = get(
        &format!(
            "/api/v1/me/happenings?not_before={}&not_after={}",
            urlencoding::encode("2024-01-01T08:00:00Z"),
            urlencoding::encode("2024-01-01T10:00:00Z"),
        ),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    let data = rsp.into_json::<Vec<HappeningShortInfo>>().unwrap();
    println!("{:?}", data);
    assert_eq!(2, data.len());

    // load dashboard
    let rsp = get(
        &format!("/api/v1/me/dashboard/{}", happening1.id),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    let data = rsp.into_json::<Dashboard>().unwrap();
    //let data = rsp.into_string().unwrap();
    println!("{:?}", data);
}
