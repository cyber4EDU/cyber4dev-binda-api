use actix_web::{http::StatusCode, ResponseError};

#[derive(Debug)]
pub enum Error {
    Plain(&'static str),
    Db(sea_orm::error::DbErr),
    NotFound(&'static str),
    BadRequest(&'static str),
    Forbidden(&'static str),
    Internal(&'static str),
}

impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Plain(err) => write!(fmt, "{err}"),
            Self::Db(err) => write!(fmt, "{err}"),
            Self::NotFound(err) => write!(fmt, "{err}"),
            Self::BadRequest(err) => write!(fmt, "{err}"),
            Self::Forbidden(err) => write!(fmt, "{err}"),
            Self::Internal(err) => write!(fmt, "{err}"),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match *self {
            Self::Plain(_) => None,
            Self::Db(ref err) => Some(err),
            Self::NotFound(_) => None,
            Self::BadRequest(_) => None,
            Self::Forbidden(_) => None,
            Self::Internal(_) => None,
        }
    }
}

macro_rules! impl_from_for_error {
    ($source:ty, $target:ident) => {
        impl From<$source> for Error {
            fn from(err: $source) -> Error {
                Self::$target(err)
            }
        }
    };
}
impl_from_for_error!(sea_orm::error::DbErr, Db);
impl_from_for_error!(&'static str, Plain);

impl ResponseError for Error {
    fn status_code(&self) -> StatusCode {
        match self {
            Self::Plain(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Self::Db(err) => match err {
                sea_orm::error::DbErr::ConnectionAcquire(_) | sea_orm::error::DbErr::Conn(_) => {
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                sea_orm::error::DbErr::RecordNotFound(_) => StatusCode::NOT_FOUND,
                _ => StatusCode::BAD_REQUEST,
            },
            Self::NotFound(_) => StatusCode::NOT_FOUND,
            Self::BadRequest(_) => StatusCode::BAD_REQUEST,
            Self::Forbidden(_) => StatusCode::FORBIDDEN,
            Self::Internal(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
