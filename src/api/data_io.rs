#![macro_use]

macro_rules! data_io {
    ($name:expr, $target_name:ident, $new_target_name:ident, $target_param_type:ident, $post_put_path:expr, $get_delete_path:expr) => {
        paste::paste! {
            /// Create new entry
            ///
            /// Create new entry of type
            #[doc = $name]
            #[utoipa::path(
                context_path = "/admin",
                tag = "admin",
                request_body = $new_target_name,
                responses(
                    (status = 201, description = "Created", body = $target_name),
                    (status = 400, description = "Bad request"),
                    (status = 401, description = "Unauthorized"),
                    (status = 409, description = "Conflict"),
                    (status = 500, description = "Internal server error"),
                ),
                security(
                    ("api_access_token" = [])
                )
            )]
            #[post($post_put_path)]
            async fn [<post_admin_ $target_name:lower>](
                auth: actix_web_httpauth::extractors::bearer::BearerAuth,
                conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
                data: actix_web::web::Json<$new_target_name>,
            ) -> Result<super::responses::JsonCreated<$target_name>, crate::error::Error> {
                Ok(super::responses::JsonCreated::from(data.into_inner().insert(&conn, MemberIdVariant::Iid(&crate::auth::get_sub_from_token_unverified(auth)?)).await?))
            }

            /// Get entry
            ///
            /// Get entry of type
            #[doc = $name]
            #[utoipa::path(
                context_path = "/admin",
                tag = "admin",
                responses(
                    (status = 200, description = "Info returned", body = $target_name),
                    (status = 400, description = "Bad request"),
                    (status = 401, description = "Unauthorized"),
                    (status = 403, description = "Forbidden"),
                    (status = 404, description = "Not found"),
                    (status = 500, description = "Internal server error"),
                ),
                params(
                    ("id" = $target_param_type, Path, description = "Object id")
                ),
                security(
                    ("api_access_token" = [])
                )
            )]
            #[get($get_delete_path)]
            async fn [<get_admin_ $target_name:lower>](
                _auth: actix_web_httpauth::extractors::bearer::BearerAuth,
                conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
                id: actix_web::web::Path<$target_param_type>,
            ) -> Result<super::responses::JsonOk<$target_name>, crate::error::Error> {
                Ok(super::responses::JsonOk::from($target_name::load(&conn, id.into_inner().into()).await?))
            }

            /// Modify entry
            ///
            /// Modify entry of type
            #[doc = $name]
            #[utoipa::path(
                context_path = "/admin",
                tag = "admin",
                request_body = $target_name,
                responses(
                    (status = 200, description = "Modified", body = $target_name),
                    (status = 400, description = "Bad request"),
                    (status = 401, description = "Unauthorized"),
                    (status = 404, description = "Not found"),
                    (status = 500, description = "Internal server error"),
                ),
                security(
                    ("api_access_token" = [])
                )
            )]
            #[put($post_put_path)]
            async fn [<put_admin_ $target_name:lower>](
                _auth: actix_web_httpauth::extractors::bearer::BearerAuth,
                conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
                data: actix_web::web::Json<$target_name>,
            ) -> Result<super::responses::JsonOk<$target_name>, crate::error::Error> {
                Ok(super::responses::JsonOk::from(data.into_inner().update(&conn).await?))
            }

            /// Delete entry
            ///
            /// Delete entry of type
            #[doc = $name]
            #[utoipa::path(
                context_path = "/admin",
                tag = "admin",
                responses(
                    (status = 204, description = "Object deleted"),
                    (status = 400, description = "Bad request"),
                    (status = 401, description = "Unauthorized"),
                    (status = 403, description = "Forbidden"),
                    (status = 404, description = "Not found"),
                    (status = 500, description = "Internal server error"),
                ),
                params(
                    ("id" = $target_param_type, Path, description = "Object id")
                ),
                security(
                    ("api_access_token" = [])
                )
            )]
            #[delete($get_delete_path)]
            async fn [<delete_admin_ $target_name:lower>](
                _auth: actix_web_httpauth::extractors::bearer::BearerAuth,
                conn: actix_web::web::Data<sea_orm::DatabaseConnection>,
                id: actix_web::web::Path<$target_param_type>,
            ) -> Result<actix_web::HttpResponse, crate::error::Error> {
                $target_name::delete(&conn, id.into_inner().into()).await?;
                Ok(actix_web::HttpResponse::NoContent().finish())
            }
        }
    };
}
