# BinDa API

[Static API documentation](./docs/openapi_v1.yaml)

## environment

- `DATABASE_URL` postgres connection string of the form `postgres://<user>:<password>@<host>/<database>`
- `KEYCLOAK_PUB_KEY_URL` - defaults to `http://kc:8080/realms/esbz/protocol/openid-connect/certs`
- `PORT` - defaults to `8000`
- `MODE` - if set to `dev` or `local` permissive CORS settings will be applied

## dev notes

### build

`cargo build`

or with docker

`docker build -t binda_api .`

### run test version

A running keycloak instance is expected (configuration see within the `run_tests.sh` script)

`pg_virtualenv test/run_tests.sh`

Or if a postgres is available at `postgres://bindaapi:bindaapi@localhost:5431/bindaapi`

`./test/run_tests.sh`

Run tests with verbose output

`BINDA_API_TEST_VERBOSE= pg_virtualenv test/run_tests.sh`

Run tests and keep API alive (e.g. to click the swagger)

`BINDA_API_TEST_CONTINUE= pg_virtualenv test/run_tests.sh`

Run a single test

`pg_virtualenv test/run_tests.sh <test_name>`

### change database

generate model from existing database:
```shell
sea-orm-cli generate entity -o src/model
```

### create a new release

Use `dev_tools/release.sh <major|minor|patch>` to create a new release.

### change API

- checklist
    - update changelog
    - update api docs
    - update generated openapi file
