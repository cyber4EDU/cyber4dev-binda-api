mod common;
use common::*;

#[test]
#[ignore]
fn create_get_update_happening() {
    expect_running_service();
    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    // create some members
    let responsibles = add_members(&atk, 2, "create_get_update_happening responsibles");
    let participants = add_members(&atk, 3, "create_get_update_happening participants");

    // create group
    let rsp = post(
        "/api/v1/admin/group",
        Some(&atk),
        NewGroup {
            name: "create_get_update_happening group 0".into(),
            responsibles: Some(responsibles.clone().into_iter().map(|rsp| rsp.id).collect()),
            participants: Some(participants.clone().into_iter().map(|rsp| rsp.id).collect()),
            happenings: None,
        },
    );
    assert_eq!(201, rsp.status());
    let group = rsp.into_json::<Group>().unwrap();

    // create happening
    let rsp = post(
        "/api/v1/admin/happening",
        Some(&atk),
        NewHappening {
            name: "Happening A".into(),
            props: None,
            responsibles: Some(responsibles.into_iter().map(|rsp| rsp.id).collect()),
            participants: Some(participants.into_iter().map(|rsp| rsp.id).collect()),
            groups: Some(vec![group.id]),
        },
    );
    assert_eq!(201, rsp.status());
    let mut happening = rsp.into_json::<Happening>().unwrap();

    // get happening
    let rsp = get(
        &format!("/api/v1/admin/happening/{}", happening.id),
        Some(&atk),
    );
    assert_eq!(200, rsp.status());
    // NOTE: can we be sure that the order of e.g. responsibles is always the same here?
    assert_eq!(happening, rsp.into_json::<Happening>().unwrap());

    // update happening name
    happening.name = "create_get_update_happening happening 0".to_string();
    let rsp = put("/api/v1/admin/happening", Some(&atk), happening.clone());
    assert_eq!(200, rsp.status());
    assert_eq!(happening, rsp.into_json::<Happening>().unwrap());

    // get member
    let rsp = get(
        &format!("/api/v1/admin/happening/{}", happening.id),
        Some(&atk),
    );
    assert_eq!(200, rsp.status());
    assert_eq!(happening, rsp.into_json::<Happening>().unwrap());

    // TODO: delete

    // list members
    let rsp = get(
        &format!("/api/v1/info/happening/{}/participants", happening.id),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    println!(
        "happening participants: {:?}",
        rsp.into_json::<Vec<MemberShortInfo>>().unwrap()
    );
    // list members
    let rsp = get(
        &format!("/api/v1/info/group/{}/participants", group.id),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    println!(
        "group participants: {:?}",
        rsp.into_json::<Vec<MemberShortInfo>>().unwrap()
    );
}

#[test]
#[ignore]
fn happening_with_json_props() {
    expect_running_service();
    let atk = get_kc_admin_token();

    // create happening broken props
    assert_eq!(
        400,
        post(
            "/api/v1/admin/happening",
            Some(&atk),
            NewHappening {
                name: "Happening A".into(),
                props: serde_json::from_str(
                    r#"{
                    "from": "202foooooooooooo:00Z",
                    "to": "2024-01-01T10:00:00Z",
                    "foo": "bar"
                }"#,
                )
                .unwrap(),
                responsibles: None,
                participants: None,
                groups: None,
            },
        )
        .status()
    );
    assert_eq!(
        400,
        post(
            "/api/v1/admin/happening",
            Some(&atk),
            NewHappening {
                name: "Happening A".into(),
                props: serde_json::from_str(
                    r#"{
                    "form": "2024-01-01T08:00:00Z",
                    "to": "2024-01-01T10:00:00Z",
                    "foo": "bar"
                }"#,
                )
                .unwrap(),
                responsibles: None,
                participants: None,
                groups: None,
            },
        )
        .status()
    );

    // create happening
    let rsp = post(
        "/api/v1/admin/happening",
        Some(&atk),
        NewHappening {
            name: "Happening A".into(),
            props: serde_json::from_str(
                r#"{
                    "from": "2024-01-01T08:00:00Z",
                    "to": "2024-01-01T10:00:00Z",
                    "foo": "bar"
                }"#,
            )
            .unwrap(),
            responsibles: None,
            participants: None,
            groups: None,
        },
    );
    assert_eq!(201, rsp.status());
    let happening = rsp.into_json::<Happening>().unwrap();

    // get happening
    let rsp = get(
        &format!("/api/v1/admin/happening/{}", happening.id),
        Some(&atk),
    );
    assert_eq!(200, rsp.status());
    assert_eq!(happening, rsp.into_json::<Happening>().unwrap());
}

#[test]
#[ignore]
fn get_happenings_with_filter() {
    expect_running_service();
    let atk = get_kc_admin_token();

    assert_eq!(
        201,
        post(
            "/api/v1/admin/happening",
            Some(&atk),
            NewHappening {
                name: "Happening to be filtered out".into(),
                props: serde_json::from_str(
                    r#"{
                    "from": "2025-01-01T10:00:00Z",
                    "to": "2025-01-01T11:00:00Z",
                    "foo": "bar"
                }"#,
                )
                .unwrap(),
                responsibles: None,
                participants: None,
                groups: None,
            },
        )
        .status()
    );
    assert_eq!(
        201,
        post(
            "/api/v1/admin/happening",
            Some(&atk),
            NewHappening {
                name: "Happening to be filtered out".into(),
                props: serde_json::from_str(
                    r#"{
                    "from": "2025-01-02T10:00:00Z",
                    "to": "2025-01-02T11:00:00Z",
                    "foo": "bar"
                }"#,
                )
                .unwrap(),
                responsibles: None,
                participants: None,
                groups: None,
            },
        )
        .status()
    );

    // get happenings without filter
    let rsp = get("/api/v1/admin/happenings", Some(&atk));
    assert_eq!(200, rsp.status());
    let number_of_all_happenings = rsp.into_json::<Vec<Happening>>().unwrap().len();
    assert!(number_of_all_happenings >= 2);

    // get happenings with filter
    let rsp = get(
        &format!(
            "/api/v1/admin/happenings?not_before={}",
            urlencoding::encode("2025-01-02T00:00:00Z"),
        ),
        Some(&atk),
    );
    assert_eq!(200, rsp.status());
    let number_of_filtered_happenings = rsp.into_json::<Vec<Happening>>().unwrap().len();
    // at least one is filtered out
    assert!(number_of_all_happenings - 1 >= number_of_filtered_happenings);
}
