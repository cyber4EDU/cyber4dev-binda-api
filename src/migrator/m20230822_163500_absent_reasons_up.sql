ALTER TYPE absent_reason RENAME TO absent_reason_old;
CREATE TYPE absent_reason AS ENUM (
    'excused', 'sicknote', 'lunchbreak', 'educational', 'classchange', 'other'
);
ALTER TABLE absents ALTER COLUMN reason TYPE absent_reason USING 'other';
DROP TYPE absent_reason_old;
