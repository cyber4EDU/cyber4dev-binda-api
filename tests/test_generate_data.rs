// FIXME: allow for now
#![allow(static_mut_refs)]
mod common;
use common::*;
use lazy_static::lazy_static;
use std::collections::HashMap;
use std::sync::Once;
use uuid::Uuid;

static mut STUDENT_CONFIGS: Vec<StudentConf> = vec![];
static DATA_GENERATED: Once = Once::new();

static mut GROUP_UUIDS: Option<HashMap<String, Uuid>> = None;
static mut HAPPENING_UUIDS: Option<HashMap<String, Uuid>> = None;
static mut STUDENT_UUIDS: Option<HashMap<Uuid, Uuid>> = None;

lazy_static! {
    pub static ref GROUPS: Vec<GroupConf> =
        serde_json::from_str(include_str!("data/groups.json")).unwrap();
    pub static ref HAPPENINGS: Vec<HappeningConf> =
        serde_json::from_str(include_str!("data/happenings.json")).unwrap();
    pub static ref STUDENTS: Vec<StudentConf> =
        serde_json::from_str(include_str!("data/students.json")).unwrap();
    pub static ref ACTIONS: Vec<ActionConf> =
        serde_json::from_str(include_str!("data/actions.json")).unwrap();
    pub static ref ABSENTS: Vec<AbsentConf> =
        serde_json::from_str(include_str!("data/absentnotes.json")).unwrap();
    pub static ref TESTS: Vec<TestConf> =
        serde_json::from_str(include_str!("data/tests.json")).unwrap();
}

fn expect_data_generated() {
    unsafe {
        DATA_GENERATED.call_once(|| {
            expect_running_service();
            let atk = get_kc_admin_token();
            let teacher_usernames = [
                "testteacher01",
                "testteacher02",
                "testteacher03",
                "testteacher04",
                "testteacher05",
            ];

            // load techers, groups, happenings and students
            let teacher_confs = teacher_usernames
                .iter()
                .map(|t| add_teacher_and_get_config(&atk, t))
                .collect::<Vec<_>>();

            let group_confs = GROUPS
                .iter()
                .map(|gc| {
                    let mut gc = gc.clone();
                    add_group(&atk, &mut gc, &teacher_confs);
                    gc
                })
                .collect::<Vec<_>>();

            let happening_confs = HAPPENINGS
                .iter()
                .map(|hc| {
                    let mut hc = hc.clone();
                    add_happening(&atk, &mut hc, &group_confs);
                    hc
                })
                .collect::<Vec<_>>();

            STUDENT_CONFIGS = STUDENTS
                .iter()
                .map(|sc| {
                    let mut sc = sc.clone();
                    add_student(&atk, &mut sc, &group_confs);
                    sc
                })
                .collect::<Vec<_>>();

            // collect generated student uuids into hashmap physical_id->id
            let mut student_uuids = HashMap::new();
            //let student_uuids = HashMap::<Uuid, Uuid>::from_iter(STUDENT_CONFIGS.iter().filter(|sc| sc.).map(|sc| ()));
            STUDENT_CONFIGS.iter().for_each(|sc| {
                if let (Some(physical_ids), Some(id)) = (&sc.physical_ids, sc.uuid) {
                    physical_ids.iter().for_each(|pid| {
                        student_uuids.insert(pid.clone(), id.clone());
                    });
                }
            });

            // collect generated group and happening uuids into hashmap name->uuid
            let group_uuids = HashMap::<String, Uuid>::from_iter(
                group_confs
                    .iter()
                    .map(|gc| (gc.name.clone(), gc.uuid.unwrap())),
            );
            let happening_uuids = HashMap::<String, Uuid>::from_iter(
                happening_confs
                    .iter()
                    .map(|hc| (hc.name.clone(), hc.uuid.unwrap())),
            );

            let ttk = get_kc_teacher_token("testteacher01");
            println!("now: 2023-06-19T08:05:00Z");

            // perform check-in/out actions from data
            ACTIONS.iter().for_each(|a| {
                println!("action: {:?}", a);
                let student_uuid = student_uuids.get(&a.physical_id).unwrap();
                let happening_uuid = happening_uuids.get(&a.happening).unwrap();
                match a.action {
                    ActionType::CheckIn => {
                        let rsp = post(
                            &format!(
                                "/api/v1/actions/happening/{}/check_in/{}?now={}",
                                happening_uuid,
                                student_uuid,
                                urlencoding::encode(&a.date)
                            ),
                            Some(&ttk),
                            "",
                        );
                        assert_eq!(200, rsp.status());
                    }
                    ActionType::CheckOut => {
                        let rsp = post(
                            &format!(
                                "/api/v1/actions/happening/{}/check_out/{}?now={}",
                                happening_uuid,
                                student_uuid,
                                urlencoding::encode(&a.date)
                            ),
                            Some(&ttk),
                            "",
                        );
                        assert_eq!(200, rsp.status());
                    }
                }
            });

            // register absent notes from data
            ABSENTS.iter().for_each(|a| {
                let student_uuid = student_uuids.get(&a.physical_id).unwrap();
                let rsp = post(
                    "/api/v1/admin/absent",
                    Some(&atk),
                    NewAbsent {
                        reason: a.reason.to_owned(),
                        member_id: student_uuid.to_owned(),
                        from_date: a.from.to_owned(),
                        until_date: a.until.to_owned(),
                        canceled: false,
                    },
                );
                assert_eq!(201, rsp.status());
            });
            STUDENT_UUIDS = Some(student_uuids);
            GROUP_UUIDS = Some(group_uuids);
            HAPPENING_UUIDS = Some(happening_uuids);
        });
    }
}

#[test]
#[ignore]
fn test_number_of_checked_in_absent_and_missing_students() {
    expect_data_generated();

    let _student_uuids = unsafe { STUDENT_UUIDS.clone().unwrap() };
    let group_uuids = unsafe { GROUP_UUIDS.clone().unwrap() };
    let happening_uuids = unsafe { HAPPENING_UUIDS.clone().unwrap() };

    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    TESTS.iter().for_each(|tc| {
        println!("\ncheck tc: {:?}", tc);
        let check_at = urlencoding::encode(&tc.date);

        let rsp = get(
            &format!("/api/v1/info/members?now={}", check_at,),
            Some(&atk),
        );
        assert_eq!(200, rsp.status());
        let rsp_data = rsp.into_json::<Vec<MemberInfo>>().unwrap();

        // check number of checked in members
        if let Some(num_checked_in) = &tc.num_checked_in {
            assert_eq!(
                num_checked_in.values().sum::<usize>(),
                rsp_data.iter().filter(|x| x.checked_in.is_some()).count()
            );
        }

        // check number of absent members
        if let Some(num_absent) = &tc.num_absent {
            assert_eq!(
                num_absent.values().sum::<usize>() + tc.num_absent_and_checked_in.unwrap_or(0),
                rsp_data.iter().filter(|x| x.absent.is_some()).count()
            );
        }

        // check number of checked in members in happenings
        if let Some(num_checked_in) = &tc.num_checked_in {
            for (happening, nci) in num_checked_in.iter() {
                let happening_uuid = happening_uuids.get(happening).unwrap();
                let rsp = get(
                    &format!(
                        "/api/v1/info/happening/{}/participants/checked_in?now={}",
                        happening_uuid, check_at,
                    ),
                    Some(&ttk),
                );
                assert_eq!(200, rsp.status());
                let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
                assert_eq!(*nci, rsp_data.len());
            }
        }

        if let Some(num_missing) = &tc.num_missing {
            // check missing group members
            for (group, nm) in num_missing.iter() {
                let group_uuid = group_uuids.get(group).unwrap();
                let rsp = get(
                    &format!(
                        "/api/v1/info/group/{}/participants/missing?now={}",
                        group_uuid, check_at,
                    ),
                    Some(&ttk),
                );
                assert_eq!(200, rsp.status());
                let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
                //println!("group missing members: {:?}", rsp_data);
                assert_eq!(*nm, rsp_data.len());
            }

            // check overall missing members
            let rsp = get(
                &format!("/api/v1/info/members/missing?now={}", check_at),
                Some(&atk),
            );
            assert_eq!(200, rsp.status());
            let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
            assert_eq!(num_missing.values().sum::<usize>(), rsp_data.len());
        }

        if let Some(num_absent) = &tc.num_absent {
            // check absent group members
            for (group, na) in num_absent.iter() {
                let group_uuid = group_uuids.get(group).unwrap();
                println!("group \"{group}\": {group_uuid:?}");
                let rsp = get(
                    &format!(
                        "/api/v1/info/group/{}/participants/absent?now={}",
                        group_uuid, check_at,
                    ),
                    Some(&ttk),
                );
                assert_eq!(200, rsp.status());
                let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
                assert_eq!(*na, rsp_data.len());
            }

            // check overall absent members
            let rsp = get(
                &format!("/api/v1/info/members/absent?now={}", check_at),
                Some(&atk),
            );
            assert_eq!(200, rsp.status());
            let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
            //println!("exp: {} but: {}", expected_absent, rsp_data.len());
            assert_eq!(num_absent.values().sum::<usize>(), rsp_data.len());
        }

        // test dashboard
        if let Some(num_checked_in) = &tc.num_checked_in {
            for (happening, nci) in num_checked_in.iter() {
                let happening_uuid = happening_uuids.get(happening).unwrap();
                let responsibles = HAPPENINGS
                    .iter()
                    .filter(|hc| &hc.name == happening)
                    .map(|hc| hc.responsibles.clone())
                    .collect::<Vec<_>>();
                let responsible = responsibles.first().unwrap().first().unwrap();
                let ttk = get_kc_teacher_token(responsible);

                let rsp = get(
                    &format!("/api/v1/me/dashboard/{}?now={}", happening_uuid, check_at,),
                    Some(&ttk),
                );
                assert_eq!(200, rsp.status());
                let dashboard = rsp.into_json::<Dashboard>().unwrap();

                // send number request
                let rsp = post(
                    &format!("/api/v1/me/dashboard/numbers?now={}", check_at,),
                    Some(&ttk),
                    dashboard.number_request,
                );
                assert_eq!(200, rsp.status());

                let nrsp = rsp.into_json::<NumberResponse>().unwrap();
                println!("Nuber response: {:?}", nrsp);
                if let Some(checked_in) = nrsp.happenings.get(&happening_uuid) {
                    assert_eq!(*nci, *checked_in as usize);
                }

                // FIXME: shows number of absent of group connected to requester
                //if let Some(absent) = &tc.num_absent {
                //    assert_eq!(absent.values().sum::<usize>(), nrsp.absent as usize);
                //}
            }
        }
    });
}

#[test]
#[ignore]
fn test_participation_info() {
    expect_data_generated();

    let group_uuids = unsafe { GROUP_UUIDS.clone().unwrap() };
    let happening_uuids = unsafe { HAPPENING_UUIDS.clone().unwrap() };

    let _atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    for group in ["Team A", "Team B"] {
        let group_uuid = group_uuids.get(group).unwrap();
        // test number of particpants
        let rsp = get(
            &format!("/api/v1/info/group/{}/participants", group_uuid),
            Some(&ttk),
        );
        assert_eq!(200, rsp.status());
        let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
        assert_eq!(20, rsp_data.len());
    }

    for (happening, expected_participants) in [("Mathe LB1", 20), ("Deutsch", 20), ("Sport", 40)] {
        let happening_uuid = happening_uuids.get(happening).unwrap();
        let rsp = get(
            &format!("/api/v1/info/happening/{}/participants", happening_uuid),
            Some(&ttk),
        );
        assert_eq!(200, rsp.status());
        let rsp_data = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
        assert_eq!(expected_participants, rsp_data.len());
    }
}

#[test]
#[ignore]
fn test_member_history() {
    expect_data_generated();

    let ttk = get_kc_teacher_token("testteacher01");

    let rsp = get(
        &format!(
            "/api/v1/info/member/pid/{}",
            // Nixie, Team B, is excused from 8 to 12
            "302c90af-48eb-4aec-aa90-9a9ef9fad834"
        ),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    let member_id = rsp.into_json::<Member>().unwrap().id;

    // load history
    let rsp = get(
        &format!(
            "/api/v1/info/member/{}/history?not_before={}&not_after={}",
            member_id,
            urlencoding::encode("2023-06-19T06:00:00Z"),
            urlencoding::encode("2023-06-19T20:00:00Z"),
        ),
        Some(&ttk),
    );
    assert_eq!(200, rsp.status());
    let rsp_data = rsp.into_json::<History>().unwrap();
    assert_eq!(4, rsp_data.happening_presences.len());

    for hpi in rsp_data.happening_presences {
        match hpi.name.as_str() {
            // 8-10
            "Deutsch" => {
                assert!(hpi.absent_entries.len() == 1);
                assert!(hpi.checked_ins.is_empty());
                assert!(hpi.checked_outs.is_empty());
            }
            // 10-12
            "Mathe" => {
                assert!(hpi.absent_entries.len() == 1);
                assert!(hpi.checked_ins.is_empty());
                assert!(hpi.checked_outs.is_empty());
            }
            // 13-15
            "Erdkunde" => {
                assert!(hpi.absent_entries.is_empty());
                assert!(hpi.checked_ins.is_empty());
                assert!(hpi.checked_outs.is_empty());
            }
            // 15-16
            "Sport" => {
                assert!(hpi.absent_entries.is_empty());
                // check-in at 15:14
                assert!(hpi.checked_ins.len() == 1);
                // event has no auto checkout
                assert!(hpi.checked_outs.is_empty());
            }
            _ => {}
        }
    }

    // load history other timeframes
    for (number, not_before, not_after) in [
        (3, "2023-06-19T08:00:00Z", "2023-06-19T15:00:00Z"),
        (2, "2023-06-19T08:01:00Z", "2023-06-19T15:00:00Z"),
        (1, "2023-06-19T08:01:00Z", "2023-06-19T14:59:59Z"),
    ] {
        let rsp = get(
            &format!(
                "/api/v1/info/member/{}/history?not_before={}&not_after={}",
                member_id,
                urlencoding::encode(not_before),
                urlencoding::encode(not_after),
            ),
            Some(&ttk),
        );
        assert_eq!(200, rsp.status());
        let rsp_data = rsp.into_json::<History>().unwrap();
        assert_eq!(number, rsp_data.happening_presences.len());
    }
}
