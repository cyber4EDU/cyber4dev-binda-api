#![allow(deprecated)]
use super::{
    GroupId, GroupShortInfo, Happening, HappeningId, HappeningName, HappeningPropsMinimum,
    MemberIdVariant, MemberInfo,
};
use crate::error::Error;
use chrono::{DateTime, Utc};
use futures::future::try_join_all;
use sea_orm::{DatabaseConnection, DbBackend, FromQueryResult, Statement};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use tracing::info;
use utoipa::{IntoParams, ToSchema};

#[derive(Serialize, Deserialize, ToSchema)]
pub struct HappeningDashboard {
    pub id: HappeningId,
    pub name: HappeningName,
    pub props: Option<HappeningPropsMinimum>,
    pub responsibles: Option<Vec<MemberInfo>>,
    pub groups: Option<Vec<GroupShortInfo>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
#[deprecated]
pub struct DashboardOld {
    pub my_happening: HappeningDashboard,
    pub related_happenings: Option<Vec<HappeningDashboard>>,
    pub number_request: NumberRequest,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct Dashboard {
    pub happenings: Vec<HappeningDashboard>,
    pub number_request: NumberRequest,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct AdminDashboard {
    pub groups: Vec<GroupShortInfo>,
    pub number_request: AdminNumberRequest,
}

#[derive(Serialize, Deserialize, ToSchema, IntoParams)]
pub struct NumberRequest {
    pub happenings: Vec<HappeningId>,
    pub groups: Vec<GroupId>,
    pub now: Option<DateTime<Utc>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct NumberResponse {
    pub happenings: HashMap<HappeningId, u16>,
    pub expected: u16,
    pub missing: u16,
    pub absent: u16,
}

#[derive(Serialize, Deserialize, ToSchema, IntoParams)]
pub struct AdminNumberRequest {
    pub groups: Vec<GroupId>,
    pub now: Option<DateTime<Utc>>,
}

#[derive(Serialize, Deserialize, ToSchema)]
pub struct AdminNumberResponse {
    pub groups: HashMap<GroupId, u16>,
    pub expected: u16,
    pub missing: u16,
    pub absent: u16,
}

#[derive(FromQueryResult, Debug)]
#[allow(dead_code)]
struct IdNumbersFromQueryResult {
    id: uuid::Uuid,
    count: i64,
}
#[derive(FromQueryResult, Debug)]
#[allow(dead_code)]
struct NumberFromQueryResult {
    count: i64,
}

impl HappeningDashboard {
    pub async fn load_from_happening(
        conn: &DatabaseConnection,
        hp: Happening,
    ) -> Result<Self, Error> {
        let responsibles = if let Some(rsv) = hp.responsibles {
            Some(
                try_join_all(
                    rsv.iter()
                        .map(|rs| MemberInfo::load(conn, MemberIdVariant::Id(rs), None)),
                )
                .await?,
            )
        } else {
            None
        };
        let groups = if let Some(gv) = hp.groups {
            Some(try_join_all(gv.iter().map(|g| GroupShortInfo::load(conn, g))).await?)
        } else {
            None
        };

        Ok(Self {
            id: hp.id,
            name: hp.name,
            props: serde_json::from_value::<HappeningPropsMinimum>(hp.props.into()).ok(),
            responsibles,
            groups,
        })
    }
}

impl Dashboard {
    pub async fn load(
        conn: &DatabaseConnection,
        happenings: Vec<Happening>,
    ) -> Result<Self, Error> {
        let mut happening_ids = HashSet::new();
        let mut group_ids = HashSet::new();
        happenings.iter().for_each(|h| {
            happening_ids.insert(h.id);
            if let Some(gv) = h.groups.clone() {
                gv.iter().for_each(|g| {
                    group_ids.insert(*g);
                })
            }
        });

        let happenings = try_join_all(
            happenings
                .into_iter()
                .map(|hp| HappeningDashboard::load_from_happening(conn, hp)),
        )
        .await?;

        let number_request = NumberRequest {
            happenings: happening_ids.into_iter().collect(),
            groups: group_ids.into_iter().collect(),
            now: None,
        };

        Ok(Self {
            happenings,
            number_request,
        })
    }
}

impl AdminDashboard {
    pub async fn load(conn: &DatabaseConnection, date: DateTime<Utc>) -> Result<Self, Error> {
        let groups = GroupShortInfo::load_all_active(conn, date).await?;
        let number_request = AdminNumberRequest {
            groups: groups.iter().map(|g| g.id).collect(),
            now: None,
        };

        Ok(Self {
            groups,
            number_request,
        })
    }
}

impl DashboardOld {
    pub async fn load(
        conn: &DatabaseConnection,
        my_happening: Happening,
        related_happenings: Vec<Happening>,
    ) -> Result<Self, Error> {
        let mut happening_ids = HashSet::from([my_happening.id]);
        let mut group_ids = HashSet::new();
        if let Some(gv) = my_happening.groups.clone() {
            gv.iter().for_each(|g| {
                group_ids.insert(*g);
            })
        }
        related_happenings.iter().for_each(|h| {
            happening_ids.insert(h.id);
            if let Some(gv) = h.groups.clone() {
                gv.iter().for_each(|g| {
                    group_ids.insert(*g);
                })
            }
        });

        let related_happenings = try_join_all(
            related_happenings
                .into_iter()
                .map(|hp| HappeningDashboard::load_from_happening(conn, hp)),
        )
        .await?;
        let my_happening = HappeningDashboard::load_from_happening(conn, my_happening).await?;
        let related_happenings = if related_happenings.is_empty() {
            None
        } else {
            Some(related_happenings)
        };

        let number_request = NumberRequest {
            happenings: happening_ids.into_iter().collect(),
            groups: group_ids.into_iter().collect(),
            now: None,
        };

        Ok(Self {
            my_happening,
            related_happenings,
            number_request,
        })
    }
}

impl NumberResponse {
    pub async fn load_from(
        conn: &DatabaseConnection,
        date: DateTime<Utc>,
        request: &NumberRequest,
    ) -> Result<Self, Error> {
        let date = request.now.unwrap_or(date);
        let happening_numbers =
            IdNumbersFromQueryResult::find_by_statement(Statement::from_sql_and_values(
                DbBackend::Postgres,
                r#"
                    SELECT DISTINCT
                        gh.happening_id AS id,
                        COUNT(DISTINCT members.id) AS count
                    FROM
                        members
                    INNER JOIN member_is_to_group AS mitg
                        ON
                            mitg.source_member_id = members.id
                            AND mitg.role = 'participant'
                    INNER JOIN groups_happenings as gh
                        ON
                            gh.group_id = mitg.target_group_id
                            AND gh.happening_id
                            = ANY(CAST($1 AS UUID ARRAY))
                    -- take who was checked in
                    INNER JOIN check_in_out AS c_in
                        ON
                            c_in.member_id = members.id
                            AND c_in.check_type = 'in'
                            AND c_in.happening_id = gh.happening_id
                            AND c_in.date <= $2
                    -- but remove who was checked out afterwards
                    LEFT JOIN check_in_out AS c_out
                        ON
                            c_out.member_id = members.id
                            AND c_out.check_type = 'out'
                            AND c_out.happening_id = gh.happening_id
                            AND c_out.date <= $2
                            AND c_out.date >= c_in.date
                    WHERE c_out.id IS NULL
                    GROUP BY gh.happening_id
                "#,
                [
                    Into::<sea_orm::Value>::into(
                        request
                            .happenings
                            .iter()
                            .map(|hid| hid.0)
                            .collect::<Vec<_>>(),
                    ),
                    date.into(),
                ],
            ))
            .all(conn)
            .await?;
        info!("happening_numbers: {:?}", happening_numbers);
        let happenings: HashMap<HappeningId, u16> = happening_numbers
            //let happenings: Vec<(HappeningId, u16)> = happening_numbers
            .into_iter()
            .map(|hn| match u16::try_from(hn.count) {
                Ok(v) => Ok((hn.id.into(), v)),
                Err(_) => Err(Error::Plain("Failed to convert happening_numbers")),
            })
            .collect::<Result<HashMap<_, _>, _>>()?;

        let expected = NumberFromQueryResult::find_by_statement(Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                SELECT DISTINCT
                    COUNT(DISTINCT members.id) AS count
                FROM
                    members
                INNER JOIN member_is_to_group AS mitg
                    ON
                        mitg.source_member_id = members.id
                        AND mitg.target_group_id
                        = ANY(CAST($1 AS UUID ARRAY))
                        AND mitg.role = 'participant'
            "#,
            [Into::<sea_orm::Value>::into(
                request.groups.iter().map(|gid| gid.0).collect::<Vec<_>>(),
            )],
        ))
        .one(conn)
        .await?
        .ok_or(Error::NotFound("Group ids not found"))?;
        info!("expected: {:?}", expected);

        let absent = NumberFromQueryResult::find_by_statement(Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                SELECT DISTINCT
                    COUNT(DISTINCT members.id)
                FROM
                    members
                INNER JOIN member_is_to_group
                    ON member_is_to_group.source_member_id = members.id
                        AND member_is_to_group.target_group_id
                        = ANY(CAST($1 AS UUID ARRAY))
                        AND member_is_to_group.role = 'participant'
                INNER JOIN absents
                    ON absents.member_id = members.id
                        AND absents.from_date <= $2
                        AND absents.until_date >= $2
                LEFT JOIN LATERAL
                    (
                        SELECT
                            "check_in_out"."happening_id",
                            "check_in_out"."check_type"
                        FROM check_in_out
                        WHERE
                            "check_in_out"."member_id" = "members"."id"
                            AND "check_in_out"."date" <= $2
                        ORDER BY "check_in_out"."date" DESC
                        LIMIT 1
                    ) AS "c_in" ON TRUE
                LEFT JOIN "happenings" AS "happening"
                    ON "happening"."id" = "c_in"."happening_id"
                    AND "c_in"."check_type" = 'in'
                WHERE "happening"."id" IS NULL
            "#,
            [
                Into::<sea_orm::Value>::into(
                    request.groups.iter().map(|gid| gid.0).collect::<Vec<_>>(),
                ),
                date.into(),
            ],
        ))
        .one(conn)
        .await?
        // FIXME: can this be None?
        .ok_or(Error::NotFound("Group ids not found"))?;
        info!("absent: {:?}", absent);

        let expected =
            u16::try_from(expected.count).map_err(|_| Error::Plain("Failed to convert number"))?;
        let absent =
            u16::try_from(absent.count).map_err(|_| Error::Plain("Failed to convert number"))?;

        let missing = expected - absent - happenings.values().sum::<u16>();

        Ok(NumberResponse {
            happenings,
            expected,
            absent,
            missing,
        })
    }
}

impl AdminNumberResponse {
    pub async fn load_from(
        conn: &DatabaseConnection,
        date: DateTime<Utc>,
        request: &AdminNumberRequest,
    ) -> Result<Self, Error> {
        let date = request.now.unwrap_or(date);
        let group_numbers =
            IdNumbersFromQueryResult::find_by_statement(Statement::from_sql_and_values(
                DbBackend::Postgres,
                r#"
                    SELECT DISTINCT
                        mitg.target_group_id AS id,
                        COUNT(DISTINCT members.id) AS count
                    FROM
                        members
                    INNER JOIN member_is_to_group AS mitg
                        ON
                            mitg.source_member_id = members.id
                            AND mitg.role = 'participant'
                            AND mitg.target_group_id
                                = ANY(CAST($1 AS UUID ARRAY))
                    INNER JOIN groups_happenings as gh
                        ON
                            gh.group_id = mitg.target_group_id
                    -- take who was checked in
                    INNER JOIN check_in_out AS c_in
                        ON
                            c_in.member_id = members.id
                            AND c_in.check_type = 'in'
                            AND c_in.happening_id = gh.happening_id
                            AND c_in.date <= $2
                    -- but remove who was checked out afterwards
                    LEFT JOIN check_in_out AS c_out
                        ON
                            c_out.member_id = members.id
                            AND c_out.check_type = 'out'
                            AND c_out.happening_id = gh.happening_id
                            AND c_out.date <= $2
                            AND c_out.date >= c_in.date
                    WHERE c_out.id IS NULL
                    GROUP BY mitg.target_group_id
                "#,
                [
                    Into::<sea_orm::Value>::into(
                        request.groups.iter().map(|gid| gid.0).collect::<Vec<_>>(),
                    ),
                    date.into(),
                ],
            ))
            .all(conn)
            .await?;

        let groups: HashMap<GroupId, u16> = group_numbers
            .into_iter()
            .map(|gn| match u16::try_from(gn.count) {
                Ok(v) => Ok((gn.id.into(), v)),
                Err(_) => Err(Error::Plain("Failed to convert group_numbers")),
            })
            .collect::<Result<HashMap<_, _>, _>>()?;

        let expected = NumberFromQueryResult::find_by_statement(Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                SELECT DISTINCT
                    COUNT(DISTINCT members.id) AS count
                FROM
                    members
                INNER JOIN member_is_to_group AS mitg
                    ON
                        mitg.source_member_id = members.id
                        AND mitg.target_group_id
                        = ANY(CAST($1 AS UUID ARRAY))
                        AND mitg.role = 'participant'
            "#,
            [Into::<sea_orm::Value>::into(
                request.groups.iter().map(|gid| gid.0).collect::<Vec<_>>(),
            )],
        ))
        .one(conn)
        .await?
        .ok_or(Error::NotFound("Group ids not found"))?;

        let absent = NumberFromQueryResult::find_by_statement(Statement::from_sql_and_values(
            DbBackend::Postgres,
            r#"
                SELECT DISTINCT
                    COUNT(DISTINCT members.id)
                FROM
                    members
                INNER JOIN member_is_to_group
                    ON member_is_to_group.source_member_id = members.id
                        AND member_is_to_group.target_group_id
                        = ANY(CAST($1 AS UUID ARRAY))
                        AND member_is_to_group.role = 'participant'
                INNER JOIN absents
                    ON absents.member_id = members.id
                        AND absents.from_date <= $2
                        AND absents.until_date >= $2
                LEFT JOIN LATERAL
                    (
                        SELECT
                            "check_in_out"."happening_id",
                            "check_in_out"."check_type"
                        FROM check_in_out
                        WHERE
                            "check_in_out"."member_id" = "members"."id"
                            AND "check_in_out"."date" <= $2
                        ORDER BY "check_in_out"."date" DESC
                        LIMIT 1
                    ) AS "c_in" ON TRUE
                LEFT JOIN "happenings" AS "happening"
                    ON "happening"."id" = "c_in"."happening_id"
                    AND "c_in"."check_type" = 'in'
                WHERE "happening"."id" IS NULL
            "#,
            [
                Into::<sea_orm::Value>::into(
                    request.groups.iter().map(|gid| gid.0).collect::<Vec<_>>(),
                ),
                date.into(),
            ],
        ))
        .one(conn)
        .await?
        .ok_or(Error::NotFound("Group ids not found"))?;
        info!("absent: {:?}", absent);

        let expected =
            u16::try_from(expected.count).map_err(|_| Error::Plain("Failed to convert number"))?;
        let absent =
            u16::try_from(absent.count).map_err(|_| Error::Plain("Failed to convert number"))?;

        let missing = expected - absent - groups.values().sum::<u16>();

        Ok(Self {
            groups,
            expected,
            missing,
            absent,
        })
    }
}
