#![allow(dead_code)]
#![allow(unused)]

mod api;
mod api_docs;
mod auth;
mod config;
mod data;
mod error;
mod model;

use std::io::Write;
use utoipa::OpenApi;

fn main() -> std::io::Result<()> {
    write!(
        std::fs::File::create("docs/openapi_v1.yaml")?,
        "{}",
        api_docs::ApiDoc::openapi()
            .to_yaml()
            .map_err(|err| std::io::Error::new(std::io::ErrorKind::Other, err))?
    )
}
