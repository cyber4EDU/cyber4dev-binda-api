mod common;
use common::*;

#[test]
#[ignore]
fn create_get_update_member() {
    expect_running_service();
    let atk = get_kc_admin_token();

    let pid1 = uuid::Uuid::new_v4();
    let pid2 = uuid::Uuid::new_v4();
    let pid3 = uuid::Uuid::new_v4();
    let pid4 = uuid::Uuid::new_v4();

    // create member
    let rsp = post(
        "/api/v1/admin/member",
        Some(&atk),
        NewMember {
            first_name: "Foo".into(),
            middle_names: None,
            last_name: "Bar".into(),
            person_qualifier: None,
            identity_id: None,
            responsibles: None,
            responsible_for: None,
            participant_in: None,
            physical_ids: Some(vec![
                PhysicalId {
                    id: pid1,
                    deactivated: false,
                    note: Some("foo bar".to_string()),
                },
                PhysicalId {
                    id: pid2,
                    deactivated: false,
                    note: None,
                },
                PhysicalId {
                    id: pid3,
                    deactivated: false,
                    note: None,
                },
            ]),
            contact: Some(
                serde_json::from_str(
                    r#"
                        [
                            {
                                "name": "Baz",
                                "telefone": "0123456789",
                                "email": "ab@c.d"
                            },
                            {
                                "name": "Bax",
                                "telefone": "1234567890",
                                "email": "dc@b.a"
                            }
                        ]
                    "#,
                )
                .unwrap(),
            ),
        },
    );
    assert_eq!(201, rsp.status());
    let mut member = rsp.into_json::<Member>().unwrap();

    // get member
    let rsp = get(&format!("/api/v1/admin/member/{}", member.id), Some(&atk));
    assert_eq!(200, rsp.status());
    assert_eq!(member, rsp.into_json::<Member>().unwrap());

    // update member name and set identity id
    // remove and add physical id
    member.first_name = "create_get".to_string();
    member.last_name = "update_member".to_string();
    member.identity_id = Some(uuid::Uuid::new_v4());
    member.physical_ids = member.physical_ids.map(|mut pids| {
        pids[2] = PhysicalId {
            id: pid4,
            deactivated: false,
            note: Some("replaced physical id".to_string()),
        };
        pids
    });

    let rsp = put("/api/v1/admin/member", Some(&atk), member.clone());
    assert_eq!(200, rsp.status());
    assert_eq!(member, rsp.into_json::<Member>().unwrap());

    // get member
    let rsp = get(&format!("/api/v1/admin/member/{}", member.id), Some(&atk));
    assert_eq!(200, rsp.status());
    assert_eq!(member, rsp.into_json::<Member>().unwrap());

    // TODO: delete
    // TODO: get -> 404
}

#[test]
#[ignore]
fn load_member_by_iid() {
    expect_running_service();
    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    let rsp = get("/api/v1/me", Some(&ttk));
    assert_eq!(200, rsp.status());
    let teacher_id = rsp.into_json::<MemberShortInfo>().unwrap().id;

    let rsp = get(&format!("/api/v1/admin/member/{}", teacher_id), Some(&atk));
    assert_eq!(200, rsp.status());
    let member_data = rsp.into_json::<Member>().unwrap();
    let teacher_iid = member_data.identity_id.unwrap();
    assert_eq!(
        teacher_iid.to_string(),
        "331dac72-9e2e-4677-a3fb-8cca168642be"
    );

    // load by iid
    let rsp = get(
        &format!("/api/v1/admin/member/iid/{}", teacher_iid),
        Some(&atk),
    );
    assert_eq!(200, rsp.status());
    assert_eq!(member_data, rsp.into_json::<Member>().unwrap());
}

#[test]
#[ignore]
fn load_member_by_pid() {
    expect_running_service();
    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    // pick one student
    let rsp = get("/api/v1/info/members", Some(&atk));
    assert_eq!(200, rsp.status());
    let students = rsp.into_json::<Vec<MemberShortInfo>>().unwrap();
    let student = students.first().unwrap();

    // add physical id
    let pid = uuid::Uuid::new_v4();

    // get student data and add physical id
    let rsp = get(&format!("/api/v1/admin/member/{}", student.id), Some(&atk));
    assert_eq!(200, rsp.status());
    let mut student_data = rsp.into_json::<Member>().unwrap();
    student_data.physical_ids = Some(vec![PhysicalId {
        id: pid,
        deactivated: false,
        note: None,
    }]);
    let rsp = put("/api/v1/admin/member", Some(&atk), student_data.clone());
    assert_eq!(200, rsp.status());

    // admin get student by pid
    let rsp = get(&format!("/api/v1/admin/member/pid/{}", pid), Some(&atk));
    assert_eq!(200, rsp.status());
    assert_eq!(student_data, rsp.into_json::<Member>().unwrap());

    // teacher get student by pid
    let rsp = get(&format!("/api/v1/info/member/pid/{}", pid), Some(&ttk));
    assert_eq!(200, rsp.status());
}
