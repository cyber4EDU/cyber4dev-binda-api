use super::{Absent, AbsentId, MemberIdVariant, MemberInfo, NewAbsent};
use crate::error::Error;
use crate::model::{prelude::*, *};
use sea_orm::{ActiveModelTrait, ActiveValue, DatabaseConnection, EntityTrait};
use tracing::info;

impl NewAbsent {
    pub async fn insert(
        self,
        conn: &DatabaseConnection,
        creator: MemberIdVariant<'_>,
    ) -> Result<Absent, Error> {
        let creator_info = MemberInfo::load(conn, creator, None).await?;
        let new_absent = absents::ActiveModel {
            member_id: ActiveValue::Set(self.member_id.into()),
            reason: ActiveValue::Set(self.reason.clone().into()),
            from_date: ActiveValue::Set(self.from_date.into()),
            until_date: ActiveValue::Set(self.until_date.into()),
            canceled: ActiveValue::Set(self.canceled),
            creator: ActiveValue::Set(creator_info.id.0.into()),
            ..ActiveModelTrait::default()
        }
        .insert(conn)
        .await?;

        info!("insert new absent");
        Ok(Absent {
            id: new_absent.id.into(),
            member_id: self.member_id,
            reason: self.reason,
            from_date: self.from_date,
            until_date: self.until_date,
            canceled: self.canceled,
            creator: Some(creator_info.id),
        })
    }
}

impl Absent {
    pub async fn load(conn: &DatabaseConnection, id: AbsentId) -> Result<Absent, Error> {
        let absent: absents::Model = Absents::find_by_id(id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Absent not found"))?;
        Ok(Self {
            id: absent.id.into(),
            member_id: absent.member_id.into(),
            reason: absent.reason.into(),
            from_date: absent.from_date.into(),
            until_date: absent.until_date.into(),
            canceled: absent.canceled,
            creator: absent.creator.map(Into::into),
        })
    }

    pub async fn update(self, conn: &DatabaseConnection) -> Result<Self, Error> {
        let id = Absents::find_by_id(self.id)
            .one(conn)
            .await?
            .ok_or(Error::NotFound("Absent not found"))?
            .id;
        absents::ActiveModel {
            id: ActiveValue::Set(id),
            member_id: ActiveValue::Set(self.member_id.into()),
            reason: ActiveValue::Set(self.reason.clone().into()),
            from_date: ActiveValue::Set(self.from_date.into()),
            until_date: ActiveValue::Set(self.until_date.into()),
            canceled: ActiveValue::Set(self.canceled),
            creator: ActiveValue::Set(self.creator.map(Into::into)),
            ..ActiveModelTrait::default()
        }
        .update(conn)
        .await?;
        Ok(self)
    }

    pub async fn delete(conn: &DatabaseConnection, id: AbsentId) -> Result<(), Error> {
        absents::Entity::delete_by_id(id).exec(conn).await?;
        Ok(())
    }
}

// TODO: list by timeframe
// TODO: list by group and timeframe
