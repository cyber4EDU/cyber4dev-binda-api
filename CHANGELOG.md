# Changelog

All notable changes to this project will be documented in this file.

## [unreleased]

### Fixed
- group roles are inherited by happenings properly

### Changed
- admin-info endpoints are deprecated, use info endpoints instead!

### Added
- history endpoint to retrieve presences/absents for all happenings in a given timeframe for a given member
    - `/api/v1/info/member/<member-id>/history` with query parameters `not_before` and `not_after`
 
### Removed
- _user_ role (_admin_ or _teacher_ is enough)

## [0.2.0] - 2025-01-23

### Fixed
- Queries that collect information about missing members
- Non UTC-timestamps can be used now safely

### Changed

- Member identity management now consists of
    - id - the id used to reference the member at the api in general (e.g. for update, deletion and all other cases)
    - iid - the identity id, given by an external identity provider (for check-in/out, get info)
    - pid - the physical id, one member can have multiple of those, they represent a physical object containing an id (for check-in/out, get info)
- Members now contain lists of physical ids including some information
- All member related status information is now contained the the new type `MemberInfo`
    - all endpoints that return some kind of status information of members use this type
    - action endpoints as well
    - and contact information
- Check out for all members who are checked-in into a specific happening produces a check-out entry for each member
- OperationIds in openapi fixed by renaming functions
- Absent-Id included in MemberInfo
- Members now allow arbitrary json objects to store contact
 
### Added

- `middle_names` and `person_qualifier` to members
- `contact`-information to members
- Further endpoints (see a [API documentation](./docs/openapi_v1.yaml) for details)
    - `/api/v1/admin/physical_id...` to manage physical ids
        - If at creation no physical id is given, it will be generated
    - `/api/v1/admin/groups` and `/api/v1/admin/happenings` to get a list of all groups/happenings
    - a whole block of member-status endpoints for the admin user, see "admin-info" block in API documentation
    - `/api/v1/info/group/<group-id>/participants/checked_in` return a list of checked-in group participants
    - `/api/v1/info/members` return a list of member status information
    - `/api/v1/info/member/{id}` return status infromation for one member by id
    - `/api/v1/info/member/iid/{iid}` return status infromation for one member by identity id
    - `/api/v1/info/member/pid/{pid}` return status infromation for one member by physical id
    - `/api/v1/info/dashboard/happening/id` replacement for `/api/v1/me/dashboard/{id}`
        - the return type now does not distinguish by "my_" and "related_" happenings, the user has to derive which happenings belong to them by the responsible field
    - `/api/v1/info/dashboard/numbers` replacement for `/api/v1/me/dashboard/numbers`
    - a whole block of "absent" endpoints for teachers to create, deactivate, and modify absent entries
        - modify is only possible if modifier is also creator of absent-entry
        - deactivate (`DELETE`) can be done by everyone
- `auto_checkout` as option in happening properties, in minutes from end of happening (e.g. value `-1` will create a checkout entry for all members at check-in for `1` minute before the end of the happening)
- store physical id at check-in/out if it was used
- simple prometheus metrics at `/metrics` endpoint
- dashboard for admin (similar to teacher dashboard)
- add optional `not_before`-filter to `/api/v1/admin/happenings`

### Removed

- Ability to check-in/out or get member info by keycloak-id (`kc` in path), use `iid` instead (see above)
  (the endpoints in question are marked as deprecated and will be removed in the upcomming release!)

## [0.1.0] - 2023-07-07

This version was used for an initial test with users.

### Added

- Further endpoints (see a [API documentation](./docs/openapi_v1.yaml) for details)
    - `/status`
    - `/version`
    - `/api/v1/admin/...` additional endpoints for _absents_
    - `/api/v1/me`
        - `/happenings` (`GET`) return a list of requesters happenings in a given timeframe
        - `/dashboard/<happening-id>` (`GET`) return dashboard information and number-request for a given happening
        - `/dashboard/numbers` (`POST`) return numbers to show in the dashboard for a given number-request
    - `/api/v1/info` (all `GET`)
        - `/happening/<happening-id>/participants` return a list of all happening participants
        - `/happening/<happening-id>/participants/checked_in` return a list of checked-in happening participants
        - `/group/<group-id>/participants` return a list of gll roup participants
        - `/group/<group-id>/participants/absent` return a list of absent group participants
        - `/group/<group-id>/participants/missing` return a list of missing group participants
        - `/member/<member-id>` return information of a member by member-id
        - `/member/kc/<keycloak-id>` return information of a member by keycloak-id
    - `/api/v1/actions` (all `POST`)
        - `/happening/<happening-id>/check_in/<member-id>` check-in a member by member-id into a happening
        - `/happening/<happening-id>/check_in/kc/<member-id>` check-in a member by keycloak-id into a happening
        - `/happening/<happening-id>/check_out/<member-id>` check-out a member by member-id into a happening
        - `/happening/<happening-id>/check_out/kc/<member-id>` check-out a member by keycloak-id into a happening
        - `/happening/<happening-id>/close` does not work in this version
- Authorization
    - `role_binda_admin` required at all `/api/v1/admin` endpoints
    - `role_binda_teacher` required at all `/api/v1/info` and `/api/v1/actions` endpoints
    - `role_binda_user` required at all `/api/v1/me` endpoints
- Integration tests that require a running keycloak instance

## [0.0.1] - 2023-06-02

### Added

- Initial development version of a reduced subset of the planned API, including
    - API-endpoints (`/api/v1/admin/...`) for administrating _members_, _groups_ and _happenings_
    - API-endpoint (`/api/v1/me`) that returns information about the member that holds the keycloak-id from the token
    - Authentication via keycloak JWT token
    - OpenAPI (swagger) documentation at [`/swagger/`](http://localhost:8000/swagger/)
- Some test scripts to fill the database with test-data
