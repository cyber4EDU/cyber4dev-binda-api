mod common;
use common::*;

const ADMIN_ENDPOINTS: &[(RequestMethod, &str)] = &[
    (RequestMethod::Put, "/admin/absent"),
    (RequestMethod::Pos, "/admin/absent"),
    (
        RequestMethod::Get,
        "/admin/absent/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Del,
        "/admin/absent/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Put, "/admin/group"),
    (RequestMethod::Pos, "/admin/group"),
    (
        RequestMethod::Get,
        "/admin/group/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Del,
        "/admin/group/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Get, "/admin/groups"),
    (RequestMethod::Put, "/admin/happening"),
    (RequestMethod::Pos, "/admin/happening"),
    (
        RequestMethod::Get,
        "/admin/happening/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Del,
        "/admin/happening/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Get, "/admin/happenings"),
    (RequestMethod::Put, "/admin/member"),
    (RequestMethod::Pos, "/admin/member"),
    (
        RequestMethod::Get,
        "/admin/member/iid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/admin/member/pid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/admin/member/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Del,
        "/admin/member/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Put, "/admin/physical_id"),
    (RequestMethod::Pos, "/admin/physical_id"),
    (
        RequestMethod::Get,
        "/admin/physical_id/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Del,
        "/admin/physical_id/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Get, "/admin/physical_ids"),
    (
        RequestMethod::Get,
        "/admin/info/absents/member/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/admin/info/group/00000000-0000-0000-0000-00000000/participants/checked_in",
    ),
    (
        RequestMethod::Get,
        "/admin/info/member/iid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/admin/info/member/pid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/admin/info/member/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Get, "/admin/info/members"),
    (RequestMethod::Get, "/admin/info/members/absent"),
    (RequestMethod::Get, "/admin/info/members/missing"),
    (RequestMethod::Get, "/admin/info/dashboard"),
    (RequestMethod::Pos, "/admin/info/dashboard/numbers"),
    (
        RequestMethod::Pos,
        "/admin/check_out/iid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Pos,
        "/admin/check_out/pid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Pos,
        "/admin/check_out/00000000-0000-0000-0000-00000000",
    ),
];

const TEACHER_ENDPOINTS: &[(RequestMethod, &str)] = &[
    (RequestMethod::Put, "/absent"),
    (RequestMethod::Pos, "/absent"),
    (RequestMethod::Get, "/absent/00000000-0000-0000-0000-00000000"),
    (RequestMethod::Del, "/absent/00000000-0000-0000-0000-00000000"),
    (RequestMethod::Get, "/absents/member/00000000-0000-0000-0000-00000000"),
    (
        RequestMethod::Pos,
        "/actions/happening/00000000-0000-0000-0000-00000000/check_in/iid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Pos,
        "/actions/happening/00000000-0000-0000-0000-00000000/check_in/kc/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Pos,
        "/actions/happening/00000000-0000-0000-0000-00000000/check_in/pid/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Pos, "/actions/happening/00000000-0000-0000-0000-00000000/check_in/00000000-0000-0000-0000-00000000"),
    (
        RequestMethod::Pos,
        "/actions/happening/00000000-0000-0000-0000-00000000/check_out/iid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Pos,
        "/actions/happening/00000000-0000-0000-0000-00000000/check_out/kc/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Pos,
        "/actions/happening/00000000-0000-0000-0000-00000000/check_out/pid/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Pos, "/actions/happening/00000000-0000-0000-0000-00000000/check_out/00000000-0000-0000-0000-00000000"),
    (RequestMethod::Pos, "/actions/happening/00000000-0000-0000-0000-00000000/close"),
];

const TEACHER_OR_ADMIN_ENDPOINTS: &[(RequestMethod, &str)] = &[
    (RequestMethod::Get, "/me"),
    (RequestMethod::Pos, "/me/dashboard/numbers"),
    (
        RequestMethod::Get,
        "/me/dashboard/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Get, "/me/happenings"),
    (
        RequestMethod::Get,
        "/info/group/00000000-0000-0000-0000-00000000/participants",
    ),
    (
        RequestMethod::Get,
        "/info/group/00000000-0000-0000-0000-00000000/participants/absent",
    ),
    (
        RequestMethod::Get,
        "/info/group/00000000-0000-0000-0000-00000000/participants/checked_in",
    ),
    (
        RequestMethod::Get,
        "/info/group/00000000-0000-0000-0000-00000000/participants/missing",
    ),
    (
        RequestMethod::Get,
        "/info/happening/00000000-0000-0000-0000-00000000/participants",
    ),
    (
        RequestMethod::Get,
        "/info/happening/00000000-0000-0000-0000-00000000/participants/checked_in",
    ),
    (
        RequestMethod::Get,
        "/info/member/iid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/info/member/kc/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/info/member/pid/00000000-0000-0000-0000-00000000",
    ),
    (
        RequestMethod::Get,
        "/info/member/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Get, "/info/members"),
    (RequestMethod::Get, "/info/members/absent"),
    (RequestMethod::Get, "/info/members/missing"),
    (
        RequestMethod::Get,
        "/info/dashboard/happening/00000000-0000-0000-0000-00000000",
    ),
    (RequestMethod::Pos, "/info/dashboard/numbers"),
    (
        RequestMethod::Get,
        "/info/member/00000000-0000-0000-0000-00000000/history",
    ),
];

const NO_AUTH_ENDPOINTS: &[(RequestMethod, &str)] = &[
    (RequestMethod::Get, "/version"),
    (RequestMethod::Get, "/status"),
];

fn expect_status(status: &[u16], token: Option<&str>, endpoints: &[(RequestMethod, &str)]) {
    //
    for (method, path) in endpoints {
        let path = format!("/api/v1{}", path);
        let rsp = match method {
            RequestMethod::Get => get(&path, token),
            RequestMethod::Pos => post(&path, token, Dummy {}),
            RequestMethod::Put => put(&path, token, Dummy {}),
            RequestMethod::Del => delete(&path, token),
        };
        eprintln!("calling {:?} {} -> {}", method, path, rsp.status());
        assert!(status.contains(&rsp.status()));
    }
}

#[test]
#[ignore]
fn unauthorized() {
    expect_running_service();

    // without token
    expect_status(&[401], None, ADMIN_ENDPOINTS);
    expect_status(&[401], None, TEACHER_ENDPOINTS);
    expect_status(&[401], None, TEACHER_OR_ADMIN_ENDPOINTS);
}

#[test]
#[ignore]
fn forbidden() {
    expect_running_service();

    // with teacher token
    let ttk = get_kc_teacher_token("testteacher01");
    expect_status(&[403], Some(&ttk), ADMIN_ENDPOINTS);

    // with admin token
    let atk = get_kc_admin_token();
    expect_status(&[403], Some(&atk), TEACHER_ENDPOINTS);
}

#[test]
#[ignore]
fn authorized() {
    expect_running_service();

    let atk = get_kc_admin_token();
    let ttk = get_kc_teacher_token("testteacher01");

    expect_status(&[200, 201, 400, 404], Some(&atk), ADMIN_ENDPOINTS);

    expect_status(&[200, 201, 400, 404], Some(&ttk), TEACHER_ENDPOINTS);

    expect_status(
        &[200, 201, 400, 404],
        Some(&atk),
        TEACHER_OR_ADMIN_ENDPOINTS,
    );
    expect_status(
        &[200, 201, 400, 404],
        Some(&ttk),
        TEACHER_OR_ADMIN_ENDPOINTS,
    );

    expect_status(&[200], None, NO_AUTH_ENDPOINTS);
    expect_status(&[200], Some(&atk), NO_AUTH_ENDPOINTS);
    expect_status(&[200], Some(&ttk), NO_AUTH_ENDPOINTS);
}
